package com.imooc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "sys_organization", schema = "welcomenews", catalog = "")
public class SysOrganization {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String address;
    private String code;
    private String icon;
    private Integer pid;
    private Integer seq;
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8") //json格式化输出，列表显示时用
    @DateTimeFormat(pattern="yyyy-MM-dd") //Spring MVC格式转换：增加、修改时，向后台传送数据时用
    private Date createdatetime;
    private String property;

    private String idname;

    private String potnum;

    public String getPotnum() {
        return potnum;
    }

    public void setPotnum(String potnum) {
        this.potnum = potnum;
    }

    public String getIdname() {
        return idname;
    }

    public void setIdname(String idname) {
        this.idname = idname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Date getCreatedatetime() {
        return createdatetime;
    }

    public void setCreatedatetime(Date createdatetime) {
        this.createdatetime = createdatetime;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
