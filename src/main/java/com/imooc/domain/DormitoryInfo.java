package com.imooc.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@Entity
@Table(name = "t_stu_dormitory")
public class DormitoryInfo {

    @Id
    @GeneratedValue
    private int id;
    private String examineeId;
    private String name;
    private int buildId;
    private int roomId;
    private int bedId;
    private String remark;
    private Integer createPeople;
    private Date createTime;
    private Integer createDepartment;
    private Integer updatePeople;
    private Date updateTime;
    private Integer updateDepartment;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExamineeId() {
        return examineeId;
    }

    public void setExamineeId(String examineeId) {
        this.examineeId = examineeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBuildId() {
        return buildId;
    }

    public void setBuildId(int buildId) {
        this.buildId = buildId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getBedId() {
        return bedId;
    }

    public void setBedId(int bedId) {
        this.bedId = bedId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Integer createPeople) {
        this.createPeople = createPeople;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateDepartment() {
        return createDepartment;
    }

    public void setCreateDepartment(Integer createDepartment) {
        this.createDepartment = createDepartment;
    }

    public Integer getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Integer updatePeople) {
        this.updatePeople = updatePeople;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUpdateDepartment() {
        return updateDepartment;
    }

    public void setUpdateDepartment(Integer updateDepartment) {
        this.updateDepartment = updateDepartment;
    }
}
