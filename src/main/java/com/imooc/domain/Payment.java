package com.imooc.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@Entity
@Table(name = "t_need_payment_data")
public class Payment {

    @Id
    @GeneratedValue
    private Integer id;

    private String examineeId;

    //学生姓名
    private String xm;

    //班级
    private String className;

    //项目
    private String chargeItem;

    //应收金额
    private String assessmentMoney;

    //网上已缴费/现场缴费/绿色通道
    private String state;

    //标准
    private String standard;

    //实际缴费金额
    private String actuallyMoney;

    //减免金额
    private String derateMoney;

    //缓缴金额
    private String deferredMoney;

    //退费金额
    private String returnsMoney;

    //缴费状态
    private  String payState;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExamineeId() {
        return examineeId;
    }

    public void setExamineeId(String examineeId) {
        this.examineeId = examineeId;
    }

    public String getChargeItem() {
        return chargeItem;
    }

    public void setChargeItem(String chargeItem) {
        this.chargeItem = chargeItem;
    }

    public String getAssessmentMoney() {
        return assessmentMoney;
    }

    public void setAssessmentMoney(String assessmentMoney) {
        this.assessmentMoney = assessmentMoney;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getActuallyMoney() {
        return actuallyMoney;
    }

    public void setActuallyMoney(String actuallyMoney) {
        this.actuallyMoney = actuallyMoney;
    }

    public String getDerateMoney() {
        return derateMoney;
    }

    public void setDerateMoney(String derateMoney) {
        this.derateMoney = derateMoney;
    }

    public String getDeferredMoney() {
        return deferredMoney;
    }

    public void setDeferredMoney(String deferredMoney) {
        this.deferredMoney = deferredMoney;
    }

    public String getReturnsMoney() {
        return returnsMoney;
    }

    public void setReturnsMoney(String returnsMoney) {
        this.returnsMoney = returnsMoney;
    }

    public String getPayState() {
        return payState;
    }

    public void setPayState(String payState) {
        this.payState = payState;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }
}
