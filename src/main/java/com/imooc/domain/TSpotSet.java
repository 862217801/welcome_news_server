package com.imooc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "t_spot_set", schema = "welcomenews", catalog = "")
public class TSpotSet {
    @Id
    @GeneratedValue
    private int id;
    private String spotName;
    private Double spotX;
    private Double spotY;
    private String college;
    private String department;
    private String major;
    private Integer createPeople;
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8") //json格式化输出，列表显示时用
    @DateTimeFormat(pattern="yyyy-MM-dd") //Spring MVC格式转换：增加、修改时，向后台传送数据时用
    private Date createTime;
    private Integer createDepartment;
    private Integer updatePeople;
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8") //json格式化输出，列表显示时用
    @DateTimeFormat(pattern="yyyy-MM-dd") //Spring MVC格式转换：增加、修改时，向后台传送数据时用
    private Date updateTime;
    private Integer updateDepartment;
    private String spotType;
    private String detailAddress;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpotName() {
        return spotName;
    }

    public void setSpotName(String spotName) {
        this.spotName = spotName;
    }

    public Double getSpotX() {
        return spotX;
    }

    public void setSpotX(Double spotX) {
        this.spotX = spotX;
    }

    public Double getSpotY() {
        return spotY;
    }

    public void setSpotY(Double spotY) {
        this.spotY = spotY;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Integer getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Integer createPeople) {
        this.createPeople = createPeople;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateDepartment() {
        return createDepartment;
    }

    public void setCreateDepartment(Integer createDepartment) {
        this.createDepartment = createDepartment;
    }

    public Integer getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Integer updatePeople) {
        this.updatePeople = updatePeople;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUpdateDepartment() {
        return updateDepartment;
    }

    public void setUpdateDepartment(Integer updateDepartment) {
        this.updateDepartment = updateDepartment;
    }

    public String getSpotType() {
        return spotType;
    }

    public void setSpotType(String spotType) {
        this.spotType = spotType;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }
}
