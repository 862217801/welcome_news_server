package com.imooc.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Administrator on 2017/7/25.
 */
@Entity
@Table(name = "base_data")
public class BaseData {
    @Id
    @GeneratedValue
    private Integer id;
    private String ddCode;
    private String ddName;
    private String ddTypeCode;
    private String ddTypeName;
    private String ppCode;
    private String ddSort;
    private String ddLevel;
    private String ddExp1;
    private String ddExp2;
    private String ddExp3;

    public BaseData(String ddCode, String ddName) {
        this.ddCode = ddCode;
        this.ddName = ddName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDdCode() {
        return ddCode;
    }

    public void setDdCode(String ddCode) {
        this.ddCode = ddCode;
    }

    public String getDdName() {
        return ddName;
    }

    public void setDdName(String ddName) {
        this.ddName = ddName;
    }

    public String getDdTypeCode() {
        return ddTypeCode;
    }

    public void setDdTypeCode(String ddTypeCode) {
        this.ddTypeCode = ddTypeCode;
    }

    public String getDdTypeName() {
        return ddTypeName;
    }

    public void setDdTypeName(String ddTypeName) {
        this.ddTypeName = ddTypeName;
    }

    public String getPpCode() {
        return ppCode;
    }

    public void setPpCode(String ppCode) {
        this.ppCode = ppCode;
    }

    public String getDdSort() {
        return ddSort;
    }

    public void setDdSort(String ddSort) {
        this.ddSort = ddSort;
    }

    public String getDdLevel() {
        return ddLevel;
    }

    public void setDdLevel(String ddLevel) {
        this.ddLevel = ddLevel;
    }

    public String getDdExp1() {
        return ddExp1;
    }

    public void setDdExp1(String ddExp1) {
        this.ddExp1 = ddExp1;
    }

    public String getDdExp2() {
        return ddExp2;
    }

    public void setDdExp2(String ddExp2) {
        this.ddExp2 = ddExp2;
    }

    public String getDdExp3() {
        return ddExp3;
    }

    public void setDdExp3(String ddExp3) {
        this.ddExp3 = ddExp3;
    }

    @Override
    public String toString() {
        return "BaseData{" +
                "id=" + id +
                ", ddCode='" + ddCode + '\'' +
                ", ddName='" + ddName + '\'' +
                ", ddTypeCode='" + ddTypeCode + '\'' +
                ", ddTypeName='" + ddTypeName + '\'' +
                ", ppCode='" + ppCode + '\'' +
                ", ddSort='" + ddSort + '\'' +
                ", ddLevel='" + ddLevel + '\'' +
                '}';
    }
}
