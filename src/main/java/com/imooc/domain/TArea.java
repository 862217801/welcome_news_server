package com.imooc.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "t_area", schema = "welcomenews", catalog = "")
public class TArea {
    private int id;
    private String name;
    private String remark;
    private String college;
    private Integer createPeople;
    private Timestamp createTime;
    private Integer createDepartment;
    private Integer updatePeople;
    private Timestamp updateTime;
    private Integer updateDepartment;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "college")
    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    @Basic
    @Column(name = "create_people")
    public Integer getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Integer createPeople) {
        this.createPeople = createPeople;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "create_department")
    public Integer getCreateDepartment() {
        return createDepartment;
    }

    public void setCreateDepartment(Integer createDepartment) {
        this.createDepartment = createDepartment;
    }

    @Basic
    @Column(name = "update_people")
    public Integer getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Integer updatePeople) {
        this.updatePeople = updatePeople;
    }

    @Basic
    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "update_department")
    public Integer getUpdateDepartment() {
        return updateDepartment;
    }

    public void setUpdateDepartment(Integer updateDepartment) {
        this.updateDepartment = updateDepartment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TArea tArea = (TArea) o;

        if (id != tArea.id) return false;
        if (name != null ? !name.equals(tArea.name) : tArea.name != null) return false;
        if (remark != null ? !remark.equals(tArea.remark) : tArea.remark != null) return false;
        if (college != null ? !college.equals(tArea.college) : tArea.college != null) return false;
        if (createPeople != null ? !createPeople.equals(tArea.createPeople) : tArea.createPeople != null) return false;
        if (createTime != null ? !createTime.equals(tArea.createTime) : tArea.createTime != null) return false;
        if (createDepartment != null ? !createDepartment.equals(tArea.createDepartment) : tArea.createDepartment != null)
            return false;
        if (updatePeople != null ? !updatePeople.equals(tArea.updatePeople) : tArea.updatePeople != null) return false;
        if (updateTime != null ? !updateTime.equals(tArea.updateTime) : tArea.updateTime != null) return false;
        if (updateDepartment != null ? !updateDepartment.equals(tArea.updateDepartment) : tArea.updateDepartment != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (college != null ? college.hashCode() : 0);
        result = 31 * result + (createPeople != null ? createPeople.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (createDepartment != null ? createDepartment.hashCode() : 0);
        result = 31 * result + (updatePeople != null ? updatePeople.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (updateDepartment != null ? updateDepartment.hashCode() : 0);
        return result;
    }
}
