package com.imooc.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "t_room", schema = "welcomenews", catalog = "")
public class TRoom {
    private int id;
    private Integer bid;
    private String code;
    private String name;
    private String property;
    private Integer price;
    private String type;
    private Integer layer;
    private String college;
    private String remark;
    private Integer createPeople;
    private Timestamp createTime;
    private Integer createDepartment;
    private Integer updatePeople;
    private Timestamp updateTime;
    private Integer updateDepartment;

    //showcontentname
    private String showcontentname;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "bid")
    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "property")
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Basic
    @Column(name = "price")
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "layer")
    public Integer getLayer() {
        return layer;
    }

    public void setLayer(Integer layer) {
        this.layer = layer;
    }

    @Basic
    @Column(name = "college")
    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "create_people")
    public Integer getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Integer createPeople) {
        this.createPeople = createPeople;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "create_department")
    public Integer getCreateDepartment() {
        return createDepartment;
    }

    public void setCreateDepartment(Integer createDepartment) {
        this.createDepartment = createDepartment;
    }

    @Basic
    @Column(name = "update_people")
    public Integer getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Integer updatePeople) {
        this.updatePeople = updatePeople;
    }

    @Basic
    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "update_department")
    public Integer getUpdateDepartment() {
        return updateDepartment;
    }

    public void setUpdateDepartment(Integer updateDepartment) {
        this.updateDepartment = updateDepartment;
    }

    public String getShowcontentname() {
        return showcontentname;
    }

    public void setShowcontentname(String showcontentname) {
        this.showcontentname = showcontentname;
    }
}
