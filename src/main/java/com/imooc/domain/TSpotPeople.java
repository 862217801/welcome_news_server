package com.imooc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "t_spot_people", schema = "welcomenews", catalog = "")
public class TSpotPeople {
    @Id
    @GeneratedValue
    private int id;
    private Integer spotId;
    private String charge;
    private String name;
    private String contactInformation;
    private String role;
    private String studentId;
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8") //json格式化输出，列表显示时用
    @DateTimeFormat(pattern="yyyy-MM-dd") //Spring MVC格式转换：增加、修改时，向后台传送数据时用
    private Date schedulingTimeS;
    private Integer createPeople;
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8") //json格式化输出，列表显示时用
    @DateTimeFormat(pattern="yyyy-MM-dd") //Spring MVC格式转换：增加、修改时，向后台传送数据时用
    private Date createTime;
    private Integer createDepartment;
    private Integer updatePeople;
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8") //json格式化输出，列表显示时用
    @DateTimeFormat(pattern="yyyy-MM-dd") //Spring MVC格式转换：增加、修改时，向后台传送数据时用
    private Date updateTime;
    private Integer updateDepartment;
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8") //json格式化输出，列表显示时用
    @DateTimeFormat(pattern="yyyy-MM-dd") //Spring MVC格式转换：增加、修改时，向后台传送数据时用
    private Date schedulingTimeE;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getSpotId() {
        return spotId;
    }

    public void setSpotId(Integer spotId) {
        this.spotId = spotId;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(String contactInformation) {
        this.contactInformation = contactInformation;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Date getSchedulingTimeS() {
        return schedulingTimeS;
    }

    public void setSchedulingTimeS(Date schedulingTimeS) {
        this.schedulingTimeS = schedulingTimeS;
    }

    public Integer getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Integer createPeople) {
        this.createPeople = createPeople;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateDepartment() {
        return createDepartment;
    }

    public void setCreateDepartment(Integer createDepartment) {
        this.createDepartment = createDepartment;
    }

    public Integer getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Integer updatePeople) {
        this.updatePeople = updatePeople;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUpdateDepartment() {
        return updateDepartment;
    }

    public void setUpdateDepartment(Integer updateDepartment) {
        this.updateDepartment = updateDepartment;
    }

    public Date getSchedulingTimeE() {
        return schedulingTimeE;
    }

    public void setSchedulingTimeE(Date schedulingTimeE) {
        this.schedulingTimeE = schedulingTimeE;
    }
}
