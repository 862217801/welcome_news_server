package com.imooc.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "t_stu_dormitory", schema = "welcomenews", catalog = "")
public class TStuDormitory {
    private int id;
    private String examineeId;
    private String name;
    private Integer buildId;
    private Integer roomId;
    private Integer bedId;
    private String remark;
    private Integer createPeople;
    private Timestamp createTime;
    private Integer createDepartment;
    private Integer updatePeople;
    private Timestamp updateTime;
    private Integer updateDepartment;

    //楼栋名称
    private String buildName;
    //宿舍名称
    private String roomName;
    //床位名称
    private String bedName;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "examinee_id")
    public String getExamineeId() {
        return examineeId;
    }

    public void setExamineeId(String examineeId) {
        this.examineeId = examineeId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "build_id")
    public Integer getBuildId() {
        return buildId;
    }

    public void setBuildId(Integer buildId) {
        this.buildId = buildId;
    }

    @Basic
    @Column(name = "room_id")
    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    @Basic
    @Column(name = "bed_id")
    public Integer getBedId() {
        return bedId;
    }

    public void setBedId(Integer bedId) {
        this.bedId = bedId;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "create_people")
    public Integer getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Integer createPeople) {
        this.createPeople = createPeople;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "create_department")
    public Integer getCreateDepartment() {
        return createDepartment;
    }

    public void setCreateDepartment(Integer createDepartment) {
        this.createDepartment = createDepartment;
    }

    @Basic
    @Column(name = "update_people")
    public Integer getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Integer updatePeople) {
        this.updatePeople = updatePeople;
    }

    @Basic
    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "update_department")
    public Integer getUpdateDepartment() {
        return updateDepartment;
    }

    public void setUpdateDepartment(Integer updateDepartment) {
        this.updateDepartment = updateDepartment;
    }

    public String getBuildName() {
        return buildName;
    }

    public void setBuildName(String buildName) {
        this.buildName = buildName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getBedName() {
        return bedName;
    }

    public void setBedName(String bedName) {
        this.bedName = bedName;
    }
}
