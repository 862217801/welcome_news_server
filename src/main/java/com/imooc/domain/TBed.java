package com.imooc.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "t_bed", schema = "welcomenews", catalog = "")
public class TBed {
    private int id;
    private Integer bid;
    private Integer rid;
    private Integer code;
    private String remark;
    private Integer createPeople;
    private Timestamp createTime;
    private Integer createDepartment;
    private Integer updatePeople;
    private Timestamp updateTime;
    private Integer updateDepartment;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "bid")
    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    @Basic
    @Column(name = "rid")
    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    @Basic
    @Column(name = "code")
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "create_people")
    public Integer getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Integer createPeople) {
        this.createPeople = createPeople;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "create_department")
    public Integer getCreateDepartment() {
        return createDepartment;
    }

    public void setCreateDepartment(Integer createDepartment) {
        this.createDepartment = createDepartment;
    }

    @Basic
    @Column(name = "update_people")
    public Integer getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Integer updatePeople) {
        this.updatePeople = updatePeople;
    }

    @Basic
    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "update_department")
    public Integer getUpdateDepartment() {
        return updateDepartment;
    }

    public void setUpdateDepartment(Integer updateDepartment) {
        this.updateDepartment = updateDepartment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TBed tBed = (TBed) o;

        if (id != tBed.id) return false;
        if (bid != null ? !bid.equals(tBed.bid) : tBed.bid != null) return false;
        if (rid != null ? !rid.equals(tBed.rid) : tBed.rid != null) return false;
        if (code != null ? !code.equals(tBed.code) : tBed.code != null) return false;
        if (remark != null ? !remark.equals(tBed.remark) : tBed.remark != null) return false;
        if (createPeople != null ? !createPeople.equals(tBed.createPeople) : tBed.createPeople != null) return false;
        if (createTime != null ? !createTime.equals(tBed.createTime) : tBed.createTime != null) return false;
        if (createDepartment != null ? !createDepartment.equals(tBed.createDepartment) : tBed.createDepartment != null)
            return false;
        if (updatePeople != null ? !updatePeople.equals(tBed.updatePeople) : tBed.updatePeople != null) return false;
        if (updateTime != null ? !updateTime.equals(tBed.updateTime) : tBed.updateTime != null) return false;
        if (updateDepartment != null ? !updateDepartment.equals(tBed.updateDepartment) : tBed.updateDepartment != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (bid != null ? bid.hashCode() : 0);
        result = 31 * result + (rid != null ? rid.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (createPeople != null ? createPeople.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (createDepartment != null ? createDepartment.hashCode() : 0);
        result = 31 * result + (updatePeople != null ? updatePeople.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (updateDepartment != null ? updateDepartment.hashCode() : 0);
        return result;
    }
}
