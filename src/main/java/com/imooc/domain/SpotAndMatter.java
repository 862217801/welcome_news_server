package com.imooc.domain;

import javax.persistence.*;

@Entity
@Table(name = "spot_and_matter", schema = "welcomenews", catalog = "")
public class SpotAndMatter {
    @Id
    @GeneratedValue
    private int id;
    private Integer spotId;
    private Integer matterId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getSpotId() {
        return spotId;
    }

    public void setSpotId(Integer spotId) {
        this.spotId = spotId;
    }

    public Integer getMatterId() {
        return matterId;
    }

    public void setMatterId(Integer matterId) {
        this.matterId = matterId;
    }


}
