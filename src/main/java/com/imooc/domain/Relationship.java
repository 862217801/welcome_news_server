package com.imooc.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@Entity
@Table(name = "t_relationship")
public class Relationship {

    @Id
    @GeneratedValue
    private Integer id;
    private Integer registerId;
    private String relationship;
    private String name;
    private String  contactInformation;

    public Relationship() {
    }

    public Relationship(Integer id, Integer registerId, String relationship, String name, String contactInformation) {
        this.id = id;
        this.registerId = registerId;
        this.relationship = relationship;
        this.name = name;
        this.contactInformation = contactInformation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(String contactInformation) {
        this.contactInformation = contactInformation;
    }

}
