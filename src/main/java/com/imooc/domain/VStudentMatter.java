package com.imooc.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by BINGO on 2017/8/15 ${time}.
 */
@Entity
@Table(name = "v_spot_student_state")
public class VStudentMatter {

    @Id
    private Integer id;

    @Column(name = "taskid")
    private Integer taskId;

    private String examineeId;
    private String spotName;
    private Integer state;
    private String matter;
    private String matterDescribe;
    private String spotType;
    private String type;
    private String spot_x;
    private String spot_y;

    public String getExamineeId() {
        return examineeId;
    }

    public void setExamineeId(String examineeId) {
        this.examineeId = examineeId;
    }

    public String getSpotName() {
        return spotName;
    }

    public void setSpotName(String spotName) {
        this.spotName = spotName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMatter() {
        return matter;
    }

    public void setMatter(String matter) {
        this.matter = matter;
    }

    public String getMatterDescribe() {
        return matterDescribe;
    }

    public void setMatterDescribe(String matterDescribe) {
        this.matterDescribe = matterDescribe;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSpotType() {
        return spotType;
    }

    public void setSpotType(String spotType) {
        this.spotType = spotType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getSpot_x() {
        return spot_x;
    }

    public void setSpot_x(String spot_x) {
        this.spot_x = spot_x;
    }

    public String getSpot_y() {
        return spot_y;
    }

    public void setSpot_y(String spot_y) {
        this.spot_y = spot_y;
    }
}
