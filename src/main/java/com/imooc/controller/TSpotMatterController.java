package com.imooc.controller;

import com.imooc.domain.KeyValue;
import com.imooc.domain.Result;
import com.imooc.domain.TSpotMatter;
import com.imooc.domain.TSpotSet;
import com.imooc.repository.TSpotMatterRepository;
import com.imooc.repository.TSpotSetRepository;
import com.imooc.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
@CrossOrigin
@RestController
public class TSpotMatterController {
   @Autowired
   private TSpotMatterRepository tSpotMatterRepository;
    private final static Logger logger = LoggerFactory.getLogger(TSpotMatterController.class);

    @GetMapping(value = "tspotmatter")
    public Result<TSpotMatter> findall() {
        return ResultUtil.success(tSpotMatterRepository.findAll());
    }

    @GetMapping(value = "tspotmatterpopup")
    public Result<TSpotMatter> tspotmatterpopup() {
        List<TSpotMatter> tSpotMatterList = tSpotMatterRepository.findAll();
        List<KeyValue> kvList = new ArrayList();
        for (TSpotMatter t:tSpotMatterList) {
            KeyValue kv = new KeyValue();
            kv.setName(t.getMatter());
            kv.setValue(String.valueOf(t.getId()));
            kvList.add(kv);
        }
        return ResultUtil.success(kvList);
    }
}
