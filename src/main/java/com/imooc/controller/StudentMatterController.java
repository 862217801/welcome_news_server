package com.imooc.controller;

import com.imooc.domain.Result;
import com.imooc.domain.VStudentMatter;
import com.imooc.repository.VStudentMatterRepository;
import com.imooc.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by BINGO on 2017/8/15 ${time}.
 */
@CrossOrigin
@RestController
public class StudentMatterController {
    @Autowired
    private VStudentMatterRepository vStudentMatterRepository;

  /*  @GetMapping(value = "studentMatters/{id}")
    private Result<VStudentMatter> getStudentMatters(@PathVariable("id") String examineeId){
        VStudentMatter vStudentMatter = new VStudentMatter();
        vStudentMatter.setExamineeId(examineeId);
        vStudentMatter.setType("需办理");
        vStudentMatter.setSpotType("2301");
        ExampleMatcher matcher = ExampleMatcher.matching();
        Example<VStudentMatter> ex= Example.of(vStudentMatter,matcher);
        return ResultUtil.success(vStudentMatterRepository.findAll(ex));
    }*/

    @GetMapping(value = "Matters/{id}")
    private Result<VStudentMatter> getMatters(@PathVariable("id") String examineeId){
        List<VStudentMatter> vStudentMatters =  vStudentMatterRepository.findByExamineeId(examineeId);
        return ResultUtil.success(vStudentMatters);
    }
    @GetMapping(value = "Matters/{id}/{examineeId}")
    private Result<VStudentMatter> getMatterss(@PathVariable("id") Integer id,@PathVariable("examineeId") String examineeId){
        List<VStudentMatter>  vStudentMatter =  vStudentMatterRepository.findByIdAndExamineeId(id,examineeId);
        return ResultUtil.success(vStudentMatter);
    }
}
