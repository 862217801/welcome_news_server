package com.imooc.controller;

import com.imooc.domain.Result;
import com.imooc.domain.SysUser;
import com.imooc.repository.SysUserRepository;
import com.imooc.utils.MD5Util;
import com.imooc.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/7/25.
 */
@CrossOrigin
@RestController
public class LoginController {

    private final static Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private SysUserRepository sysUserRepository;

    @PostMapping(value = "loginValidate")
    public Result<SysUser> loginValidate(String loginname,String password){
        password = MD5Util.md5(password);
        logger.info(password);
        return ResultUtil.success(sysUserRepository.findByLoginnameAndPassword(loginname, password));
    }

    @PostMapping(value = "validateNoPassword")
    public Result<SysUser> loginValidateNoPassword(String loginname){
        return ResultUtil.success(sysUserRepository.findByLoginname(loginname));
    }

}
