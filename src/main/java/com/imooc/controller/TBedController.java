package com.imooc.controller;

import com.imooc.domain.Result;
import com.imooc.domain.TBed;
import com.imooc.domain.TRoom;
import com.imooc.domain.TStuDormitory;
import com.imooc.repository.TBedRepository;
import com.imooc.repository.TBuildRepository;
import com.imooc.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@CrossOrigin
@RestController
public class TBedController {
@Autowired
private TBedRepository tBedRepository;

    @GetMapping(value = "tbed")
    public Result<TBed> dormitoryFindAll() {
        return ResultUtil.success(tBedRepository.findAll());
    }

    @GetMapping(value = "tbed/{rid}")
    public Result<TRoom> dormitoryFindOne(@PathVariable("rid") Integer rid) {
        return ResultUtil.success(tBedRepository.findByRid(rid));
    }


}
