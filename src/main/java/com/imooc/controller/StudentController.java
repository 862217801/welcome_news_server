package com.imooc.controller;

import com.imooc.domain.Result;
import com.imooc.domain.Student;
import com.imooc.repository.StudentRepository;
import com.imooc.service.StudentService;
import com.imooc.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * Created by Administrator on 2017/7/25.
 */
@CrossOrigin
@RestController
public class StudentController {

    private final static Logger logger = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/students")
    public Result<Student> studentList(Student student) {
        return ResultUtil.success(studentRepository.findAll());
    }

    @GetMapping(value = "students/{id}")
    public Result<Student> studentFindOne(@PathVariable("id") String examineeId) {
        return ResultUtil.success(studentService.selectStudentById(examineeId));
    }

    @PutMapping(value = "students")
    public Result<Student> studentUpdate(Student student) {
        student.setUpdateTime(new Date());
        return ResultUtil.success(studentRepository.saveAndFlush(student));
    }

}
