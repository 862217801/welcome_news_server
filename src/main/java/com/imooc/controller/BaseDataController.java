package com.imooc.controller;

import com.imooc.domain.BaseData;
import com.imooc.domain.KeyValue;
import com.imooc.domain.Result;
import com.imooc.repository.BaseDataRepository;
import com.imooc.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
@CrossOrigin
@RestController
public class BaseDataController {
    private final static Logger logger = LoggerFactory.getLogger(BaseDataController.class);

    @Autowired
    private BaseDataRepository baseDataRepository;

    @GetMapping(value = "/baseDatas/{name}")
    public Result<BaseData> getBaseDatas(@PathVariable("name") String name) {
        List<BaseData> baseDataList = baseDataRepository.findByddTypeName(name);
        List<KeyValue> kvList = new ArrayList();
        for (BaseData b:baseDataList) {
            KeyValue kv = new KeyValue();
            kv.setName(b.getDdName());
            kv.setValue(b.getDdCode());
            kvList.add(kv);
        }
        return ResultUtil.success(kvList);
    }


}
