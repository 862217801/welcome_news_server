package com.imooc.controller;

import com.imooc.domain.Result;
import com.imooc.domain.TBuild;
import com.imooc.domain.TRoom;
import com.imooc.domain.TStuDormitory;
import com.imooc.repository.TBedRepository;
import com.imooc.repository.TBuildRepository;
import com.imooc.repository.TRoomRepository;
import com.imooc.service.TStuDormitoryService;
import com.imooc.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@CrossOrigin
@RestController
public class TBuildController {
@Autowired
private TBuildRepository tBuildRepository;
@Autowired
private   TRoomRepository tRoomRepository;
@Autowired
private  TBedRepository tBedRepository;

    @GetMapping(value = "tbuild")
    public Result<TBuild> dormitoryFindAll() {
        List<TBuild> tBuilds = tBuildRepository.findAll();
        for (TBuild t:tBuilds) {
            List<TRoom> tRooms = tRoomRepository.findByBid(t.getId());
            t.setRoomnum(tRooms.size());
            Integer bednum = 0;
            for (TRoom tr: tRooms ) {
                bednum += tBedRepository.findByRid(tr.getId()).size();
            }
            t.setBednum(bednum);
        }
        return ResultUtil.success(tBuilds);
    }

    @GetMapping(value = "tbuild/{aid}")
    public Result<TRoom> dormitoryFindOne(@PathVariable("aid") Integer aid) {
        return ResultUtil.success(tBuildRepository.findByAid(aid));
    }

}
