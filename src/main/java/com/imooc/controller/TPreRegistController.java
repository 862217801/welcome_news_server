package com.imooc.controller;

import com.imooc.domain.*;
import com.imooc.repository.StudentRepository;
import com.imooc.repository.TBedRepository;
import com.imooc.repository.TPreRegistRepository;
import com.imooc.service.StudentService;
import com.imooc.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@CrossOrigin
@RestController
public class TPreRegistController {
@Autowired
private TPreRegistRepository tPreRegistRepository;
@Autowired
private StudentService studentService;

    @GetMapping(value = "tpregist")
    public Result<TPreRegist> dormitoryFindAll() {
        return ResultUtil.success(tPreRegistRepository.findAll());
    }

    @GetMapping(value = "tongji")
    public Result<TPreRegist> tongji() {
        List<TPreRegist> tPreRegists = tPreRegistRepository.findAll();
        List<Student> studentList = new ArrayList<>();
        for (TPreRegist t:tPreRegists) {
            Student student = studentService.selectStudentById(t.getExamineeId());
            System.out.println(student.getClassName());
            studentList.add(student);
        }
        return ResultUtil.success(studentList);
    }


}
