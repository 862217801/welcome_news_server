package com.imooc.controller;

import com.imooc.domain.BaseData;
import com.imooc.domain.FamilyInfo;
import com.imooc.domain.Result;
import com.imooc.repository.BaseDataRepository;
import com.imooc.repository.FamilyInfoRepository;
import com.imooc.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2017/8/4.
 */
@CrossOrigin
@RestController
public class FamilyInfoController {

    @Autowired
    private FamilyInfoRepository familyInfoRepository;

    @Autowired
    private BaseDataRepository baseDataRepository;

    @GetMapping(value = "familyInfo/{examineeId}")
    public Result<FamilyInfo> getFamilyInfo(@PathVariable("examineeId") String examineeId){
        List<FamilyInfo> famlist = familyInfoRepository.findByexamineeId(examineeId);
        List<BaseData> baseDataList = baseDataRepository.findByddTypeName("称谓");
        for (FamilyInfo fam : famlist){
            for (BaseData bd : baseDataList){
                if (bd.getDdCode().equals(fam.getAppellation())){
                    fam.setAppellationName(bd.getDdName());
                }
            }
        }
        return ResultUtil.success(familyInfoRepository.findByexamineeId(examineeId));
    }
}
