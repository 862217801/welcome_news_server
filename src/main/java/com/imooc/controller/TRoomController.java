package com.imooc.controller;

import com.imooc.domain.Result;
import com.imooc.domain.TBed;
import com.imooc.domain.TRoom;
import com.imooc.domain.TStuDormitory;
import com.imooc.repository.TBedRepository;
import com.imooc.repository.TRoomRepository;
import com.imooc.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@CrossOrigin
@RestController
public class TRoomController {
@Autowired
private TRoomRepository tRoomRepository;

    @GetMapping(value = "troom")
    public Result<TRoom> dormitoryFindAll() {
        return ResultUtil.success(tRoomRepository.findAll());
    }

    @GetMapping(value = "troom/{bid}")
    public Result<TRoom> dormitoryFindOne(@PathVariable("bid") Integer bid) {
        List<TRoom> tRoomList = tRoomRepository.findByBid(bid);
        Integer i = 1;
        for (TRoom t: tRoomList ) {
           t.setShowcontentname("showContent"+i);
            i++;
        }
        return ResultUtil.success(tRoomList);
    }



}
