package com.imooc.controller;

import com.imooc.domain.KeyValue;
import com.imooc.domain.Result;
import com.imooc.domain.SysOrganization;
import com.imooc.repository.SysOrganizationRepository;
import com.imooc.repository.TSpotSetRepository;
import com.imooc.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
@CrossOrigin
@RestController
public class SysOrganizationController {

    private final static Logger logger = LoggerFactory.getLogger(SysOrganizationController.class);

    @Autowired
    private SysOrganizationRepository sysOrganizationRepository;
    @Autowired
    private TSpotSetRepository tSpotSetRepository;

    @GetMapping(value = "/organizationList")
    public Result<SysOrganization> organizationList(SysOrganization sysOrganization) {
        List<SysOrganization> sysOrganizations = sysOrganizationRepository.findByPropertyIsNotNullAndPidIsNull();
        for (SysOrganization s:sysOrganizations) {
            s.setIdname("showContent"+s.getId());
            Integer potnum = tSpotSetRepository.findByDepartment(s.getId().toString()).size();
            if (potnum>0){
                s.setPotnum(potnum.toString()+"处");
            }else {
                s.setPotnum(potnum.toString());
            }


        }
        return ResultUtil.success(sysOrganizations);
    }

    @GetMapping(value = "/orgpopupList")
    public Result<SysOrganization> orgpopupList(SysOrganization sysOrganization) {
        List<SysOrganization> sysOrganizations = sysOrganizationRepository.findByPropertyIsNotNullAndPidIsNull();
        List<KeyValue> kvList = new ArrayList();
        for (SysOrganization s:sysOrganizations) {
            KeyValue kv = new KeyValue();
            kv.setName(s.getName());
            kv.setValue(s.getId().toString());
            kvList.add(kv);
        }
        return ResultUtil.success(kvList);
    }
}
