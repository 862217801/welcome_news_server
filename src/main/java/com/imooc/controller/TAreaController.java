package com.imooc.controller;

import com.imooc.domain.Result;
import com.imooc.domain.TArea;
import com.imooc.domain.TBed;
import com.imooc.domain.TRoom;
import com.imooc.repository.TAreaRepository;
import com.imooc.repository.TBedRepository;
import com.imooc.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@CrossOrigin
@RestController
public class TAreaController {
@Autowired
private TAreaRepository tAreaRepository;

    @GetMapping(value = "tarea")
    public Result<TArea> dormitoryFindAll() {
        return ResultUtil.success(tAreaRepository.findAll());
    }

}
