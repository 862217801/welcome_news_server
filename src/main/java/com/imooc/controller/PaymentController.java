package com.imooc.controller;

import com.imooc.domain.Payment;
import com.imooc.domain.Result;
import com.imooc.repository.PaymentRepository;
import com.imooc.service.PaymentService;
import com.imooc.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
@CrossOrigin
@RestController
public class PaymentController {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentService paymentService;
    @GetMapping(value = "payment/{examineeId}")
    public Result<Payment> getPayment(@PathVariable("examineeId") String examineeId){
        return ResultUtil.success(paymentService.findByexamineeId(examineeId));
    }


}
