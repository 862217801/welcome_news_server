package com.imooc.controller;

import com.alibaba.fastjson.JSONObject;
import com.imooc.domain.Result;
import com.imooc.domain.SpotAndMatter;
import com.imooc.domain.TSpotPeople;
import com.imooc.domain.TSpotSet;
import com.imooc.repository.SpotAndMatterRepository;
import com.imooc.repository.TSpotPeopleRepository;
import com.imooc.repository.TSpotSetRepository;
import com.imooc.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2017/7/25.
 */
@CrossOrigin
@RestController
public class TSpotSetController {
   @Autowired
   private TSpotSetRepository tSpotSetRepository;

    @Autowired
    private TSpotPeopleRepository tSpotPeopleRepository;

    @Autowired
    private SpotAndMatterRepository spotAndMatterRepository;
    private final static Logger logger = LoggerFactory.getLogger(TSpotSetController.class);

    @GetMapping(value = "tspotset/{department}")
    public Result<TSpotSet> studentFindOne(@PathVariable("department") String department) {
        return ResultUtil.success(tSpotSetRepository.findByDepartment(department));
    }

    @GetMapping(value = "tspotsetbsdw/{spottype}")
    public Result<TSpotSet> tspotsetbsdw(@PathVariable("spottype") String spottype) {
        return ResultUtil.success(tSpotSetRepository.findBySpotType(spottype));
    }
    @GetMapping(value = "tspotsetid/{id}")
    public Result<TSpotSet> tspotsetid(@PathVariable("id") Integer id) {
        return ResultUtil.success(tSpotSetRepository.findById(id));
    }

    @PostMapping(value = "spotsave")
    public Result<TSpotSet> tspotsave(String tSpotSetStr,String matter,String spotType,String department,String fzr) {
        System.out.println(fzr);
        TSpotSet tSpotSet = JSONObject.parseObject(tSpotSetStr, TSpotSet.class);
        tSpotSet.setDepartment(department);
        tSpotSet.setSpotType(spotType);
        TSpotSet spotSetsave = tSpotSetRepository.save(tSpotSet);
        SpotAndMatter spotAndMatter = new SpotAndMatter();
        spotAndMatter.setMatterId(Integer.parseInt(matter));
        spotAndMatter.setSpotId(spotSetsave.getId());
        spotAndMatterRepository.save(spotAndMatter);
        TSpotPeople tSpotPeople = new TSpotPeople();
        tSpotPeople.setSpotId(spotSetsave.getId());
        tSpotPeople.setCharge(fzr);
        tSpotPeopleRepository.save(tSpotPeople);
        return ResultUtil.success(spotSetsave);
    }


}
