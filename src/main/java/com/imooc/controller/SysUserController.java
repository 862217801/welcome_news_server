package com.imooc.controller;

import com.imooc.domain.KeyValue1;
import com.imooc.domain.Result;
import com.imooc.domain.SysUser;
import com.imooc.domain.TSpotSet;
import com.imooc.repository.SysUserRepository;
import com.imooc.repository.TSpotSetRepository;
import com.imooc.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
@CrossOrigin
@RestController
public class SysUserController {
   @Autowired
   private SysUserRepository sysUserRepository;
    private final static Logger logger = LoggerFactory.getLogger(SysUserController.class);

    @GetMapping(value = "userList")
    public Result<SysUser> userList() {
        List<SysUser> sysUserList = sysUserRepository.findAll();
        List<KeyValue1> keyValue1List = new ArrayList<>();
        for (SysUser s: sysUserList) {
            KeyValue1 keyValue1 = new KeyValue1();
            keyValue1.setKey(s.getId().toString());
            keyValue1.setValue(s.getName());
            keyValue1List.add(keyValue1);
        }
        return ResultUtil.success(keyValue1List);
    }

}
