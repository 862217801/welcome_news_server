package com.imooc.repository;

import com.imooc.domain.PreRegist;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface PreRegistRepository extends JpaRepository<PreRegist,Integer> {

    public PreRegist findByexamineeId(String id);
}
