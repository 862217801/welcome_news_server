package com.imooc.repository;

import com.imooc.domain.TArea;
import com.imooc.domain.TBed;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface TAreaRepository extends JpaRepository<TArea,Integer> {

}
