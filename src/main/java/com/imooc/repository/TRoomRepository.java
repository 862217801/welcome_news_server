package com.imooc.repository;

import com.imooc.domain.TBed;
import com.imooc.domain.TRoom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface TRoomRepository extends JpaRepository<TRoom,Integer> {

public TRoom findById(Integer id);
public List<TRoom> findByBid(Integer bid);
}
