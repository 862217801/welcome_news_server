package com.imooc.repository;

import com.imooc.domain.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 2017/7/25.
 */
public interface SysUserRepository extends JpaRepository<SysUser,Integer> {

    public SysUser findByLoginnameAndPassword(String loginname,String password);

    public SysUser findByLoginname(String loginname);

}
