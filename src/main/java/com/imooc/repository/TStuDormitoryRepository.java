package com.imooc.repository;

import com.imooc.domain.Payment;
import com.imooc.domain.TStuDormitory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface TStuDormitoryRepository extends JpaRepository<TStuDormitory,Integer> {

public TStuDormitory findByExamineeId(String examineeId);
}
