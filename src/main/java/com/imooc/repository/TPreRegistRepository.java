package com.imooc.repository;

import com.imooc.domain.TPreRegist;
import com.imooc.domain.TRoom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface TPreRegistRepository extends JpaRepository<TPreRegist,Integer> {

}
