package com.imooc.repository;

import com.imooc.domain.DormitoryInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/11 ${time}.
 */
public interface DormitoryInfoRepository extends JpaRepository<DormitoryInfo,Integer> {

    public DormitoryInfo findByexamineeId(String examineeId);

    public List<DormitoryInfo> findByroomId(Integer roomId);

}
