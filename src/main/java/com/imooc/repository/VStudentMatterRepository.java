package com.imooc.repository;

import com.imooc.domain.VStudentMatter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/15 ${time}.
 */
public interface VStudentMatterRepository extends JpaRepository<VStudentMatter,Integer> {

    public List<VStudentMatter> findByExamineeId(String examineeId);

    public List<VStudentMatter> findByIdAndExamineeId(Integer id,String examineeId);
}
