package com.imooc.repository;

import com.imooc.domain.TBed;
import com.imooc.domain.TBuild;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface TBuildRepository extends JpaRepository<TBuild,Integer> {

public TBuild findById(Integer id);
public List<TBuild> findByAid(Integer aid);
}
