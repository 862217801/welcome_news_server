package com.imooc.repository;

import com.imooc.domain.FamilyInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017/8/4.
 */
public interface FamilyInfoRepository extends JpaRepository<FamilyInfo, Integer> {

    public List<FamilyInfo> findByexamineeId(String examineeId);
}
