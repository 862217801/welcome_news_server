package com.imooc.repository;

import com.imooc.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface PaymentRepository extends JpaRepository<Payment,Integer> {

    public List<Payment> findByexamineeId(String examineeId);
}
