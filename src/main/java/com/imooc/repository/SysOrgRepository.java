package com.imooc.repository;

import com.imooc.domain.SysOrg;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 2017/8/3.
 */
public interface SysOrgRepository extends JpaRepository<SysOrg,Integer> {
}
