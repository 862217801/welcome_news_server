package com.imooc.repository;

import com.imooc.domain.SysOrganization;
import com.imooc.domain.TSpotSet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
public interface TSpotSetRepository extends JpaRepository<TSpotSet, Integer> {
  public List<TSpotSet> findByDepartment(String department);
  public List<TSpotSet> findBySpotType(String spotType);
  public TSpotSet findById(Integer id);
}
