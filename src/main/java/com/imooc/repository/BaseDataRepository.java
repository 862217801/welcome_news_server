package com.imooc.repository;

import com.imooc.domain.BaseData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
public interface BaseDataRepository extends JpaRepository<BaseData, Integer> {

    @Query("select new BaseData (b.ddCode, b.ddName) from BaseData  b where b.ddTypeName = ?1")
    public List<BaseData> findByddTypeName(String ddTypeName);


}
