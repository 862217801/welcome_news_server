package com.imooc.repository;

import com.imooc.domain.TBed;
import com.imooc.domain.TStuDormitory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface TBedRepository extends JpaRepository<TBed,Integer> {

public TBed findById(Integer id);
public List<TBed> findByRid(Integer rid);
}
