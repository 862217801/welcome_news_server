package com.imooc.repository;

import com.imooc.domain.SpotAndMatter;
import com.imooc.domain.SysOrganization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
public interface SpotAndMatterRepository extends JpaRepository<SpotAndMatter, Integer> {

}
