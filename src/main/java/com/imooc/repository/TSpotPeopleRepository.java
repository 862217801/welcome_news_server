package com.imooc.repository;

import com.imooc.domain.TSpotMatter;
import com.imooc.domain.TSpotPeople;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 2017/7/25.
 */
public interface TSpotPeopleRepository extends JpaRepository<TSpotPeople, Integer> {
}
