package com.imooc.repository;

import com.imooc.domain.TSpotMatter;
import com.imooc.domain.TSpotSet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
public interface TSpotMatterRepository extends JpaRepository<TSpotMatter, Integer> {
}
