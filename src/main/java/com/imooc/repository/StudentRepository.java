package com.imooc.repository;

import com.imooc.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
public interface StudentRepository extends JpaRepository<Student, Integer> {
    public Student findByexamineeId(String examineeId);

    public List<Student> findByclassName(String classId);


}
