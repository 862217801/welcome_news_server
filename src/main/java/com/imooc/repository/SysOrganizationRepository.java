package com.imooc.repository;

import com.imooc.domain.Student;
import com.imooc.domain.SysOrganization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
public interface SysOrganizationRepository extends JpaRepository<SysOrganization, Integer> {
  public List<SysOrganization> findByPropertyIsNotNullAndPidIsNull();
}
