package com.imooc.repository;

import com.imooc.domain.Relationship;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BINGO on 2017/8/5 ${time}.
 */
public interface RelationshipRepository extends JpaRepository<Relationship,Integer> {

    public List<Relationship> findByregisterId(Integer id);

    public int deleteByregisterId(Integer id);
}
