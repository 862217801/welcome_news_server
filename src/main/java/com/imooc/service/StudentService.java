package com.imooc.service;

import com.imooc.domain.Student;
import com.imooc.repository.StudentRepository;
import com.imooc.repository.SysOrgRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/7/25.
 */
@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SysOrgRepository sysOrgRepository;

    public Student selectStudentById(String id) {
        Student student = studentRepository.findByexamineeId(id);
        if (student != null) {
            student.setCollegeName(sysOrgRepository.findOne(Integer.parseInt(student.getCollege())).getName());
            student.setDepartmentName(sysOrgRepository.findOne(Integer.parseInt(student.getDepartment())).getName());
            student.setClassName(sysOrgRepository.findOne(Integer.parseInt(student.getClassName())).getName());
            student.setMajorName(sysOrgRepository.findOne(Integer.parseInt(student.getMajor())).getName());
        }
        return student;
    }



}
