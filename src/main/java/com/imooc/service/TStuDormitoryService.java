package com.imooc.service;

import com.imooc.domain.Payment;
import com.imooc.domain.Student;
import com.imooc.domain.TStuDormitory;
import com.imooc.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
@Service
public class TStuDormitoryService {
    @Autowired
    private TStuDormitoryRepository tStuDormitoryRepository;
    @Autowired
    private TBedRepository tBedRepository;
    @Autowired
    private TBuildRepository tBuildRepository;
    @Autowired
    private TRoomRepository tRoomRepository;


public TStuDormitory selectdormitory(String examineeId){
    TStuDormitory tStuDormitory =tStuDormitoryRepository.findByExamineeId(examineeId);
    tStuDormitory.setBedName(tBedRepository.findById(tStuDormitory.getBedId()).getRemark());
    tStuDormitory.setBuildName(tBuildRepository.findById(tStuDormitory.getBuildId()).getName());
    tStuDormitory.setRoomName(tRoomRepository.findById(tStuDormitory.getRoomId()).getName());
    return tStuDormitory;
}
}
