package com.imooc.service;

import com.imooc.domain.Payment;
import com.imooc.domain.Student;
import com.imooc.repository.PaymentRepository;
import com.imooc.repository.StudentRepository;
import com.imooc.repository.SysOrgRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/7/25.
 */
@Service
public class PaymentService {
@Autowired
private PaymentRepository paymentRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private SysOrgRepository sysOrgRepository;
    public List<Payment> findByexamineeId(String id) {
        List<Payment> paymentList = paymentRepository.findByexamineeId(id);
        for (Payment p:paymentList
             ) {
            Student student = studentRepository.findByexamineeId(id);
            p.setXm(student.getName());
            p.setClassName(sysOrgRepository.findOne(Integer.parseInt(student.getClassName())).getName());
        }
        return paymentList;
    }
}
