package com.imooc.service;

import com.imooc.domain.PreRegist;
import com.imooc.domain.Relationship;
import com.imooc.repository.PreRegistRepository;
import com.imooc.repository.RelationshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by BINGO on 2017/8/9 ${time}.
 */
@Service
public class PreRegistService {

    @Autowired
    private PreRegistRepository preRegistRepository;

    @Autowired
    private RelationshipRepository relationshipRepository;

    @Transactional
    public void saveAll(PreRegist preRegist, List<Relationship> relationshipList){
        preRegistRepository.save(preRegist);
        relationshipRepository.deleteByregisterId(preRegist.getId());
        for (Relationship rs:relationshipList){
            rs.setRegisterId(preRegist.getId());
            relationshipRepository.save(rs);
        }
    }

}
