/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.19_3306
Source Server Version : 50528
Source Host           : 192.168.1.19:3306
Source Database       : welcomenews

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2017-08-05 17:35:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_data
-- ----------------------------
DROP TABLE IF EXISTS `base_data`;
CREATE TABLE `base_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dd_code` varchar(255) DEFAULT NULL COMMENT '编码（唯一）',
  `dd_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `dd_type_code` varchar(255) DEFAULT NULL COMMENT '类别编码（对应组件）',
  `dd_type_name` varchar(255) DEFAULT NULL COMMENT '类别名称（对应组件）',
  `pp_code` varchar(255) DEFAULT NULL COMMENT '父类编码',
  `dd_sort` varchar(255) DEFAULT NULL COMMENT '排序',
  `dd_level` varchar(255) DEFAULT NULL COMMENT '级别',
  `dd_exp1` varchar(255) DEFAULT NULL,
  `dd_exp2` varchar(255) DEFAULT NULL,
  `dd_exp3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=851 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_data
-- ----------------------------
INSERT INTO `base_data` VALUES ('1', '101', '中共党员', '1', '政治面貌', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('2', '102', '中共预备党员', '1', '政治面貌', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('3', '103', '共青团员', '1', '政治面貌', '0', '3', '1', null, null, null);
INSERT INTO `base_data` VALUES ('4', '104', '民革会员', '1', '政治面貌', '0', '4', '1', '', '', '');
INSERT INTO `base_data` VALUES ('5', '105', '民盟盟员', '1', '政治面貌', '0', '5', '1', '', '', '');
INSERT INTO `base_data` VALUES ('6', '106', '民建会员', '1', '政治面貌', '0', '6', '1', '', '', '');
INSERT INTO `base_data` VALUES ('7', '107', '民进会员', '1', '政治面貌', '0', '7', '1', '', '', '');
INSERT INTO `base_data` VALUES ('8', '108', '农工党党员', '1', '政治面貌', '0', '8', '1', '', '', '');
INSERT INTO `base_data` VALUES ('9', '109', '致工党党员', '1', '政治面貌', '0', '9', '1', '', '', '');
INSERT INTO `base_data` VALUES ('10', '110', '九三学社社员', '1', '政治面貌', '0', '10', '1', '', '', '');
INSERT INTO `base_data` VALUES ('11', '111', '台盟盟员', '1', '政治面貌', '0', '11', '1', '', '', '');
INSERT INTO `base_data` VALUES ('12', '112', '无党派人士', '1', '政治面貌', '0', '12', '1', '', '', '');
INSERT INTO `base_data` VALUES ('13', '113', '群众', '1', '政治面貌', '0', '13', '1', '', '', '');
INSERT INTO `base_data` VALUES ('14', '201', '博士研究生', '2', '学历', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('15', '202', '硕士研究生', '2', '学历', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('16', '203', '大学本科', '2', '学历', '0', '3', '1', '', '', '');
INSERT INTO `base_data` VALUES ('17', '204', '大学专科', '2', '学历', '0', '4', '1', '', '', '');
INSERT INTO `base_data` VALUES ('18', '205', '中专技校', '2', '学历', '0', '5', '1', '', '', '');
INSERT INTO `base_data` VALUES ('19', '206', '中等专科', '2', '学历', '0', '6', '1', '', '', '');
INSERT INTO `base_data` VALUES ('20', '207', '职业高中', '2', '学历', '0', '7', '1', '', '', '');
INSERT INTO `base_data` VALUES ('21', '208', '技工学校', '2', '学历', '0', '8', '1', '', '', '');
INSERT INTO `base_data` VALUES ('22', '209', '普通高中', '2', '学历', '0', '9', '1', '', '', '');
INSERT INTO `base_data` VALUES ('23', '210', '其他', '2', '学历', '0', '10', '1', '', '', '');
INSERT INTO `base_data` VALUES ('24', '301', '博士', '3', '学位', '0', '1', '1', '', '', '');
INSERT INTO `base_data` VALUES ('25', '302', '硕士', '3', '学位', '0', '2', '1', '', '', '');
INSERT INTO `base_data` VALUES ('26', '303', '学士', '3', '学位', '0', '3', '1', '', '', '');
INSERT INTO `base_data` VALUES ('27', '401', '一级', '4', '岗位等级', '0', '1', '1', '', '', '');
INSERT INTO `base_data` VALUES ('28', '402', '二级', '4', '岗位等级', '0', '2', '1', '', '', '');
INSERT INTO `base_data` VALUES ('29', '403', '三级', '4', '岗位等级', '0', '3', '1', '', '', '');
INSERT INTO `base_data` VALUES ('30', '404', '四级', '4', '岗位等级', '0', '4', '1', '', '', '');
INSERT INTO `base_data` VALUES ('31', '405', '五级', '4', '岗位等级', '0', '5', '1', '', '', '');
INSERT INTO `base_data` VALUES ('32', '406', '六级', '4', '岗位等级', '0', '6', '1', '', '', '');
INSERT INTO `base_data` VALUES ('33', '407', '七级', '4', '岗位等级', '0', '7', '1', '', '', '');
INSERT INTO `base_data` VALUES ('34', '408', '八级', '4', '岗位等级', '0', '8', '1', '', '', '');
INSERT INTO `base_data` VALUES ('35', '409', '九级', '4', '岗位等级', '0', '9', '1', '', '', '');
INSERT INTO `base_data` VALUES ('36', '410', '十级', '4', '岗位等级', '0', '10', '1', '', '', '');
INSERT INTO `base_data` VALUES ('37', '411', '十一级', '4', '岗位等级', '0', '11', '1', '', '', '');
INSERT INTO `base_data` VALUES ('38', '412', '十二级', '4', '岗位等级', '0', '12', '1', '', '', '');
INSERT INTO `base_data` VALUES ('39', '413', '十三级', '4', '岗位等级', '0', '13', '1', '', '', '');
INSERT INTO `base_data` VALUES ('40', '414', '普通工', '4', '岗位等级', '0', '14', '1', '', '', '');
INSERT INTO `base_data` VALUES ('41', '415', '待定', '4', '岗位等级', '0', '15', '1', '', '', '');
INSERT INTO `base_data` VALUES ('42', '501', '管理岗位', '5', '岗位类别', '0', '1', '1', '', '', '');
INSERT INTO `base_data` VALUES ('43', '502', '专技岗位', '5', '岗位类别', '0', '2', '1', '', '', '');
INSERT INTO `base_data` VALUES ('44', '503', '工勤岗位', '5', '岗位类别', '0', '3', '1', '', '', '');
INSERT INTO `base_data` VALUES ('45', '601', '机械工程学院', '6', '学院', '0', '1', '1', '', '', '');
INSERT INTO `base_data` VALUES ('46', '602', '电气与电子工程学院', '6', '学院', '0', '2', '1', '', '', '');
INSERT INTO `base_data` VALUES ('47', '603', '材料与化学工程学院', '6', '学院', '0', '3', '1', '', '', '');
INSERT INTO `base_data` VALUES ('48', '604', '生物工程与食品学院', '6', '学院', '0', '4', '1', '', '', '');
INSERT INTO `base_data` VALUES ('49', '605', '计算机学院', '6', '学院', '0', '5', '1', '', '', '');
INSERT INTO `base_data` VALUES ('50', '606', '土木建筑与环境学院', '6', '学院', '0', '6', '1', '', '', '');
INSERT INTO `base_data` VALUES ('51', '607', '艺术设计学院', '6', '学院', '0', '7', '1', '', '', '');
INSERT INTO `base_data` VALUES ('52', '608', '工业设计学院', '6', '学院', '0', '8', '1', '', '', '');
INSERT INTO `base_data` VALUES ('53', '609', '经济与管理学院', '6', '学院', '0', '9', '1', '', '', '');
INSERT INTO `base_data` VALUES ('54', '610', '外国语学院', '6', '学院', '0', '10', '1', '', '', '');
INSERT INTO `base_data` VALUES ('55', '611', '理学院', '6', '学院', '0', '11', '1', '', '', '');
INSERT INTO `base_data` VALUES ('56', '612', '职业技术师范学院', '6', '学院', '0', '12', '1', '', '', '');
INSERT INTO `base_data` VALUES ('57', '701', '仪器科学与质量工程系', '7', '系别', '601', '1', '1', '', '', '');
INSERT INTO `base_data` VALUES ('58', '702', '机械设计系', '7', '系别', '601', '2', '1', '', '', '');
INSERT INTO `base_data` VALUES ('59', '703', '工业与制造工程系', '7', '系别', '601', '3', '1', '', '', '');
INSERT INTO `base_data` VALUES ('60', '704', '包装工程系', '7', '系别', '601', '4', '1', '', '', '');
INSERT INTO `base_data` VALUES ('61', '705', '电气工程系', '7', '系别', '602', '5', '1', '', '', '');
INSERT INTO `base_data` VALUES ('62', '706', '自动化系', '7', '系别', '602', '6', '1', '', '', '');
INSERT INTO `base_data` VALUES ('63', '707', '电子信息工程系', '7', '系别', '602', '7', '1', '', '', '');
INSERT INTO `base_data` VALUES ('64', '708', '通信工程系', '7', '系别', '602', '8', '1', '', '', '');
INSERT INTO `base_data` VALUES ('65', '709', '电子科学与技术系', '7', '系别', '602', '9', '1', '', '', '');
INSERT INTO `base_data` VALUES ('66', '710', '材料科学与工程系', '7', '系别', '603', '10', '1', '', '', '');
INSERT INTO `base_data` VALUES ('67', '711', '轻化工程系', '7', '系别', '603', '11', '1', '', '', '');
INSERT INTO `base_data` VALUES ('68', '712', '化学与化工系', '7', '系别', '603', '12', '1', '', '', '');
INSERT INTO `base_data` VALUES ('69', '713', '材料成型及控制工程系', '7', '系别', '603', '13', '1', '', '', '');
INSERT INTO `base_data` VALUES ('70', '714', '生物技术系', '7', '系别', '604', '14', '1', '', '', '');
INSERT INTO `base_data` VALUES ('71', '715', '生物工程系', '7', '系别', '604', '15', '1', '', '', '');
INSERT INTO `base_data` VALUES ('72', '716', '食品科学与工程系', '7', '系别', '604', '16', '1', '', '', '');
INSERT INTO `base_data` VALUES ('73', '717', '制药工程系', '7', '系别', '604', '17', '1', '', '', '');
INSERT INTO `base_data` VALUES ('74', '718', '计算机应用系', '7', '系别', '605', '18', '1', '', '', '');
INSERT INTO `base_data` VALUES ('75', '719', '软件工程系', '7', '系别', '605', '19', '1', '', '', '');
INSERT INTO `base_data` VALUES ('76', '720', '网络工程系', '7', '系别', '605', '20', '1', '', '', '');
INSERT INTO `base_data` VALUES ('77', '721', '建筑工程系', '7', '系别', '606', '21', '1', '', '', '');
INSERT INTO `base_data` VALUES ('78', '722', '防水材料系', '7', '系别', '606', '22', '1', '', '', '');
INSERT INTO `base_data` VALUES ('79', '723', '工程管理系', '7', '系别', '606', '23', '1', '', '', '');
INSERT INTO `base_data` VALUES ('80', '724', '道路与桥梁工程系', '7', '系别', '606', '24', '1', '', '', '');
INSERT INTO `base_data` VALUES ('81', '725', '建筑规划系', '7', '系别', '606', '25', '1', '', '', '');
INSERT INTO `base_data` VALUES ('82', '726', '风景园林系', '7', '系别', '606', '26', '1', '', '', '');
INSERT INTO `base_data` VALUES ('83', '727', '资源与环境工程系', '7', '系别', '606', '27', '1', '', '', '');
INSERT INTO `base_data` VALUES ('84', '728', '视觉传达设计系', '7', '系别', '607', '28', '1', '', '', '');
INSERT INTO `base_data` VALUES ('85', '729', '环境艺术设计系', '7', '系别', '607', '29', '1', '', '', '');
INSERT INTO `base_data` VALUES ('86', '730', '装饰艺术设计系', '7', '系别', '607', '30', '1', '', '', '');
INSERT INTO `base_data` VALUES ('87', '731', '数字媒体艺术系', '7', '系别', '607', '31', '1', '', '', '');
INSERT INTO `base_data` VALUES ('88', '732', '工业设计系', '7', '系别', '608', '32', '1', '', '', '');
INSERT INTO `base_data` VALUES ('89', '733', '产品设计系', '7', '系别', '608', '33', '1', '', '', '');
INSERT INTO `base_data` VALUES ('90', '734', '工商管理系', '7', '系别', '609', '34', '1', '', '', '');
INSERT INTO `base_data` VALUES ('91', '735', '会计系', '7', '系别', '609', '35', '1', '', '', '');
INSERT INTO `base_data` VALUES ('92', '736', '管理科学与工程系', '7', '系别', '609', '36', '1', '', '', '');
INSERT INTO `base_data` VALUES ('93', '737', '公共管理', '7', '系别', '609', '37', '1', '', '', '');
INSERT INTO `base_data` VALUES ('94', '738', '金融学系', '7', '系别', '609', '38', '1', '', '', '');
INSERT INTO `base_data` VALUES ('95', '739', '国际贸易系', '7', '系别', '609', '39', '1', '', '', '');
INSERT INTO `base_data` VALUES ('96', '740', '保险系', '7', '系别', '609', '40', '1', '', '', '');
INSERT INTO `base_data` VALUES ('97', '741', '能源经济系', '7', '系别', '609', '41', '1', '', '', '');
INSERT INTO `base_data` VALUES ('98', '742', '英语系', '7', '系别', '610', '42', '1', '', '', '');
INSERT INTO `base_data` VALUES ('99', '743', '商务英语系', '7', '系别', '610', '43', '1', '', '', '');
INSERT INTO `base_data` VALUES ('100', '744', '语言文化传播系', '7', '系别', '610', '44', '1', '', '', '');
INSERT INTO `base_data` VALUES ('101', '745', '光电子信息科学系', '7', '系别', '611', '45', '1', '', '', '');
INSERT INTO `base_data` VALUES ('102', '746', '数学课部', '7', '系别', '611', '46', '1', '', '', '');
INSERT INTO `base_data` VALUES ('103', '747', '信息与计算科学系', '7', '系别', '611', '47', '1', '', '', '');
INSERT INTO `base_data` VALUES ('104', '748', '电气工程师资系', '7', '系别', '612', '48', '1', '', '', '');
INSERT INTO `base_data` VALUES ('105', '749', '机械设计师资系', '7', '系别', '612', '49', '1', '', '', '');
INSERT INTO `base_data` VALUES ('106', '750', '电子商务师资系', '7', '系别', '612', '50', '1', '', '', '');
INSERT INTO `base_data` VALUES ('107', '751', '网络工程师资系', '7', '系别', '612', '51', '1', '', '', '');
INSERT INTO `base_data` VALUES ('108', '752', '国际贸易师资系', '7', '系别', '612', '52', '1', '', '', '');
INSERT INTO `base_data` VALUES ('109', '753', '计算机应用师资系', '7', '系别', '612', '53', '1', '', '', '');
INSERT INTO `base_data` VALUES ('110', '754', '土木工程师资系', '7', '系别', '612', '54', '1', '', '', '');
INSERT INTO `base_data` VALUES ('111', '755', '教育学系', '7', '系别', '612', '55', '1', '', '', '');
INSERT INTO `base_data` VALUES ('112', '801', '测控技术与仪器', '8', '专业', '701', '1', '1', '', '', '');
INSERT INTO `base_data` VALUES ('113', '802', '测控技术与仪器(产品质量工程)', '8', '专业', '701', '2', '1', '', '', '');
INSERT INTO `base_data` VALUES ('114', '803', '机械设计制造及其自动化', '8', '专业', '702', '3', '1', '', '', '');
INSERT INTO `base_data` VALUES ('115', '804', '农业机械化及其自动化', '8', '专业', '702', '4', '1', '', '', '');
INSERT INTO `base_data` VALUES ('116', '805', '工业工程', '8', '专业', '703', '5', '1', '', '', '');
INSERT INTO `base_data` VALUES ('117', '806', '包装工程', '8', '专业', '704', '6', '1', '', '', '');
INSERT INTO `base_data` VALUES ('118', '807', '电气工程及其自动化', '8', '专业', '705', '7', '1', '', '', '');
INSERT INTO `base_data` VALUES ('119', '808', '自动化', '8', '专业', '706', '8', '1', '', '', '');
INSERT INTO `base_data` VALUES ('120', '809', '电子信息工程', '8', '专业', '707', '9', '1', '', '', '');
INSERT INTO `base_data` VALUES ('121', '810', '通信工程', '8', '专业', '708', '10', '1', '', '', '');
INSERT INTO `base_data` VALUES ('122', '811', '电子科学与技术', '8', '专业', '709', '11', '1', '', '', '');
INSERT INTO `base_data` VALUES ('123', '812', '高分子材料与工程', '8', '专业', '710', '12', '1', '', '', '');
INSERT INTO `base_data` VALUES ('124', '813', '材料科学与工程', '8', '专业', '710', '13', '1', '', '', '');
INSERT INTO `base_data` VALUES ('125', '814', '轻化工程', '8', '专业', '711', '14', '1', '', '', '');
INSERT INTO `base_data` VALUES ('126', '815', '化学工程与工艺专业', '8', '专业', '712', '15', '1', '', '', '');
INSERT INTO `base_data` VALUES ('127', '816', '材料成型及控制工程', '8', '专业', '713', '16', '1', '', '', '');
INSERT INTO `base_data` VALUES ('128', '817', '生物工程(工学)', '8', '专业', '714', '17', '1', '', '', '');
INSERT INTO `base_data` VALUES ('129', '818', '生物技术(理学)', '8', '专业', '715', '18', '1', '', '', '');
INSERT INTO `base_data` VALUES ('130', '819', '生物科学(理学)', '8', '专业', '715', '19', '1', '', '', '');
INSERT INTO `base_data` VALUES ('131', '820', '酿酒工程(工学)', '8', '专业', '716', '20', '1', '', '', '');
INSERT INTO `base_data` VALUES ('132', '821', '食品科学与工程(工学)', '8', '专业', '716', '21', '1', '', '', '');
INSERT INTO `base_data` VALUES ('133', '822', '食品科学与工程(工学)(农副产品深加工)', '8', '专业', '716', '22', '1', '', '', '');
INSERT INTO `base_data` VALUES ('134', '823', '食品质量与安全专业', '8', '专业', '716', '23', '1', '', '', '');
INSERT INTO `base_data` VALUES ('135', '824', '制药工程(工学)', '8', '专业', '717', '24', '1', '', '', '');
INSERT INTO `base_data` VALUES ('136', '825', '计算机科学与技术', '8', '专业', '718', '25', '1', '', '', '');
INSERT INTO `base_data` VALUES ('137', '826', '软件工程', '8', '专业', '719', '26', '1', '', '', '');
INSERT INTO `base_data` VALUES ('138', '827', '网络工程', '8', '专业', '720', '27', '1', '', '', '');
INSERT INTO `base_data` VALUES ('139', '828', '数字媒体技术', '8', '专业', '718', '28', '1', '', '', '');
INSERT INTO `base_data` VALUES ('140', '829', '物联网工程', '8', '专业', '720', '29', '1', '', '', '');
INSERT INTO `base_data` VALUES ('141', '830', '信息安全', '8', '专业', '720', '30', '1', '', '', '');
INSERT INTO `base_data` VALUES ('142', '831', '土木工程', '8', '专业', '721', '31', '1', '', '', '');
INSERT INTO `base_data` VALUES ('143', '832', '土木工程(防水材料与工程)', '8', '专业', '722', '32', '1', '', '', '');
INSERT INTO `base_data` VALUES ('144', '833', '工程管理', '8', '专业', '723', '33', '1', '', '', '');
INSERT INTO `base_data` VALUES ('145', '834', '交通工程', '8', '专业', '724', '34', '1', '', '', '');
INSERT INTO `base_data` VALUES ('146', '835', '建筑学', '8', '专业', '725', '35', '1', '', '', '');
INSERT INTO `base_data` VALUES ('147', '836', '风景园林', '8', '专业', '726', '36', '1', '', '', '');
INSERT INTO `base_data` VALUES ('148', '837', '城乡规划', '8', '专业', '725', '37', '1', '', '', '');
INSERT INTO `base_data` VALUES ('149', '838', '建筑学(室内设计)', '8', '专业', '725', '38', '1', '', '', '');
INSERT INTO `base_data` VALUES ('150', '839', '环境工程', '8', '专业', '727', '39', '1', '', '', '');
INSERT INTO `base_data` VALUES ('151', '840', '环境生态工程', '8', '专业', '727', '40', '1', '', '', '');
INSERT INTO `base_data` VALUES ('152', '841', '水务工程', '8', '专业', '727', '41', '1', '', '', '');
INSERT INTO `base_data` VALUES ('153', '842', '视觉传达设计', '8', '专业', '728', '42', '1', '', '', '');
INSERT INTO `base_data` VALUES ('154', '843', '视觉传达设计(广告设计)', '8', '专业', '728', '43', '1', '', '', '');
INSERT INTO `base_data` VALUES ('155', '844', '环境设计', '8', '专业', '729', '44', '1', '', '', '');
INSERT INTO `base_data` VALUES ('156', '845', '环境设计(展示设计)', '8', '专业', '729', '45', '1', '', '', '');
INSERT INTO `base_data` VALUES ('157', '846', '公共艺术(装饰设计)', '8', '专业', '730', '46', '1', '', '', '');
INSERT INTO `base_data` VALUES ('158', '847', '数字媒体艺术', '8', '专业', '731', '47', '1', '', '', '');
INSERT INTO `base_data` VALUES ('159', '848', '工业设计专业（工科）', '8', '专业', '732', '48', '1', '', '', '');
INSERT INTO `base_data` VALUES ('160', '849', '产品设计', '8', '专业', '733', '49', '1', '', '', '');
INSERT INTO `base_data` VALUES ('161', '850', '产品设计(玩具设计)', '8', '专业', '733', '50', '1', '', '', '');
INSERT INTO `base_data` VALUES ('162', '851', '市场营销', '8', '专业', '734', '51', '1', '', '', '');
INSERT INTO `base_data` VALUES ('163', '852', '人力资源管理', '8', '专业', '734', '52', '1', '', '', '');
INSERT INTO `base_data` VALUES ('164', '853', '财务管理', '8', '专业', '734', '53', '1', '', '', '');
INSERT INTO `base_data` VALUES ('165', '854', '会计学', '8', '专业', '735', '54', '1', '', '', '');
INSERT INTO `base_data` VALUES ('166', '855', '信息管理与信息系统', '8', '专业', '736', '55', '1', '', '', '');
INSERT INTO `base_data` VALUES ('167', '856', '电子商务', '8', '专业', '736', '56', '1', '', '', '');
INSERT INTO `base_data` VALUES ('168', '857', '行政管理', '8', '专业', '737', '57', '1', '', '', '');
INSERT INTO `base_data` VALUES ('169', '858', '金融学', '8', '专业', '738', '58', '1', '', '', '');
INSERT INTO `base_data` VALUES ('170', '859', '金融学(数理金融双专业)', '8', '专业', '738', '59', '1', '', '', '');
INSERT INTO `base_data` VALUES ('171', '860', '国际经济与贸易', '8', '专业', '739', '60', '1', '', '', '');
INSERT INTO `base_data` VALUES ('172', '861', '国际经济与贸易(国际贸易英语双专业)', '8', '专业', '739', '61', '1', '', '', '');
INSERT INTO `base_data` VALUES ('173', '862', '保险学', '8', '专业', '740', '62', '1', '', '', '');
INSERT INTO `base_data` VALUES ('174', '863', '能源经济', '8', '专业', '741', '63', '1', '', '', '');
INSERT INTO `base_data` VALUES ('175', '864', '英语', '8', '专业', '742', '64', '1', '', '', '');
INSERT INTO `base_data` VALUES ('176', '865', '商务英语', '8', '专业', '743', '65', '1', '', '', '');
INSERT INTO `base_data` VALUES ('177', '866', '汉语国际教育', '8', '专业', '744', '66', '1', '', '', '');
INSERT INTO `base_data` VALUES ('178', '867', '电子信息科学与技术', '8', '专业', '745', '67', '1', '', '', '');
INSERT INTO `base_data` VALUES ('179', '868', '应用统计学', '8', '专业', '746', '68', '1', '', '', '');
INSERT INTO `base_data` VALUES ('180', '869', '信息与计算科学', '8', '专业', '747', '69', '1', '', '', '');
INSERT INTO `base_data` VALUES ('181', '870', '光电信息科学与工程', '8', '专业', '745', '70', '1', '', '', '');
INSERT INTO `base_data` VALUES ('182', '871', '电气工程及其自动化(师资)', '8', '专业', '748', '71', '1', '', '', '');
INSERT INTO `base_data` VALUES ('183', '872', '机械设计制造及其自动化(师资)', '8', '专业', '749', '72', '1', '', '', '');
INSERT INTO `base_data` VALUES ('184', '873', '电子商务(师资)', '8', '专业', '750', '73', '1', '', '', '');
INSERT INTO `base_data` VALUES ('185', '874', '网络工程（师资）', '8', '专业', '751', '74', '1', '', '', '');
INSERT INTO `base_data` VALUES ('186', '875', '国际经济与贸易（物流管理.师资）', '8', '专业', '752', '75', '1', '', '', '');
INSERT INTO `base_data` VALUES ('187', '876', '计算机科学与技术（师资）', '8', '专业', '753', '76', '1', '', '', '');
INSERT INTO `base_data` VALUES ('188', '877', '职业技术教育学', '8', '专业', '754', '77', '1', '', '', '');
INSERT INTO `base_data` VALUES ('711', '8501', '汉族', '85', '民族', '0', '1', '1', '', '', null);
INSERT INTO `base_data` VALUES ('712', '8502', '蒙古族', '85', '民族', '0', '2', '1', '', '', null);
INSERT INTO `base_data` VALUES ('713', '8503', '回族', '85', '民族', '0', '3', '1', '', '', null);
INSERT INTO `base_data` VALUES ('714', '8504', '藏族', '85', '民族', '0', '4', '1', '', '', null);
INSERT INTO `base_data` VALUES ('715', '8505', '维吾尔族', '85', '民族', '0', '5', '1', '', '', null);
INSERT INTO `base_data` VALUES ('716', '8506', '苗族', '85', '民族', '0', '6', '1', '', '', null);
INSERT INTO `base_data` VALUES ('717', '8507', '彝族', '85', '民族', '0', '7', '1', '', '', null);
INSERT INTO `base_data` VALUES ('718', '8508', '壮族', '85', '民族', '0', '8', '1', '', '', null);
INSERT INTO `base_data` VALUES ('719', '8509', '布依族', '85', '民族', '0', '9', '1', '', '', null);
INSERT INTO `base_data` VALUES ('720', '8510', '朝鲜族', '85', '民族', '0', '10', '1', '', '', null);
INSERT INTO `base_data` VALUES ('721', '8511', '满族', '85', '民族', '0', '11', '1', '', '', null);
INSERT INTO `base_data` VALUES ('722', '8512', '侗族', '85', '民族', '0', '12', '1', '', '', null);
INSERT INTO `base_data` VALUES ('723', '8513', '瑶族', '85', '民族', '0', '13', '1', '', '', null);
INSERT INTO `base_data` VALUES ('724', '8514', '白族', '85', '民族', '0', '14', '1', '', '', null);
INSERT INTO `base_data` VALUES ('725', '8515', '土家族', '85', '民族', '0', '15', '1', '', '', null);
INSERT INTO `base_data` VALUES ('726', '8516', '哈尼族', '85', '民族', '0', '16', '1', '', '', null);
INSERT INTO `base_data` VALUES ('727', '8517', '哈萨克族', '85', '民族', '0', '17', '1', '', '', null);
INSERT INTO `base_data` VALUES ('728', '8518', '傣族', '85', '民族', '0', '18', '1', '', '', null);
INSERT INTO `base_data` VALUES ('729', '8519', '黎族', '85', '民族', '0', '19', '1', '', '', null);
INSERT INTO `base_data` VALUES ('730', '8520', '傈僳族', '85', '民族', '0', '20', '1', '', '', null);
INSERT INTO `base_data` VALUES ('731', '8521', '佤族', '85', '民族', '0', '21', '1', '', '', null);
INSERT INTO `base_data` VALUES ('732', '8522', '畲族', '85', '民族', '0', '22', '1', '', '', null);
INSERT INTO `base_data` VALUES ('733', '8523', '高山族', '85', '民族', '0', '23', '1', '', '', null);
INSERT INTO `base_data` VALUES ('734', '8524', '拉祜族', '85', '民族', '0', '24', '1', '', '', null);
INSERT INTO `base_data` VALUES ('735', '8525', '水族', '85', '民族', '0', '25', '1', '', '', null);
INSERT INTO `base_data` VALUES ('736', '8526', '东乡族', '85', '民族', '0', '26', '1', '', '', null);
INSERT INTO `base_data` VALUES ('737', '8527', '纳西族', '85', '民族', '0', '27', '1', '', '', null);
INSERT INTO `base_data` VALUES ('738', '8528', '景颇族', '85', '民族', '0', '28', '1', '', '', null);
INSERT INTO `base_data` VALUES ('739', '8529', '柯尔克孜族', '85', '民族', '0', '29', '1', '', '', null);
INSERT INTO `base_data` VALUES ('740', '8530', '土族', '85', '民族', '0', '30', '1', '', '', null);
INSERT INTO `base_data` VALUES ('741', '8531', '达斡尔族', '85', '民族', '0', '31', '1', '', '', null);
INSERT INTO `base_data` VALUES ('742', '8532', '仫佬族', '85', '民族', '0', '32', '1', '', '', null);
INSERT INTO `base_data` VALUES ('743', '8533', '羌族', '85', '民族', '0', '33', '1', '', '', null);
INSERT INTO `base_data` VALUES ('744', '8534', '布朗族', '85', '民族', '0', '34', '1', '', '', null);
INSERT INTO `base_data` VALUES ('745', '8535', '撒拉族', '85', '民族', '0', '35', '1', '', '', null);
INSERT INTO `base_data` VALUES ('746', '8536', '毛南族', '85', '民族', '0', '36', '1', '', '', null);
INSERT INTO `base_data` VALUES ('747', '8537', '仡佬族', '85', '民族', '0', '37', '1', '', '', null);
INSERT INTO `base_data` VALUES ('748', '8538', '锡伯族', '85', '民族', '0', '38', '1', '', '', null);
INSERT INTO `base_data` VALUES ('749', '8539', '阿昌族', '85', '民族', '0', '39', '1', '', '', null);
INSERT INTO `base_data` VALUES ('750', '8540', '普米族', '85', '民族', '0', '40', '1', '', '', null);
INSERT INTO `base_data` VALUES ('751', '8541', '塔吉克族', '85', '民族', '0', '41', '1', '', '', null);
INSERT INTO `base_data` VALUES ('752', '8542', '怒族', '85', '民族', '0', '42', '1', '', '', null);
INSERT INTO `base_data` VALUES ('753', '8543', '乌孜别克族', '85', '民族', '0', '43', '1', '', '', null);
INSERT INTO `base_data` VALUES ('754', '8544', '俄罗斯族', '85', '民族', '0', '44', '1', '', '', null);
INSERT INTO `base_data` VALUES ('755', '8545', '鄂温克族', '85', '民族', '0', '45', '1', '', '', null);
INSERT INTO `base_data` VALUES ('756', '8546', '德昂族', '85', '民族', '0', '46', '1', '', '', null);
INSERT INTO `base_data` VALUES ('757', '8547', '保安族', '85', '民族', '0', '47', '1', '', '', null);
INSERT INTO `base_data` VALUES ('758', '8548', '裕固族', '85', '民族', '0', '48', '1', '', '', null);
INSERT INTO `base_data` VALUES ('759', '8549', '京族', '85', '民族', '0', '49', '1', '', '', null);
INSERT INTO `base_data` VALUES ('760', '8550', '塔塔尔族', '85', '民族', '0', '50', '1', '', '', null);
INSERT INTO `base_data` VALUES ('761', '8551', '独龙族', '85', '民族', '0', '51', '1', '', '', null);
INSERT INTO `base_data` VALUES ('762', '8552', '鄂伦春族', '85', '民族', '0', '52', '1', '', '', null);
INSERT INTO `base_data` VALUES ('763', '8553', '赫哲族', '85', '民族', '0', '53', '1', '', '', null);
INSERT INTO `base_data` VALUES ('764', '8554', '门巴族', '85', '民族', '0', '54', '1', '', '', null);
INSERT INTO `base_data` VALUES ('765', '8555', '珞巴族', '85', '民族', '0', '55', '1', '', '', null);
INSERT INTO `base_data` VALUES ('766', '8556', '基诺族', '85', '民族', '0', '56', '1', '', '', null);
INSERT INTO `base_data` VALUES ('768', '8602', '父亲', '86', '称谓', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('769', '8603', '母亲', '86', '称谓', '0', '3', '1', null, null, null);
INSERT INTO `base_data` VALUES ('770', '8604', '姐姐', '86', '称谓', '0', '4', '1', null, null, null);
INSERT INTO `base_data` VALUES ('771', '8605', '妹妹', '86', '称谓', '0', '5', '1', null, null, null);
INSERT INTO `base_data` VALUES ('772', '8606', '哥哥', '86', '称谓', '0', '6', '1', null, null, null);
INSERT INTO `base_data` VALUES ('773', '8607', '弟弟', '86', '称谓', '0', '7', '1', null, null, null);
INSERT INTO `base_data` VALUES ('774', '8608', '祖父母', '86', '称谓', '0', '8', '1', null, null, null);
INSERT INTO `base_data` VALUES ('775', '8609', '外祖父母', '86', '称谓', '0', '9', '1', null, null, null);
INSERT INTO `base_data` VALUES ('776', '8610', '其他亲属', '86', '称谓', '0', '10', '1', null, null, null);
INSERT INTO `base_data` VALUES ('777', '801', '已开发产品', '8', '产品状态', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('778', '802', '在开发产品', '8', '产品状态', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('779', '901', '政府', '9', '客户类别', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('780', '902', '企业', '9', '客户类别', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('781', '1001', '行业1', '10', '客户所属行业', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('782', '1002', '行业2', '10', '客户所属行业', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('783', '1101', '一级', '11', '联系人紧密度', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('784', '1102', '二级', '11', '联系人紧密度', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('785', '1103', '三级', '11', '联系人紧密度', '0', '3', '1', null, null, null);
INSERT INTO `base_data` VALUES ('786', '1201', '研讨会', '12', '商机来源', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('787', '1202', '拜访', '12', '商机来源', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('788', '1203', '客户介绍', '12', '商机来源', '0', '3', '1', null, null, null);
INSERT INTO `base_data` VALUES ('789', '1301', '新客户商机', '13', '商机类型', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('790', '1302', '老客户商机', '13', '商机类型', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('791', '1401', '不提醒', '14', '定时提醒', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('792', '1402', '准时', '14', '定时提醒', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('793', '1403', '提前5分钟', '14', '定时提醒', '0', '3', '1', null, null, null);
INSERT INTO `base_data` VALUES ('794', '1404', '提前10分钟', '14', '定时提醒', '0', '4', '1', null, null, null);
INSERT INTO `base_data` VALUES ('795', '1501', '申请绿色通道', '15', '申请类别', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('796', '1502', '申请延迟报到', '15', '申请类别', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('798', '1601', '一级', '16', '项目级别', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('799', '1602', '二级', '16', '项目级别', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('800', '1603', '三级', '16', '项目级别', '0', '3', '1', null, null, null);
INSERT INTO `base_data` VALUES ('801', '1701', '值班人', '17', '人员类别', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('802', '1702', '负责人', '17', '人员类别', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('803', '1801', '天河机场', '18', '乘车点', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('804', '1802', '武昌站', '18', '乘车点', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('805', '1803', '武汉站', '18', '乘车点', '0', '3', '1', null, null, null);
INSERT INTO `base_data` VALUES ('806', '1804', '汉口站', '18', '乘车点', '0', '4', '1', null, null, null);
INSERT INTO `base_data` VALUES ('807', '1901', '男', '19', '性别', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('808', '1902', '女', '19', '性别', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('810', '2002', '班导师', '20', '类别（辅导员、班导师）', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('811', '2101', '学院', '21', '部门类别', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('812', '2102', '系别', '21', '部门类别', '0', '2', '1', null, null, null);
INSERT INTO `base_data` VALUES ('813', '2103', '专业', '21', '部门类别', '0', '3', '1', null, null, null);
INSERT INTO `base_data` VALUES ('814', '2104', '班级', '21', '部门类别', '0', '4', '1', null, null, null);
INSERT INTO `base_data` VALUES ('815', '2201', '北京市', '22', '省份', '0', '1', '1', '北京', '', '');
INSERT INTO `base_data` VALUES ('816', '2202', '天津市', '22', '省份', '0', '2', '1', '天津', '', '');
INSERT INTO `base_data` VALUES ('817', '2203', '河北省', '22', '省份', '0', '3', '1', '河北', '', '');
INSERT INTO `base_data` VALUES ('818', '2204', '山西省', '22', '省份', '0', '4', '1', '山西', '', '');
INSERT INTO `base_data` VALUES ('819', '2205', '内蒙古自治区', '22', '省份', '0', '5', '1', '内蒙古', '', '');
INSERT INTO `base_data` VALUES ('820', '2206', '辽宁省', '22', '省份', '0', '6', '1', '辽宁', '', '');
INSERT INTO `base_data` VALUES ('821', '2207', '吉林省', '22', '省份', '0', '7', '1', '吉林', '', '');
INSERT INTO `base_data` VALUES ('822', '2208', '黑龙江省', '22', '省份', '0', '8', '1', '黑龙江', '', '');
INSERT INTO `base_data` VALUES ('823', '2209', '上海市', '22', '省份', '0', '9', '1', '上海', '', '');
INSERT INTO `base_data` VALUES ('824', '2210', '江苏省', '22', '省份', '0', '10', '1', '江苏', '', '');
INSERT INTO `base_data` VALUES ('825', '2211', '浙江省', '22', '省份', '0', '11', '1', '浙江', '', '');
INSERT INTO `base_data` VALUES ('826', '2212', '安徽省', '22', '省份', '0', '12', '1', '安徽', '', '');
INSERT INTO `base_data` VALUES ('827', '2213', '福建省', '22', '省份', '0', '13', '1', '福建', '', '');
INSERT INTO `base_data` VALUES ('828', '2214', '江西省', '22', '省份', '0', '14', '1', '江西', '', '');
INSERT INTO `base_data` VALUES ('829', '2215', '山东省', '22', '省份', '0', '15', '1', '山东', '', '');
INSERT INTO `base_data` VALUES ('830', '2216', '河南省', '22', '省份', '0', '16', '1', '河南', '', '');
INSERT INTO `base_data` VALUES ('831', '2217', '湖北省', '22', '省份', '0', '17', '1', '湖北', '', '');
INSERT INTO `base_data` VALUES ('832', '2218', '湖南省', '22', '省份', '0', '18', '1', '湖南', '', '');
INSERT INTO `base_data` VALUES ('833', '2219', '广东省', '22', '省份', '0', '19', '1', '广东', '', '');
INSERT INTO `base_data` VALUES ('834', '2220', '广西壮族自治区', '22', '省份', '0', '20', '1', '广西', '', '');
INSERT INTO `base_data` VALUES ('835', '2221', '海南省', '22', '省份', '0', '21', '1', '海南', '', '');
INSERT INTO `base_data` VALUES ('836', '2222', '重庆市', '22', '省份', '0', '22', '1', '重庆', '', '');
INSERT INTO `base_data` VALUES ('837', '2223', '四川省', '22', '省份', '0', '23', '1', '四川', '', '');
INSERT INTO `base_data` VALUES ('838', '2224', '贵州省', '22', '省份', '0', '24', '1', '贵州', '', '');
INSERT INTO `base_data` VALUES ('839', '2225', '云南省', '22', '省份', '0', '25', '1', '云南', '', '');
INSERT INTO `base_data` VALUES ('840', '2226', '西藏自治区', '22', '省份', '0', '26', '1', '西藏', '', '');
INSERT INTO `base_data` VALUES ('841', '2227', '陕西省', '22', '省份', '0', '27', '1', '陕西', '', '');
INSERT INTO `base_data` VALUES ('842', '2228', '甘肃省', '22', '省份', '0', '28', '1', '甘肃', '', '');
INSERT INTO `base_data` VALUES ('843', '2229', '青海省', '22', '省份', '0', '29', '1', '青海', '', '');
INSERT INTO `base_data` VALUES ('844', '2230', '宁夏回族自治区', '22', '省份', '0', '30', '1', '宁夏', '', '');
INSERT INTO `base_data` VALUES ('845', '2231', '新疆维吾尔自治区', '22', '省份', '0', '31', '1', '新疆', '', '');
INSERT INTO `base_data` VALUES ('846', '2232', '台湾省', '22', '省份', '0', '32', '1', '台湾', '', '');
INSERT INTO `base_data` VALUES ('847', '2233', '香港特别行政区', '22', '省份', '0', '33', '1', '香港', '', '');
INSERT INTO `base_data` VALUES ('848', '2234', '澳门特别行政区', '22', '省份', '0', '34', '1', '澳门', '', '');
INSERT INTO `base_data` VALUES ('849', '2301', '办理点位', '23', '点位类别', '0', '1', '1', null, null, null);
INSERT INTO `base_data` VALUES ('850', '2302', '标识点位', '23', '点位类别', '0', '2', '1', null, null, null);

-- ----------------------------
-- Table structure for girl
-- ----------------------------
DROP TABLE IF EXISTS `girl`;
CREATE TABLE `girl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(11) DEFAULT NULL,
  `cup_size` varchar(255) NOT NULL,
  `money` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of girl
-- ----------------------------

-- ----------------------------
-- Table structure for spot_and_matter
-- ----------------------------
DROP TABLE IF EXISTS `spot_and_matter`;
CREATE TABLE `spot_and_matter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spot_id` int(11) DEFAULT NULL,
  `matter_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of spot_and_matter
-- ----------------------------
INSERT INTO `spot_and_matter` VALUES ('105', '4', '7');
INSERT INTO `spot_and_matter` VALUES ('106', '4', '8');
INSERT INTO `spot_and_matter` VALUES ('107', '4', '9');
INSERT INTO `spot_and_matter` VALUES ('108', '4', '10');
INSERT INTO `spot_and_matter` VALUES ('109', '4', '11');
INSERT INTO `spot_and_matter` VALUES ('110', '4', '12');
INSERT INTO `spot_and_matter` VALUES ('111', '14', '11');
INSERT INTO `spot_and_matter` VALUES ('112', '14', '12');
INSERT INTO `spot_and_matter` VALUES ('113', '17', '7');
INSERT INTO `spot_and_matter` VALUES ('114', '17', '8');
INSERT INTO `spot_and_matter` VALUES ('115', '17', '9');
INSERT INTO `spot_and_matter` VALUES ('116', '17', '10');
INSERT INTO `spot_and_matter` VALUES ('117', '17', '11');
INSERT INTO `spot_and_matter` VALUES ('118', '17', '12');
INSERT INTO `spot_and_matter` VALUES ('119', '18', '7');
INSERT INTO `spot_and_matter` VALUES ('120', '18', '8');
INSERT INTO `spot_and_matter` VALUES ('121', '18', '9');
INSERT INTO `spot_and_matter` VALUES ('122', '18', '10');
INSERT INTO `spot_and_matter` VALUES ('123', '18', '11');
INSERT INTO `spot_and_matter` VALUES ('124', '18', '12');

-- ----------------------------
-- Table structure for sys_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `text` varchar(64) NOT NULL,
  `dictionarytype_id` int(11) NOT NULL,
  `seq` tinyint(1) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_51htkeif203365pkc74w3388d` (`dictionarytype_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='数据字典';

-- ----------------------------
-- Records of sys_dictionary
-- ----------------------------
INSERT INTO `sys_dictionary` VALUES ('1', '0', '系统管理', '2', '0', '0', '0');
INSERT INTO `sys_dictionary` VALUES ('9', '1', '教职工用户', '2', '1', '0', '1');
INSERT INTO `sys_dictionary` VALUES ('14', '2', '学生用户', '2', '0', '0', '1');

-- ----------------------------
-- Table structure for sys_dictionarytype
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionarytype`;
CREATE TABLE `sys_dictionarytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `seq` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pw36d8tecu9ljc326r141ewqe` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='字典分类';

-- ----------------------------
-- Records of sys_dictionarytype
-- ----------------------------
INSERT INTO `sys_dictionarytype` VALUES ('1', 'base', '基础设置', '0', '基础设置', null);
INSERT INTO `sys_dictionarytype` VALUES ('2', 'usertype', '用户类型', '0', '用户类型', '1');

-- ----------------------------
-- Table structure for sys_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_organization`;
CREATE TABLE `sys_organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `code` varchar(64) NOT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `seq` tinyint(255) NOT NULL DEFAULT '0',
  `createdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `property` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7fssu67fw54bf6fbo1iwr756b` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=281 DEFAULT CHARSET=utf8 COMMENT='组织机构';

-- ----------------------------
-- Records of sys_organization
-- ----------------------------
INSERT INTO `sys_organization` VALUES ('1', '信息维护', '地址', '01', 'icon_company', null, '0', '2016-11-14 11:10:28', null);
INSERT INTO `sys_organization` VALUES ('275', '电气1', '', '08010101', 'icon_folder', '200', '0', '2017-06-29 17:39:14', '2104');
INSERT INTO `sys_organization` VALUES ('276', '轻化1', '', '09020101', 'icon_folder', '207', '0', '2017-06-29 17:40:55', '2104');
INSERT INTO `sys_organization` VALUES ('121', '学工部', '', '06', 'icon_folder', null, '0', '2017-06-26 15:37:01', null);
INSERT INTO `sys_organization` VALUES ('274', '测控2', '', '07010102', 'icon_folder', '194', '0', '2017-06-29 17:38:09', '2104');
INSERT INTO `sys_organization` VALUES ('128', '机械工程学院', '', '07', 'icon_folder', null, '0', '2017-06-29 14:46:41', '2101');
INSERT INTO `sys_organization` VALUES ('129', '电气与电子工程学院', '', '08', 'icon_folder', null, '0', '2017-06-29 14:50:30', '2101');
INSERT INTO `sys_organization` VALUES ('130', '材料与化学工程学院', '', '09', 'icon_folder', null, '0', '2017-06-29 14:50:51', '2101');
INSERT INTO `sys_organization` VALUES ('131', '生物工程与食品学院', '', '10', 'icon_folder', null, '0', '2017-06-29 14:51:26', '2101');
INSERT INTO `sys_organization` VALUES ('132', '计算机学院', '', '11', 'icon_folder', null, '0', '2017-06-29 14:52:28', '2101');
INSERT INTO `sys_organization` VALUES ('133', '土木建筑与环境学院', '', '12', 'icon_folder', null, '0', '2017-06-29 14:53:00', '2101');
INSERT INTO `sys_organization` VALUES ('134', '艺术设计学院', '', '13', 'icon_folder', null, '0', '2017-06-29 14:53:30', '2101');
INSERT INTO `sys_organization` VALUES ('135', '工业设计学院', '', '14', 'icon_folder', null, '0', '2017-06-29 14:54:12', '2101');
INSERT INTO `sys_organization` VALUES ('136', '经济与管理学院', '', '15', 'icon_folder', null, '0', '2017-06-29 14:54:28', '2101');
INSERT INTO `sys_organization` VALUES ('137', '外国语学院', '', '16', 'icon_folder', null, '0', '2017-06-29 14:54:43', '2101');
INSERT INTO `sys_organization` VALUES ('138', '理学院', '', '17', 'icon_folder', null, '0', '2017-06-29 14:55:00', '2101');
INSERT INTO `sys_organization` VALUES ('139', '职业技术师范学院', '', '18', 'icon_folder', null, '0', '2017-06-29 14:55:16', '2101');
INSERT INTO `sys_organization` VALUES ('140', '仪器科学与质量工程系', '', '0701', 'icon_folder', '128', '0', '2017-06-29 14:55:51', '2102');
INSERT INTO `sys_organization` VALUES ('141', '机械设计系', '', '0702', 'icon_folder', '128', '0', '2017-06-29 14:56:59', '2102');
INSERT INTO `sys_organization` VALUES ('142', '工业与制造工程系', '', '0703', 'icon_folder', '128', '0', '2017-06-29 14:57:20', '2102');
INSERT INTO `sys_organization` VALUES ('143', '包装工程系', '', '0704', 'icon_folder', '128', '0', '2017-06-29 14:57:57', '2102');
INSERT INTO `sys_organization` VALUES ('144', '电气工程系', '', '0801', 'icon_folder', '129', '0', '2017-06-29 15:00:39', '2102');
INSERT INTO `sys_organization` VALUES ('145', '自动化系', '', '0802', 'icon_folder', '129', '0', '2017-06-29 15:01:03', '2102');
INSERT INTO `sys_organization` VALUES ('146', '电子信息工程系', '', '0803', 'icon_folder', '129', '0', '2017-06-29 15:01:57', '2102');
INSERT INTO `sys_organization` VALUES ('147', '通信工程系', '', '0804', 'icon_folder', '129', '0', '2017-06-29 15:02:48', '2102');
INSERT INTO `sys_organization` VALUES ('148', '电子科学与技术系', '', '0805', 'icon_folder', '129', '0', '2017-06-29 15:03:03', '2102');
INSERT INTO `sys_organization` VALUES ('149', '材料科学与工程系', '', '0901', 'icon_folder', '130', '0', '2017-06-29 15:03:39', '2102');
INSERT INTO `sys_organization` VALUES ('150', '轻化工程系', '', '0902', 'icon_folder', '130', '0', '2017-06-29 15:03:56', '2102');
INSERT INTO `sys_organization` VALUES ('151', '化学与化工系', '', '0903', 'icon_folder', '130', '0', '2017-06-29 15:04:11', '2102');
INSERT INTO `sys_organization` VALUES ('152', '材料成型及控制工程系', '', '0904', 'icon_folder', '130', '0', '2017-06-29 15:04:26', '2102');
INSERT INTO `sys_organization` VALUES ('153', '生物工程系', '', '1001', 'icon_folder', '131', '0', '2017-06-29 15:04:50', '2102');
INSERT INTO `sys_organization` VALUES ('154', '生物技术系', '', '1002', 'icon_folder', '131', '0', '2017-06-29 15:05:11', '2102');
INSERT INTO `sys_organization` VALUES ('155', '食品科学与工程系', '', '1003', 'icon_folder', '131', '0', '2017-06-29 15:05:26', '2102');
INSERT INTO `sys_organization` VALUES ('156', '制药工程系', '', '1005', 'icon_folder', '131', '0', '2017-06-29 15:05:43', '2102');
INSERT INTO `sys_organization` VALUES ('157', '计算机应用系', '', '1101', 'icon_folder', '132', '0', '2017-06-29 15:06:15', '2102');
INSERT INTO `sys_organization` VALUES ('158', '软件工程系', '', '1102', 'icon_folder', '132', '0', '2017-06-29 15:06:29', '2102');
INSERT INTO `sys_organization` VALUES ('159', '网络工程系', '', '1103', 'icon_folder', '132', '0', '2017-06-29 15:06:45', '2102');
INSERT INTO `sys_organization` VALUES ('160', '建筑工程系', '', '1201', 'icon_folder', '133', '0', '2017-06-29 15:07:02', '2102');
INSERT INTO `sys_organization` VALUES ('161', '防水材料系', '', '1202', 'icon_folder', '133', '0', '2017-06-29 15:07:30', '2102');
INSERT INTO `sys_organization` VALUES ('162', '工程管理系', '', '1203', 'icon_folder', '133', '0', '2017-06-29 15:07:47', '2102');
INSERT INTO `sys_organization` VALUES ('163', '道路与桥梁工程系', '', '1204', 'icon_folder', '133', '0', '2017-06-29 15:08:13', '2102');
INSERT INTO `sys_organization` VALUES ('164', '建筑规划系', '', '1205', 'icon_folder', '133', '0', '2017-06-29 15:08:26', '2102');
INSERT INTO `sys_organization` VALUES ('165', '风景园林系', '', '1206', 'icon_folder', '133', '0', '2017-06-29 15:08:48', '2102');
INSERT INTO `sys_organization` VALUES ('166', '资源与环境工程系', '', '1207', 'icon_folder', '133', '0', '2017-06-29 15:09:12', '2102');
INSERT INTO `sys_organization` VALUES ('167', '视觉传达设计系', '', '1301', 'icon_folder', '134', '0', '2017-06-29 15:09:40', '2102');
INSERT INTO `sys_organization` VALUES ('168', '环境艺术设计系', '', '1302', 'icon_folder', '134', '0', '2017-06-29 15:09:55', '2102');
INSERT INTO `sys_organization` VALUES ('169', '装饰艺术设计系', '', '1303', 'icon_folder', '134', '0', '2017-06-29 15:10:21', '2102');
INSERT INTO `sys_organization` VALUES ('170', '数字媒体艺术系', '', '1304', 'icon_folder', '134', '0', '2017-06-29 15:10:40', '2102');
INSERT INTO `sys_organization` VALUES ('171', '工业设计系', '', '1401', 'icon_folder', '135', '0', '2017-06-29 15:11:42', '2102');
INSERT INTO `sys_organization` VALUES ('172', '产品设计系', '', '1402', 'icon_folder', '135', '0', '2017-06-29 15:11:59', '2102');
INSERT INTO `sys_organization` VALUES ('173', '工商管理系', '', '1501', 'icon_folder', '136', '0', '2017-06-29 15:12:17', '2102');
INSERT INTO `sys_organization` VALUES ('174', '会计系', '', '1502', 'icon_folder', '136', '0', '2017-06-29 15:12:31', '2102');
INSERT INTO `sys_organization` VALUES ('175', '管理科学与工程系', '', '1503', 'icon_folder', '136', '0', '2017-06-29 15:12:51', '2102');
INSERT INTO `sys_organization` VALUES ('176', '公共管理系', '', '1504', 'icon_folder', '136', '0', '2017-06-29 15:15:06', '2102');
INSERT INTO `sys_organization` VALUES ('177', '金融学系', '', '1505', 'icon_folder', '136', '0', '2017-06-29 15:15:22', '2102');
INSERT INTO `sys_organization` VALUES ('178', '国际贸易系', '', '1506', 'icon_folder', '136', '0', '2017-06-29 15:15:50', '2102');
INSERT INTO `sys_organization` VALUES ('179', '保险系', '', '1507', 'icon_folder', '136', '0', '2017-06-29 15:16:12', '2102');
INSERT INTO `sys_organization` VALUES ('180', '能源经济系', '', '1508', 'icon_folder', '136', '0', '2017-06-29 15:16:30', '2102');
INSERT INTO `sys_organization` VALUES ('181', '英语系', '', '1601', 'icon_folder', '137', '0', '2017-06-29 15:16:57', '2102');
INSERT INTO `sys_organization` VALUES ('182', '商务英语系', '', '1602', 'icon_folder', '137', '0', '2017-06-29 15:17:18', '2102');
INSERT INTO `sys_organization` VALUES ('183', '语言文化传播系', '', '1603', 'icon_folder', '137', '0', '2017-06-29 15:17:37', '2102');
INSERT INTO `sys_organization` VALUES ('184', '光电子信息科学系', '', '1701', 'icon_folder', '138', '0', '2017-06-29 15:17:59', '2102');
INSERT INTO `sys_organization` VALUES ('185', '数学课部', '', '1702', 'icon_folder', '138', '0', '2017-06-29 15:18:18', '2102');
INSERT INTO `sys_organization` VALUES ('186', '信息与计算科学系', '', '1703', 'icon_folder', '138', '0', '2017-06-29 15:18:40', '2102');
INSERT INTO `sys_organization` VALUES ('187', '电气工程师资系', '', '1801', 'icon_folder', '139', '0', '2017-06-29 15:19:25', '2102');
INSERT INTO `sys_organization` VALUES ('188', '机械设计师资系', '', '1802', 'icon_folder', '139', '0', '2017-06-29 15:19:44', '2102');
INSERT INTO `sys_organization` VALUES ('189', '电子商务师资系', '', '1803', 'icon_folder', '139', '0', '2017-06-29 15:20:18', '2102');
INSERT INTO `sys_organization` VALUES ('190', '网络工师资系', '', '1804', 'icon_folder', '139', '0', '2017-06-29 15:20:40', '2102');
INSERT INTO `sys_organization` VALUES ('191', '国际贸易师资系', '', '1805', 'icon_folder', '139', '0', '2017-06-29 15:21:01', '2102');
INSERT INTO `sys_organization` VALUES ('192', '计算机应用师资系', '', '1806', 'icon_folder', '139', '0', '2017-06-29 15:21:37', '2102');
INSERT INTO `sys_organization` VALUES ('193', '教育学系', '', '1807', 'icon_folder', '139', '0', '2017-06-29 15:22:13', '2102');
INSERT INTO `sys_organization` VALUES ('194', '测控技术与仪器', '', '070101', 'icon_folder', '140', '0', '2017-06-29 15:31:15', '2103');
INSERT INTO `sys_organization` VALUES ('195', '测控技术与仪器(产品质量工程)', '', '070102', 'icon_folder', '140', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('196', '机械设计制造及其自动化', '', '070201', 'icon_folder', '141', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('212', '生物科学(理学)', '', '100202', 'icon_folder', '154', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('213', '酿酒工程(工学)', '', '100301', 'icon_folder', '155', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('214', '食品科学与工程(工学)', '', '100302', 'icon_folder', '155', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('215', '食品科学与工程(工学)(农副产品深加工)', '', '100303', 'icon_folder', '155', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('216', '食品质量与安全专业', '', '100304', 'icon_folder', '155', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('217', '制药工程(工学)', '', '100401', 'icon_folder', '156', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('218', '计算机科学与技术', '', '110101', 'icon_folder', '157', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('219', '软件工程', '', '110201', 'icon_folder', '158', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('220', '网络工程', '', '110301', 'icon_folder', '159', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('221', '数字媒体技术', '', '110102', 'icon_folder', '157', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('222', '物联网工程', '', '110302', 'icon_folder', '159', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('223', '信息安全', '', '110303', 'icon_folder', '159', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('224', '土木工程', '', '120101', 'icon_folder', '160', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('225', '土木工程(防水材料与工程)', '', '120201', 'icon_folder', '161', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('226', '工程管理', '', '120301', 'icon_folder', '162', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('227', '交通工程', '', '120401', 'icon_folder', '163', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('228', '建筑学', '', '120501', 'icon_folder', '164', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('229', '风景园林', '', '120601', 'icon_folder', '165', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('230', '城乡规划', '', '120502', 'icon_folder', '164', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('231', '建筑学(室内设计)', '', '120503', 'icon_folder', '164', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('232', '环境工程', '', '120701', 'icon_folder', '166', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('233', '环境生态工程', '', '120702', 'icon_folder', '166', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('234', '水务工程', '', '120703', 'icon_folder', '166', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('235', '视觉传达设计', '', '130101', 'icon_folder', '167', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('236', '视觉传达设计(广告设计)', '', '130102', 'icon_folder', '167', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('237', '环境设计', '', '130201', 'icon_folder', '168', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('238', '环境设计(展示设计)', '', '130202', 'icon_folder', '168', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('239', '公共艺术(装饰设计)', '', '130301', 'icon_folder', '169', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('240', '数字媒体艺术', '', '130401', 'icon_folder', '170', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('241', '工业设计专业（工科）', '', '140101', 'icon_folder', '171', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('242', '产品设计', '', '140201', 'icon_folder', '172', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('243', '产品设计(玩具设计)', '', '140202', 'icon_folder', '172', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('244', '市场营销', '', '150101', 'icon_folder', '173', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('245', '人力资源管理', '', '150102', 'icon_folder', '173', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('246', '财务管理', '', '150103', 'icon_folder', '173', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('247', '会计学', '', '150201', 'icon_folder', '174', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('248', '信息管理与信息系统', '', '150301', 'icon_folder', '175', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('249', '电子商务', '', '150302', 'icon_folder', '175', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('250', '行政管理', '', '150401', 'icon_folder', '176', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('251', '金融学', '', '150501', 'icon_folder', '177', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('252', '金融学(数理金融双专业)', '', '150502', 'icon_folder', '177', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('253', '国际经济与贸易', '', '150601', 'icon_folder', '178', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('254', '国际经济与贸易(国际贸易英语双专业)', '', '150602', 'icon_folder', '178', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('255', '保险学', '', '150701', 'icon_folder', '179', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('256', '能源经济', '', '150801', 'icon_folder', '180', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('257', '英语', '', '160101', 'icon_folder', '181', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('258', '商务英语', '', '160201', 'icon_folder', '182', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('259', '汉语国际教育', '', '160301', 'icon_folder', '183', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('260', '电子信息科学与技术', '', '170101', 'icon_folder', '184', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('261', '应用统计学', '', '170201', 'icon_folder', '185', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('262', '信息与计算科学', '', '170301', 'icon_folder', '186', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('263', '光电信息科学与工程', '', '170102', 'icon_folder', '184', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('264', '电气工程及其自动化(师资)', '', '180101', 'icon_folder', '187', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('265', '机械设计制造及其自动化(师资)', '', '180201', 'icon_folder', '188', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('266', '电子商务(师资)', '', '180301', 'icon_folder', '189', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('267', '网络工程（师资）', '', '180401', 'icon_folder', '190', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('268', '国际经济与贸易（物流管理.师资）', '', '180501', 'icon_folder', '191', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('269', '计算机科学与技术（师资）', '', '180601', 'icon_folder', '192', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('273', '测控1', '', '07010101', 'icon_folder', '194', '0', '2017-06-29 17:37:06', '2104');
INSERT INTO `sys_organization` VALUES ('270', '职业技术教育学', '', '180701', 'icon_folder', '193', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('197', '农业机械化及其自动化', '', '070202', 'icon_folder', '141', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('198', '工业工程', '', '070301', 'icon_folder', '142', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('199', '包装工程', '', '070401', 'icon_folder', '143', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('200', '电气工程及其自动化', '', '080101', 'icon_folder', '144', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('201', '自动化', '', '080201', 'icon_folder', '145', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('202', '电子信息工程', '', '080301', 'icon_folder', '146', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('203', '通信工程', '', '080401', 'icon_folder', '147', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('204', '电子科学与技术', '', '080501', 'icon_folder', '148', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('205', '高分子材料与工程', '', '090101', 'icon_folder', '149', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('206', '材料科学与工程', '', '090102', 'icon_folder', '149', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('207', '轻化工程', '', '090201', 'icon_folder', '150', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('208', '化学工程与工艺专业', '', '090301', 'icon_folder', '151', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('209', '材料成型及控制工程', '', '090401', 'icon_folder', '152', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('210', '生物工程(工学)', '', '100101', 'icon_folder', '153', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('211', '生物技术(理学)', '', '100201', 'icon_folder', '154', '0', '2017-06-29 15:33:29', '2103');
INSERT INTO `sys_organization` VALUES ('277', '信管1', '', '15030101', 'icon_folder', '248', '0', '2017-06-29 17:43:47', '2104');
INSERT INTO `sys_organization` VALUES ('278', '电商1', '', '15030201', 'icon_folder', '249', '0', '2017-06-29 17:44:30', '2104');

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `seq` tinyint(1) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `resourcetype` tinyint(1) NOT NULL DEFAULT '0',
  `createdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_lhcrxhwf9hilx0lwhwxigxxqg` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=801 DEFAULT CHARSET=utf8 COMMENT='资源';

-- ----------------------------
-- Records of sys_resource
-- ----------------------------
INSERT INTO `sys_resource` VALUES ('1', '系统管理', '', null, 'icon-systemset', null, '9', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('2', '资源管理', '/resource/manager', null, 'icon-resource', '1', '1', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('5', '资源列表', '/resource/treeGrid', '资源列表', ' ', '2', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('6', '添加', '/resource/add', '资源添加', ' ', '2', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('7', '编辑', '/resource/edit', '资源编辑', ' ', '2', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('8', '删除', '/resource/delete', '资源删除', ' ', '2', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('3', '角色管理', '/role/manager', null, 'icon-role', '1', '3', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('10', '角色列表', '/role/dataGrid', '角色列表', ' ', '3', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('11', '添加', '/role/add', '角色添加', ' ', '3', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('12', '编辑', '/role/edit', '角色编辑', ' ', '3', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('13', '删除', '/role/delete', '角色删除', ' ', '3', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('14', '授权', '/role/grant', '角色授权', ' ', '3', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('4', '用户管理', '/user/manager', null, 'icon-user', '1', '5', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('15', '用户列表', '/user/dataGrid', '用户列表', ' ', '4', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('16', '添加', '/user/add', '用户添加', ' ', '4', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('17', '编辑', '/user/edit', '用户编辑', ' ', '4', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('18', '删除', '/user/delete', '用户删除', ' ', '4', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('19', '查看', '/user/view', '用户查看', ' ', '4', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('20', '部门管理', '/organization/manager', null, 'icon-depart', '1', '2', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('21', '部门列表', '/organization/treeGrid', '用户列表', ' ', '20', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('22', '添加', '/organization/add', '部门添加', ' ', '20', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('23', '编辑', '/organization/edit', '部门编辑', ' ', '20', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('24', '删除', '/organization/delete', '部门删除', ' ', '20', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('25', '查看', '/organization/view', '部门查看', ' ', '20', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('26', '数据字典', '/dictionary/manager', null, 'icon-basedata', '1', '4', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('27', '字典列表', '/dictionary/dataGrid', '字典列表', ' ', '26', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('28', '字典类别列表', '/dictionarytype/dataGrid', '字典类别列表', ' ', '26', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('29', '添加', '/dictionary/add', '字典添加', ' ', '26', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('30', '编辑', '/dictionary/edit', '字典编辑', ' ', '26', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('31', '删除', '/dictionary/delete', '字典删除', ' ', '26', '0', '0', '1', '2014-02-19 01:00:00');
INSERT INTO `sys_resource` VALUES ('692', '编辑', '/tSpotSet/edit', null, '', '690', '1', '0', '1', '2017-06-26 10:37:18');
INSERT INTO `sys_resource` VALUES ('693', '详情', '/tSpotSet/view', null, '', '690', '2', '0', '1', '2017-06-26 10:38:04');
INSERT INTO `sys_resource` VALUES ('694', '删除', '/tSpotSet/delete', null, '', '690', '3', '0', '1', '2017-06-26 10:38:35');
INSERT INTO `sys_resource` VALUES ('642', '数据权限', '/sysUserData/grantPage.do', null, ' ', '4', '0', '0', '1', '2017-04-27 11:35:28');
INSERT INTO `sys_resource` VALUES ('684', '迎新地图', '/pointMap/manage', null, 'icon-welmap', '643', '3', '0', '0', '2017-06-24 11:13:49');
INSERT INTO `sys_resource` VALUES ('685', '家庭基本信息', '/tFamilyInformation/manage', null, 'icon-myfam', '643', '1', '0', '0', '2017-06-24 14:19:50');
INSERT INTO `sys_resource` VALUES ('686', '添加', '/tFamilyInformation/add', null, '', '685', '1', '0', '1', '2017-06-24 14:20:10');
INSERT INTO `sys_resource` VALUES ('687', '编辑', '/tFamilyInformation/edit', null, '', '685', '2', '0', '1', '2017-06-24 14:20:26');
INSERT INTO `sys_resource` VALUES ('688', '删除', '/tFamilyInformation/delete', null, '', '685', '3', '0', '1', '2017-06-24 14:20:45');
INSERT INTO `sys_resource` VALUES ('643', '我的(学生)', '', null, 'icon-mystd', null, '0', '0', '0', '2017-05-08 08:55:05');
INSERT INTO `sys_resource` VALUES ('680', '添加', '/tMyInformation/add', null, '', '679', '1', '0', '1', '2017-06-22 17:17:44');
INSERT INTO `sys_resource` VALUES ('681', '编辑', '/tMyInformation/edit', null, '', '679', '2', '0', '1', '2017-06-22 17:18:15');
INSERT INTO `sys_resource` VALUES ('679', '新生基本信息', '/tMyInformation/manage', null, 'icon-freshinfo', '790', '0', '0', '0', '2017-06-22 11:24:24');
INSERT INTO `sys_resource` VALUES ('651', '我的申请', '/tMwMyApply/manage', null, 'icon-myreply', '643', '4', '0', '0', '2017-05-08 10:17:52');
INSERT INTO `sys_resource` VALUES ('691', '新增', '/tSpotSet/add', null, '', '690', '0', '0', '1', '2017-06-26 10:36:51');
INSERT INTO `sys_resource` VALUES ('654', '我的审批', '/tMwApproving/manage', null, 'icon-myreview', '707', '-1', '0', '0', '2017-05-08 10:19:53');
INSERT INTO `sys_resource` VALUES ('695', '报到情况预登记', '/tPreRegist/manage', null, 'icon-register', '643', '2', '0', '0', '2017-06-26 14:05:50');
INSERT INTO `sys_resource` VALUES ('690', '点位设置', '/tSpotSet/manage', null, 'icon-location', '708', '4', '0', '0', '2017-06-26 10:36:01');
INSERT INTO `sys_resource` VALUES ('689', '详情', '/tFamilyInformation/view', null, '', '685', '4', '0', '1', '2017-06-24 14:24:43');
INSERT INTO `sys_resource` VALUES ('682', '删除', '/tMyInformation/delete', null, '', '679', '3', '0', '1', '2017-06-22 17:18:47');
INSERT INTO `sys_resource` VALUES ('683', '详情', '/tMyInformation/view', null, '', '679', '4', '0', '1', '2017-06-22 17:19:34');
INSERT INTO `sys_resource` VALUES ('696', '添加', '/tPreRegist/add', null, '', '695', '0', '0', '1', '2017-06-26 14:07:22');
INSERT INTO `sys_resource` VALUES ('697', '编辑', '/tPreRegist/edit', null, '', '695', '0', '0', '1', '2017-06-26 14:08:09');
INSERT INTO `sys_resource` VALUES ('698', '删除', '/tPreRegist/delete', null, '', '695', '0', '0', '1', '2017-06-26 14:08:43');
INSERT INTO `sys_resource` VALUES ('699', '详情', '/tPreRegist/view', null, '', '695', '0', '0', '1', '2017-06-26 14:09:24');
INSERT INTO `sys_resource` VALUES ('700', '班导师信息初始化', '/tCounselorTeacher/manage', null, 'icon-teacherinfo', '790', '5', '0', '0', '2017-06-26 16:12:48');
INSERT INTO `sys_resource` VALUES ('701', '详情', '/tCounselorTeacher/view', null, ' ', '700', '0', '0', '1', '2017-06-26 16:14:50');
INSERT INTO `sys_resource` VALUES ('702', '新增', '/tCounselorTeacher/add', null, ' ', '700', '0', '0', '1', '2017-06-26 16:15:23');
INSERT INTO `sys_resource` VALUES ('703', '编辑', '/tCounselorTeacher/edit', null, ' ', '700', '0', '0', '1', '2017-06-26 16:15:52');
INSERT INTO `sys_resource` VALUES ('704', '删除', '/tCounselorTeacher/delete', null, ' ', '700', '0', '0', '1', '2017-06-26 16:16:22');
INSERT INTO `sys_resource` VALUES ('705', '我的基本信息', '/tMyInformation/people', null, 'icon-myinfo', '643', '0', '0', '0', '2017-06-26 16:36:41');
INSERT INTO `sys_resource` VALUES ('707', '我的(教职工)', '', null, 'icon-myteacher', null, '1', '0', '0', '2017-06-27 09:58:42');
INSERT INTO `sys_resource` VALUES ('708', '我的(现场迎新人员)', '', null, 'icon-mystd', null, '2', '0', '0', '2017-06-27 09:59:13');
INSERT INTO `sys_resource` VALUES ('709', '新生物品发放初始化', '/tStudentGoods/manage', null, 'icon-goods', '790', '4', '0', '0', '2017-06-28 16:52:04');
INSERT INTO `sys_resource` VALUES ('710', '添加', '/tStudentGoods/add', null, '', '709', '0', '0', '1', '2017-06-28 16:53:04');
INSERT INTO `sys_resource` VALUES ('711', '编辑', '/tStudentGoods/edit', null, '', '709', '0', '0', '1', '2017-06-28 16:55:06');
INSERT INTO `sys_resource` VALUES ('712', '详情', '/tStudentGoods/view', null, '', '709', '0', '0', '1', '2017-06-28 16:55:38');
INSERT INTO `sys_resource` VALUES ('713', '删除', '/tStudentGoods/delete', null, '', '709', '0', '0', '1', '2017-06-28 16:56:00');
INSERT INTO `sys_resource` VALUES ('714', '我的班级', '/tMyClass/student', null, 'icon-myclass', '643', '5', '0', '0', '2017-06-28 17:34:04');
INSERT INTO `sys_resource` VALUES ('715', '班级花名册', '/tMyClass/teacher', null, ' icon-myclass', '707', '7', '0', '0', '2017-06-28 17:34:32');
INSERT INTO `sys_resource` VALUES ('716', '导出', '/tMyClass/export', null, '', '714', '0', '0', '1', '2017-06-28 17:36:25');
INSERT INTO `sys_resource` VALUES ('717', '导出', '/tMyClass/export', null, '', '715', '0', '0', '1', '2017-06-28 17:36:34');
INSERT INTO `sys_resource` VALUES ('718', '个人剩余办理事项', '/pointMap/manage-aa', null, 'icon-backlog', '708', '1', '0', '0', '2017-06-29 16:33:09');
INSERT INTO `sys_resource` VALUES ('719', '新生预报到情况', '/tStuArriveInfo/manage', null, 'icon-statistics', '707', '1', '0', '0', '2017-06-29 17:57:03');
INSERT INTO `sys_resource` VALUES ('754', '新生宿舍初始化', '', null, 'icon-freshman', null, '3', '0', '0', '2017-07-03 16:00:10');
INSERT INTO `sys_resource` VALUES ('755', '区域初始化', '/tArea/manage', null, 'icon-area', '754', '0', '0', '0', '2017-07-03 16:00:46');
INSERT INTO `sys_resource` VALUES ('756', '楼栋初始化', '/tBuild/manage', null, 'icon-building', '754', '1', '0', '0', '2017-07-03 16:01:13');
INSERT INTO `sys_resource` VALUES ('757', '宿舍初始化', '/tRoom/manage', null, 'icon-bedroom', '754', '2', '0', '0', '2017-07-03 16:01:43');
INSERT INTO `sys_resource` VALUES ('758', '床位初始化', '/tBed/manage', null, 'icon-bed', '754', '3', '0', '0', '2017-07-03 16:02:10');
INSERT INTO `sys_resource` VALUES ('759', '添加', '/tArea/add', null, '', '755', '0', '0', '1', '2017-07-03 16:02:57');
INSERT INTO `sys_resource` VALUES ('760', '编辑', '/tArea/edit', null, '', '755', '0', '0', '1', '2017-07-03 16:03:16');
INSERT INTO `sys_resource` VALUES ('761', '删除', '/tArea/delete', null, '', '755', '0', '0', '1', '2017-07-03 16:03:45');
INSERT INTO `sys_resource` VALUES ('762', '详情', '/tArea/view', null, '', '755', '0', '0', '1', '2017-07-03 16:04:03');
INSERT INTO `sys_resource` VALUES ('763', '添加', '/tBuild/add', null, '', '756', '0', '0', '1', '2017-07-03 16:04:42');
INSERT INTO `sys_resource` VALUES ('764', '编辑', '/tBuild/edit', null, '', '756', '0', '0', '1', '2017-07-03 16:05:06');
INSERT INTO `sys_resource` VALUES ('765', '删除', '/tBuild/delete', null, '', '756', '0', '0', '1', '2017-07-03 16:05:27');
INSERT INTO `sys_resource` VALUES ('766', '详情', '/tBuild/view', null, '', '756', '0', '0', '1', '2017-07-03 16:06:30');
INSERT INTO `sys_resource` VALUES ('767', '添加', '/tRoom/add', null, '', '757', '0', '0', '1', '2017-07-03 16:06:54');
INSERT INTO `sys_resource` VALUES ('768', '编辑', '/tRoom/edit', null, '', '757', '0', '0', '1', '2017-07-03 16:07:16');
INSERT INTO `sys_resource` VALUES ('769', '删除', '/tRoom/delete', null, '', '757', '0', '0', '1', '2017-07-03 16:07:41');
INSERT INTO `sys_resource` VALUES ('743', '新生住宿初始化', '/tStuDormitoryInfo/manage', null, 'icon-freshroom', '790', '2', '0', '0', '2017-07-03 11:24:27');
INSERT INTO `sys_resource` VALUES ('744', '添加', '/tStuDormitoryInfo/add', null, '', '743', '0', '0', '1', '2017-07-03 11:25:41');
INSERT INTO `sys_resource` VALUES ('745', '编辑', '/tStuDormitoryInfo/edit', null, '', '743', '0', '0', '1', '2017-07-03 11:26:41');
INSERT INTO `sys_resource` VALUES ('746', '删除', '/tStuDormitoryInfo/delete', null, '', '743', '0', '0', '1', '2017-07-03 11:27:17');
INSERT INTO `sys_resource` VALUES ('747', '详情', '/tStuDormitoryInfo/view', null, '', '743', '0', '0', '1', '2017-07-03 11:27:52');
INSERT INTO `sys_resource` VALUES ('748', '新生缴费明细初始化', '/tNeedPaymentData/manage', null, 'icon-cost', '790', '5', '0', '0', '2017-07-03 15:33:18');
INSERT INTO `sys_resource` VALUES ('749', '增加', '/tNeedPaymentData/add', null, '', '748', '0', '0', '1', '2017-07-03 15:34:02');
INSERT INTO `sys_resource` VALUES ('750', '编辑', '/tNeedPaymentData/edit', null, '', '748', '1', '0', '1', '2017-07-03 15:35:00');
INSERT INTO `sys_resource` VALUES ('752', '详情', '/tNeedPaymentData/view', null, '', '748', '3', '0', '1', '2017-07-03 15:38:28');
INSERT INTO `sys_resource` VALUES ('753', '删除', '/tNeedPaymentData/delete', null, '', '748', '4', '0', '1', '2017-07-03 15:39:05');
INSERT INTO `sys_resource` VALUES ('770', '详情', '/tRoom/view', null, '', '757', '0', '0', '1', '2017-07-03 16:07:59');
INSERT INTO `sys_resource` VALUES ('771', '添加', '/tBed/add', null, '', '758', '0', '0', '1', '2017-07-03 16:08:22');
INSERT INTO `sys_resource` VALUES ('772', '编辑', '/tBed/edit', null, '', '758', '0', '0', '1', '2017-07-03 16:08:49');
INSERT INTO `sys_resource` VALUES ('773', '删除', '/tBed/delete', null, '', '758', '0', '0', '1', '2017-07-03 16:09:08');
INSERT INTO `sys_resource` VALUES ('774', '详情', '/tBed/view', null, '', '758', '0', '0', '1', '2017-07-03 16:09:38');
INSERT INTO `sys_resource` VALUES ('775', '我的缴费单', '/tNeedPaymentData/myManage', null, 'icon-mypay', '643', '7', '0', '0', '2017-07-03 17:20:00');
INSERT INTO `sys_resource` VALUES ('791', '学生家庭信息总览', '/tFamilyTeacher/teacher', null, 'icon-party', '707', '8', '0', '0', '2017-07-12 10:08:06');
INSERT INTO `sys_resource` VALUES ('790', '新生数据初始化', '', null, 'icon-freshinfo', null, '4', '0', '0', '2017-07-11 15:41:22');
INSERT INTO `sys_resource` VALUES ('792', '导出', '/tFamilyInformation/export', null, '', '791', '0', '0', '1', '2017-07-12 10:09:50');
INSERT INTO `sys_resource` VALUES ('776', '我的室友', '/tRoommate/manage', null, 'icon-myroom', '643', '6', '0', '0', '2017-07-04 14:41:47');
INSERT INTO `sys_resource` VALUES ('777', '添加', '/tRoommate/add', null, '', '776', '0', '0', '1', '2017-07-04 14:42:35');
INSERT INTO `sys_resource` VALUES ('778', '编辑', '/tRoommate/edit', null, '', '776', '0', '0', '1', '2017-07-04 14:42:59');
INSERT INTO `sys_resource` VALUES ('779', '删除', '/tRoommate/delete', null, '', '776', '0', '0', '1', '2017-07-04 14:43:23');
INSERT INTO `sys_resource` VALUES ('780', '详情', '/tRoommate/view', null, '', '776', '0', '0', '1', '2017-07-04 14:44:21');
INSERT INTO `sys_resource` VALUES ('782', '按民族报到统计', '/statics/tjNation', null, 'icon-nation', '707', '4', '0', '0', '2017-07-05 12:10:01');
INSERT INTO `sys_resource` VALUES ('783', '按政治面貌报到统计', '/statics/tjPolitical', null, 'icon-party', '707', '5', '0', '0', '2017-07-05 15:24:12');
INSERT INTO `sys_resource` VALUES ('784', '按专业情况统计', '/statics/tjMajor', null, 'icon-professional', '707', '3', '0', '0', '2017-07-05 18:58:58');
INSERT INTO `sys_resource` VALUES ('786', '按院系报到统计', '/statics/tjCollege', null, 'icon-department', '707', '2', '0', '0', '2017-07-06 10:04:20');
INSERT INTO `sys_resource` VALUES ('787', '按到校交通方式统计', '/statics/tjWay1', null, 'icon-bus', '707', '6', '0', '0', '2017-07-06 15:23:20');
INSERT INTO `sys_resource` VALUES ('788', '新生签到统计', '/statics/tjstuSignIn', null, 'icon-sign', '707', '0', '0', '0', '2017-07-08 15:02:04');
INSERT INTO `sys_resource` VALUES ('795', '导出', '/statics/export', null, '', '788', '1', '0', '1', '2017-07-15 16:10:28');
INSERT INTO `sys_resource` VALUES ('800', '事项设置', '/tSpotMatter/manage', null, 'icon-backlog', '708', '2', '0', '0', '2017-07-19 15:21:12');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `seq` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '0', '超级管理员，拥有全部权限', '0');
INSERT INTO `sys_role` VALUES ('30', '管理员', '0', '', '1');
INSERT INTO `sys_role` VALUES ('26', '班导师', '0', '', '1');
INSERT INTO `sys_role` VALUES ('27', '学工部长', '0', '', '1');
INSERT INTO `sys_role` VALUES ('28', '学生', '0', '', '1');
INSERT INTO `sys_role` VALUES ('29', '迎新用户', '0', '', '1');

-- ----------------------------
-- Table structure for sys_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resource`;
CREATE TABLE `sys_role_resource` (
  `role_id` smallint(5) NOT NULL,
  `resource_id` smallint(5) NOT NULL,
  PRIMARY KEY (`role_id`,`resource_id`),
  KEY `FK_p16r4t8uf973ru6yrcbb7r1ku` (`resource_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='角色资源';

-- ----------------------------
-- Records of sys_role_resource
-- ----------------------------
INSERT INTO `sys_role_resource` VALUES ('1', '1');
INSERT INTO `sys_role_resource` VALUES ('1', '2');
INSERT INTO `sys_role_resource` VALUES ('1', '3');
INSERT INTO `sys_role_resource` VALUES ('1', '4');
INSERT INTO `sys_role_resource` VALUES ('1', '5');
INSERT INTO `sys_role_resource` VALUES ('1', '6');
INSERT INTO `sys_role_resource` VALUES ('1', '7');
INSERT INTO `sys_role_resource` VALUES ('1', '8');
INSERT INTO `sys_role_resource` VALUES ('1', '10');
INSERT INTO `sys_role_resource` VALUES ('1', '11');
INSERT INTO `sys_role_resource` VALUES ('1', '12');
INSERT INTO `sys_role_resource` VALUES ('1', '13');
INSERT INTO `sys_role_resource` VALUES ('1', '14');
INSERT INTO `sys_role_resource` VALUES ('1', '15');
INSERT INTO `sys_role_resource` VALUES ('1', '16');
INSERT INTO `sys_role_resource` VALUES ('1', '17');
INSERT INTO `sys_role_resource` VALUES ('1', '18');
INSERT INTO `sys_role_resource` VALUES ('1', '19');
INSERT INTO `sys_role_resource` VALUES ('1', '20');
INSERT INTO `sys_role_resource` VALUES ('1', '21');
INSERT INTO `sys_role_resource` VALUES ('1', '22');
INSERT INTO `sys_role_resource` VALUES ('1', '23');
INSERT INTO `sys_role_resource` VALUES ('1', '24');
INSERT INTO `sys_role_resource` VALUES ('1', '25');
INSERT INTO `sys_role_resource` VALUES ('1', '26');
INSERT INTO `sys_role_resource` VALUES ('1', '27');
INSERT INTO `sys_role_resource` VALUES ('1', '28');
INSERT INTO `sys_role_resource` VALUES ('1', '29');
INSERT INTO `sys_role_resource` VALUES ('1', '30');
INSERT INTO `sys_role_resource` VALUES ('1', '31');
INSERT INTO `sys_role_resource` VALUES ('1', '642');
INSERT INTO `sys_role_resource` VALUES ('1', '643');
INSERT INTO `sys_role_resource` VALUES ('1', '651');
INSERT INTO `sys_role_resource` VALUES ('1', '654');
INSERT INTO `sys_role_resource` VALUES ('1', '679');
INSERT INTO `sys_role_resource` VALUES ('1', '680');
INSERT INTO `sys_role_resource` VALUES ('1', '681');
INSERT INTO `sys_role_resource` VALUES ('1', '682');
INSERT INTO `sys_role_resource` VALUES ('1', '683');
INSERT INTO `sys_role_resource` VALUES ('1', '684');
INSERT INTO `sys_role_resource` VALUES ('1', '685');
INSERT INTO `sys_role_resource` VALUES ('1', '686');
INSERT INTO `sys_role_resource` VALUES ('1', '687');
INSERT INTO `sys_role_resource` VALUES ('1', '688');
INSERT INTO `sys_role_resource` VALUES ('1', '689');
INSERT INTO `sys_role_resource` VALUES ('1', '690');
INSERT INTO `sys_role_resource` VALUES ('1', '691');
INSERT INTO `sys_role_resource` VALUES ('1', '692');
INSERT INTO `sys_role_resource` VALUES ('1', '693');
INSERT INTO `sys_role_resource` VALUES ('1', '694');
INSERT INTO `sys_role_resource` VALUES ('1', '695');
INSERT INTO `sys_role_resource` VALUES ('1', '696');
INSERT INTO `sys_role_resource` VALUES ('1', '697');
INSERT INTO `sys_role_resource` VALUES ('1', '698');
INSERT INTO `sys_role_resource` VALUES ('1', '699');
INSERT INTO `sys_role_resource` VALUES ('1', '700');
INSERT INTO `sys_role_resource` VALUES ('1', '701');
INSERT INTO `sys_role_resource` VALUES ('1', '702');
INSERT INTO `sys_role_resource` VALUES ('1', '703');
INSERT INTO `sys_role_resource` VALUES ('1', '704');
INSERT INTO `sys_role_resource` VALUES ('1', '705');
INSERT INTO `sys_role_resource` VALUES ('1', '707');
INSERT INTO `sys_role_resource` VALUES ('1', '708');
INSERT INTO `sys_role_resource` VALUES ('1', '709');
INSERT INTO `sys_role_resource` VALUES ('1', '710');
INSERT INTO `sys_role_resource` VALUES ('1', '711');
INSERT INTO `sys_role_resource` VALUES ('1', '712');
INSERT INTO `sys_role_resource` VALUES ('1', '713');
INSERT INTO `sys_role_resource` VALUES ('1', '714');
INSERT INTO `sys_role_resource` VALUES ('1', '715');
INSERT INTO `sys_role_resource` VALUES ('1', '716');
INSERT INTO `sys_role_resource` VALUES ('1', '717');
INSERT INTO `sys_role_resource` VALUES ('1', '718');
INSERT INTO `sys_role_resource` VALUES ('1', '719');
INSERT INTO `sys_role_resource` VALUES ('1', '743');
INSERT INTO `sys_role_resource` VALUES ('1', '744');
INSERT INTO `sys_role_resource` VALUES ('1', '745');
INSERT INTO `sys_role_resource` VALUES ('1', '746');
INSERT INTO `sys_role_resource` VALUES ('1', '747');
INSERT INTO `sys_role_resource` VALUES ('1', '748');
INSERT INTO `sys_role_resource` VALUES ('1', '749');
INSERT INTO `sys_role_resource` VALUES ('1', '750');
INSERT INTO `sys_role_resource` VALUES ('1', '752');
INSERT INTO `sys_role_resource` VALUES ('1', '753');
INSERT INTO `sys_role_resource` VALUES ('1', '754');
INSERT INTO `sys_role_resource` VALUES ('1', '755');
INSERT INTO `sys_role_resource` VALUES ('1', '756');
INSERT INTO `sys_role_resource` VALUES ('1', '757');
INSERT INTO `sys_role_resource` VALUES ('1', '758');
INSERT INTO `sys_role_resource` VALUES ('1', '759');
INSERT INTO `sys_role_resource` VALUES ('1', '760');
INSERT INTO `sys_role_resource` VALUES ('1', '761');
INSERT INTO `sys_role_resource` VALUES ('1', '762');
INSERT INTO `sys_role_resource` VALUES ('1', '763');
INSERT INTO `sys_role_resource` VALUES ('1', '764');
INSERT INTO `sys_role_resource` VALUES ('1', '765');
INSERT INTO `sys_role_resource` VALUES ('1', '766');
INSERT INTO `sys_role_resource` VALUES ('1', '767');
INSERT INTO `sys_role_resource` VALUES ('1', '768');
INSERT INTO `sys_role_resource` VALUES ('1', '769');
INSERT INTO `sys_role_resource` VALUES ('1', '770');
INSERT INTO `sys_role_resource` VALUES ('1', '771');
INSERT INTO `sys_role_resource` VALUES ('1', '772');
INSERT INTO `sys_role_resource` VALUES ('1', '773');
INSERT INTO `sys_role_resource` VALUES ('1', '774');
INSERT INTO `sys_role_resource` VALUES ('1', '775');
INSERT INTO `sys_role_resource` VALUES ('1', '776');
INSERT INTO `sys_role_resource` VALUES ('1', '777');
INSERT INTO `sys_role_resource` VALUES ('1', '778');
INSERT INTO `sys_role_resource` VALUES ('1', '779');
INSERT INTO `sys_role_resource` VALUES ('1', '780');
INSERT INTO `sys_role_resource` VALUES ('1', '782');
INSERT INTO `sys_role_resource` VALUES ('1', '783');
INSERT INTO `sys_role_resource` VALUES ('1', '784');
INSERT INTO `sys_role_resource` VALUES ('1', '786');
INSERT INTO `sys_role_resource` VALUES ('1', '787');
INSERT INTO `sys_role_resource` VALUES ('1', '788');
INSERT INTO `sys_role_resource` VALUES ('1', '790');
INSERT INTO `sys_role_resource` VALUES ('1', '791');
INSERT INTO `sys_role_resource` VALUES ('1', '792');
INSERT INTO `sys_role_resource` VALUES ('1', '795');
INSERT INTO `sys_role_resource` VALUES ('1', '800');
INSERT INTO `sys_role_resource` VALUES ('26', '654');
INSERT INTO `sys_role_resource` VALUES ('26', '679');
INSERT INTO `sys_role_resource` VALUES ('26', '680');
INSERT INTO `sys_role_resource` VALUES ('26', '681');
INSERT INTO `sys_role_resource` VALUES ('26', '682');
INSERT INTO `sys_role_resource` VALUES ('26', '683');
INSERT INTO `sys_role_resource` VALUES ('26', '689');
INSERT INTO `sys_role_resource` VALUES ('26', '707');
INSERT INTO `sys_role_resource` VALUES ('26', '715');
INSERT INTO `sys_role_resource` VALUES ('26', '717');
INSERT INTO `sys_role_resource` VALUES ('26', '719');
INSERT INTO `sys_role_resource` VALUES ('26', '748');
INSERT INTO `sys_role_resource` VALUES ('26', '749');
INSERT INTO `sys_role_resource` VALUES ('26', '750');
INSERT INTO `sys_role_resource` VALUES ('26', '752');
INSERT INTO `sys_role_resource` VALUES ('26', '753');
INSERT INTO `sys_role_resource` VALUES ('26', '782');
INSERT INTO `sys_role_resource` VALUES ('26', '783');
INSERT INTO `sys_role_resource` VALUES ('26', '784');
INSERT INTO `sys_role_resource` VALUES ('26', '786');
INSERT INTO `sys_role_resource` VALUES ('26', '787');
INSERT INTO `sys_role_resource` VALUES ('26', '788');
INSERT INTO `sys_role_resource` VALUES ('26', '790');
INSERT INTO `sys_role_resource` VALUES ('26', '791');
INSERT INTO `sys_role_resource` VALUES ('26', '792');
INSERT INTO `sys_role_resource` VALUES ('27', '654');
INSERT INTO `sys_role_resource` VALUES ('27', '679');
INSERT INTO `sys_role_resource` VALUES ('27', '680');
INSERT INTO `sys_role_resource` VALUES ('27', '681');
INSERT INTO `sys_role_resource` VALUES ('27', '682');
INSERT INTO `sys_role_resource` VALUES ('27', '683');
INSERT INTO `sys_role_resource` VALUES ('27', '700');
INSERT INTO `sys_role_resource` VALUES ('27', '701');
INSERT INTO `sys_role_resource` VALUES ('27', '702');
INSERT INTO `sys_role_resource` VALUES ('27', '703');
INSERT INTO `sys_role_resource` VALUES ('27', '704');
INSERT INTO `sys_role_resource` VALUES ('27', '707');
INSERT INTO `sys_role_resource` VALUES ('27', '709');
INSERT INTO `sys_role_resource` VALUES ('27', '710');
INSERT INTO `sys_role_resource` VALUES ('27', '711');
INSERT INTO `sys_role_resource` VALUES ('27', '712');
INSERT INTO `sys_role_resource` VALUES ('27', '713');
INSERT INTO `sys_role_resource` VALUES ('27', '715');
INSERT INTO `sys_role_resource` VALUES ('27', '717');
INSERT INTO `sys_role_resource` VALUES ('27', '719');
INSERT INTO `sys_role_resource` VALUES ('27', '743');
INSERT INTO `sys_role_resource` VALUES ('27', '744');
INSERT INTO `sys_role_resource` VALUES ('27', '745');
INSERT INTO `sys_role_resource` VALUES ('27', '746');
INSERT INTO `sys_role_resource` VALUES ('27', '747');
INSERT INTO `sys_role_resource` VALUES ('27', '748');
INSERT INTO `sys_role_resource` VALUES ('27', '749');
INSERT INTO `sys_role_resource` VALUES ('27', '750');
INSERT INTO `sys_role_resource` VALUES ('27', '752');
INSERT INTO `sys_role_resource` VALUES ('27', '753');
INSERT INTO `sys_role_resource` VALUES ('27', '754');
INSERT INTO `sys_role_resource` VALUES ('27', '755');
INSERT INTO `sys_role_resource` VALUES ('27', '756');
INSERT INTO `sys_role_resource` VALUES ('27', '757');
INSERT INTO `sys_role_resource` VALUES ('27', '758');
INSERT INTO `sys_role_resource` VALUES ('27', '759');
INSERT INTO `sys_role_resource` VALUES ('27', '760');
INSERT INTO `sys_role_resource` VALUES ('27', '761');
INSERT INTO `sys_role_resource` VALUES ('27', '762');
INSERT INTO `sys_role_resource` VALUES ('27', '763');
INSERT INTO `sys_role_resource` VALUES ('27', '764');
INSERT INTO `sys_role_resource` VALUES ('27', '765');
INSERT INTO `sys_role_resource` VALUES ('27', '766');
INSERT INTO `sys_role_resource` VALUES ('27', '767');
INSERT INTO `sys_role_resource` VALUES ('27', '768');
INSERT INTO `sys_role_resource` VALUES ('27', '769');
INSERT INTO `sys_role_resource` VALUES ('27', '770');
INSERT INTO `sys_role_resource` VALUES ('27', '771');
INSERT INTO `sys_role_resource` VALUES ('27', '772');
INSERT INTO `sys_role_resource` VALUES ('27', '773');
INSERT INTO `sys_role_resource` VALUES ('27', '774');
INSERT INTO `sys_role_resource` VALUES ('27', '782');
INSERT INTO `sys_role_resource` VALUES ('27', '783');
INSERT INTO `sys_role_resource` VALUES ('27', '784');
INSERT INTO `sys_role_resource` VALUES ('27', '786');
INSERT INTO `sys_role_resource` VALUES ('27', '787');
INSERT INTO `sys_role_resource` VALUES ('27', '788');
INSERT INTO `sys_role_resource` VALUES ('27', '790');
INSERT INTO `sys_role_resource` VALUES ('28', '643');
INSERT INTO `sys_role_resource` VALUES ('28', '651');
INSERT INTO `sys_role_resource` VALUES ('28', '684');
INSERT INTO `sys_role_resource` VALUES ('28', '685');
INSERT INTO `sys_role_resource` VALUES ('28', '686');
INSERT INTO `sys_role_resource` VALUES ('28', '687');
INSERT INTO `sys_role_resource` VALUES ('28', '688');
INSERT INTO `sys_role_resource` VALUES ('28', '689');
INSERT INTO `sys_role_resource` VALUES ('28', '695');
INSERT INTO `sys_role_resource` VALUES ('28', '696');
INSERT INTO `sys_role_resource` VALUES ('28', '697');
INSERT INTO `sys_role_resource` VALUES ('28', '698');
INSERT INTO `sys_role_resource` VALUES ('28', '699');
INSERT INTO `sys_role_resource` VALUES ('28', '705');
INSERT INTO `sys_role_resource` VALUES ('28', '714');
INSERT INTO `sys_role_resource` VALUES ('28', '775');
INSERT INTO `sys_role_resource` VALUES ('28', '776');
INSERT INTO `sys_role_resource` VALUES ('29', '690');
INSERT INTO `sys_role_resource` VALUES ('29', '691');
INSERT INTO `sys_role_resource` VALUES ('29', '692');
INSERT INTO `sys_role_resource` VALUES ('29', '693');
INSERT INTO `sys_role_resource` VALUES ('29', '694');
INSERT INTO `sys_role_resource` VALUES ('29', '708');
INSERT INTO `sys_role_resource` VALUES ('29', '718');
INSERT INTO `sys_role_resource` VALUES ('29', '800');
INSERT INTO `sys_role_resource` VALUES ('30', '1');
INSERT INTO `sys_role_resource` VALUES ('30', '2');
INSERT INTO `sys_role_resource` VALUES ('30', '3');
INSERT INTO `sys_role_resource` VALUES ('30', '4');
INSERT INTO `sys_role_resource` VALUES ('30', '5');
INSERT INTO `sys_role_resource` VALUES ('30', '6');
INSERT INTO `sys_role_resource` VALUES ('30', '7');
INSERT INTO `sys_role_resource` VALUES ('30', '8');
INSERT INTO `sys_role_resource` VALUES ('30', '10');
INSERT INTO `sys_role_resource` VALUES ('30', '11');
INSERT INTO `sys_role_resource` VALUES ('30', '12');
INSERT INTO `sys_role_resource` VALUES ('30', '13');
INSERT INTO `sys_role_resource` VALUES ('30', '14');
INSERT INTO `sys_role_resource` VALUES ('30', '15');
INSERT INTO `sys_role_resource` VALUES ('30', '16');
INSERT INTO `sys_role_resource` VALUES ('30', '17');
INSERT INTO `sys_role_resource` VALUES ('30', '18');
INSERT INTO `sys_role_resource` VALUES ('30', '19');
INSERT INTO `sys_role_resource` VALUES ('30', '20');
INSERT INTO `sys_role_resource` VALUES ('30', '21');
INSERT INTO `sys_role_resource` VALUES ('30', '22');
INSERT INTO `sys_role_resource` VALUES ('30', '23');
INSERT INTO `sys_role_resource` VALUES ('30', '24');
INSERT INTO `sys_role_resource` VALUES ('30', '25');
INSERT INTO `sys_role_resource` VALUES ('30', '26');
INSERT INTO `sys_role_resource` VALUES ('30', '27');
INSERT INTO `sys_role_resource` VALUES ('30', '28');
INSERT INTO `sys_role_resource` VALUES ('30', '29');
INSERT INTO `sys_role_resource` VALUES ('30', '30');
INSERT INTO `sys_role_resource` VALUES ('30', '31');
INSERT INTO `sys_role_resource` VALUES ('30', '642');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `age` tinyint(1) NOT NULL DEFAULT '0',
  `usertype` tinyint(1) NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `organization_id` int(11) NOT NULL DEFAULT '0',
  `createdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginname` (`loginname`),
  KEY `FK_jl2srlt2cvxyiudt0fjly6m7n` (`organization_id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '超级管理员', '21232f297a57a5a743894a0e4a801fc3', '0', '18', '1', '0', '0', '1', '2012-06-04 01:00:00');
INSERT INTO `sys_user` VALUES ('41', '1410831115', '学生', 'e10adc3949ba59abbe56e057f20f883e', '0', '19', '14', '1', '0', '274', '2017-06-30 09:43:44');
INSERT INTO `sys_user` VALUES ('36', '2017062601', '迎新人员', '05ef0adf44257e0173e4eeb83bed2448', '0', '0', '14', '1', '0', '136', '2017-06-26 15:57:07');
INSERT INTO `sys_user` VALUES ('46', '2017070502', '余凌', '79a21554d931793b7c0169d941cb713d', '1', '0', '9', '1', '0', '278', '2017-07-05 13:57:21');
INSERT INTO `sys_user` VALUES ('42', '1410831113', '黄任峥', 'c48946e7ee42ccfebf009db384182c05', '0', '22', '14', '1', '0', '277', '2017-06-30 10:08:00');
INSERT INTO `sys_user` VALUES ('38', '2017062602', '管理员', 'af3c715d280430ff6b6e0686ae8cfe0a', '1', '0', '1', '1', '0', '1', '2017-06-26 16:05:33');
INSERT INTO `sys_user` VALUES ('45', '2017070501', '学工部长', 'e9c7a56d63550d137f01a808a54837ac', '0', '0', '9', '1', '0', '121', '2017-07-05 11:50:15');
INSERT INTO `sys_user` VALUES ('40', '2017070503', '班主任', 'ec0732f1ddb0bebe5052afe4e20b8e1c', '0', '40', '9', '1', '0', '277', '2017-06-29 18:42:01');
INSERT INTO `sys_user` VALUES ('47', '1310313127', '王爽', 'e10adc3949ba59abbe56e057f20f883e', '1', '0', '14', '1', '0', '273', '2017-07-06 16:36:10');
INSERT INTO `sys_user` VALUES ('48', '1410831109', '刘伟', 'e10adc3949ba59abbe56e057f20f883e', '0', '25', '14', '1', '0', '278', '2017-07-07 09:33:21');
INSERT INTO `sys_user` VALUES ('49', '1410831102', '阿斯哈提', 'e10adc3949ba59abbe56e057f20f883e', '0', '21', '14', '1', '0', '277', '2017-07-07 09:33:56');
INSERT INTO `sys_user` VALUES ('50', '1410831106', '张坚城', 'e10adc3949ba59abbe56e057f20f883e', '0', '22', '14', '1', '0', '278', '2017-07-07 09:34:19');
INSERT INTO `sys_user` VALUES ('51', '1410831116', '华凯伦', '547d594e208a44fdf8e7c5068c49f25b', '1', '21', '14', '1', '0', '277', '2017-07-10 11:31:07');
INSERT INTO `sys_user` VALUES ('52', '1410831119', '王栋', '079bfc1371534b288df84597cc4c37b4', '0', '22', '14', '1', '0', '273', '2017-07-10 11:49:43');
INSERT INTO `sys_user` VALUES ('53', '1410831122', '喻雅洁', 'a6d434cd2806ef8fe1d4ebfb0dc6e9e9', '1', '21', '14', '1', '0', '273', '2017-07-10 11:50:34');
INSERT INTO `sys_user` VALUES ('54', '2017070504', '朱桢', '26b3c28c2898d96990cc9f6241aab433', '1', '40', '9', '1', '0', '273', '2017-07-10 11:51:54');
INSERT INTO `sys_user` VALUES ('55', '1410831135', '杜安黎', 'f467d41e9f423b99a4bed7eb71c5beed', '1', '22', '14', '1', '0', '278', '2017-07-12 17:55:22');
INSERT INTO `sys_user` VALUES ('56', '2017071901', '迎新2', '94b881eb5ab0f929abf302626ad14a6b', '1', '0', '14', '1', '0', '136', '2017-07-19 20:27:20');
INSERT INTO `sys_user` VALUES ('57', '10086', '测试', '6412121cbb2dc2cb9e460cfee7046be2', '1', '18', '14', '1', '0', '274', '2017-08-03 10:37:35');
INSERT INTO `sys_user` VALUES ('58', '201710086', '测试班主任', '6412121cbb2dc2cb9e460cfee7046be2', '1', '18', '9', '1', '0', '274', '2017-08-03 14:00:45');
INSERT INTO `sys_user` VALUES ('59', '080310086', '测试迎新人员', '6412121cbb2dc2cb9e460cfee7046be2', '0', '21', '14', '1', '0', '128', '2017-08-03 14:55:22');
INSERT INTO `sys_user` VALUES ('60', '2017070509', '杨老三', '1096d47401ca9a8a98a3ada8c0bde3aa', '0', '23', '9', '1', '0', '275', '2017-08-05 14:24:14');

-- ----------------------------
-- Table structure for sys_user_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_data`;
CREATE TABLE `sys_user_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `data_id` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_data
-- ----------------------------
INSERT INTO `sys_user_data` VALUES ('26', '27', '');
INSERT INTO `sys_user_data` VALUES ('27', '28', '105');
INSERT INTO `sys_user_data` VALUES ('32', '41', '277');
INSERT INTO `sys_user_data` VALUES ('33', '44', '221');
INSERT INTO `sys_user_data` VALUES ('37', '51', '277');
INSERT INTO `sys_user_data` VALUES ('38', '50', '278');
INSERT INTO `sys_user_data` VALUES ('39', '49', '278');
INSERT INTO `sys_user_data` VALUES ('41', '47', '273');
INSERT INTO `sys_user_data` VALUES ('42', '46', '278');
INSERT INTO `sys_user_data` VALUES ('43', '42', '277');
INSERT INTO `sys_user_data` VALUES ('45', '53', '273');
INSERT INTO `sys_user_data` VALUES ('47', '52', '273');
INSERT INTO `sys_user_data` VALUES ('50', '48', '278');
INSERT INTO `sys_user_data` VALUES ('51', '45', '130');
INSERT INTO `sys_user_data` VALUES ('52', '45', '150');
INSERT INTO `sys_user_data` VALUES ('53', '45', '207');
INSERT INTO `sys_user_data` VALUES ('54', '45', '276');
INSERT INTO `sys_user_data` VALUES ('55', '45', '151');
INSERT INTO `sys_user_data` VALUES ('56', '45', '208');
INSERT INTO `sys_user_data` VALUES ('57', '45', '152');
INSERT INTO `sys_user_data` VALUES ('58', '45', '209');
INSERT INTO `sys_user_data` VALUES ('59', '45', '149');
INSERT INTO `sys_user_data` VALUES ('60', '45', '205');
INSERT INTO `sys_user_data` VALUES ('61', '45', '206');
INSERT INTO `sys_user_data` VALUES ('62', '45', '134');
INSERT INTO `sys_user_data` VALUES ('63', '45', '170');
INSERT INTO `sys_user_data` VALUES ('64', '45', '240');
INSERT INTO `sys_user_data` VALUES ('65', '45', '167');
INSERT INTO `sys_user_data` VALUES ('66', '45', '235');
INSERT INTO `sys_user_data` VALUES ('67', '45', '236');
INSERT INTO `sys_user_data` VALUES ('68', '45', '168');
INSERT INTO `sys_user_data` VALUES ('69', '45', '237');
INSERT INTO `sys_user_data` VALUES ('70', '45', '238');
INSERT INTO `sys_user_data` VALUES ('71', '45', '169');
INSERT INTO `sys_user_data` VALUES ('72', '45', '239');
INSERT INTO `sys_user_data` VALUES ('73', '45', '138');
INSERT INTO `sys_user_data` VALUES ('74', '45', '186');
INSERT INTO `sys_user_data` VALUES ('75', '45', '262');
INSERT INTO `sys_user_data` VALUES ('76', '45', '184');
INSERT INTO `sys_user_data` VALUES ('77', '45', '260');
INSERT INTO `sys_user_data` VALUES ('78', '45', '263');
INSERT INTO `sys_user_data` VALUES ('79', '45', '185');
INSERT INTO `sys_user_data` VALUES ('80', '45', '261');
INSERT INTO `sys_user_data` VALUES ('81', '45', '121');
INSERT INTO `sys_user_data` VALUES ('82', '45', '131');
INSERT INTO `sys_user_data` VALUES ('83', '45', '154');
INSERT INTO `sys_user_data` VALUES ('84', '45', '211');
INSERT INTO `sys_user_data` VALUES ('85', '45', '212');
INSERT INTO `sys_user_data` VALUES ('86', '45', '155');
INSERT INTO `sys_user_data` VALUES ('87', '45', '213');
INSERT INTO `sys_user_data` VALUES ('88', '45', '214');
INSERT INTO `sys_user_data` VALUES ('89', '45', '215');
INSERT INTO `sys_user_data` VALUES ('90', '45', '216');
INSERT INTO `sys_user_data` VALUES ('91', '45', '156');
INSERT INTO `sys_user_data` VALUES ('92', '45', '217');
INSERT INTO `sys_user_data` VALUES ('93', '45', '153');
INSERT INTO `sys_user_data` VALUES ('94', '45', '210');
INSERT INTO `sys_user_data` VALUES ('95', '45', '135');
INSERT INTO `sys_user_data` VALUES ('96', '45', '171');
INSERT INTO `sys_user_data` VALUES ('97', '45', '241');
INSERT INTO `sys_user_data` VALUES ('98', '45', '172');
INSERT INTO `sys_user_data` VALUES ('99', '45', '242');
INSERT INTO `sys_user_data` VALUES ('100', '45', '243');
INSERT INTO `sys_user_data` VALUES ('101', '45', '139');
INSERT INTO `sys_user_data` VALUES ('102', '45', '190');
INSERT INTO `sys_user_data` VALUES ('103', '45', '267');
INSERT INTO `sys_user_data` VALUES ('104', '45', '187');
INSERT INTO `sys_user_data` VALUES ('105', '45', '264');
INSERT INTO `sys_user_data` VALUES ('106', '45', '191');
INSERT INTO `sys_user_data` VALUES ('107', '45', '268');
INSERT INTO `sys_user_data` VALUES ('108', '45', '188');
INSERT INTO `sys_user_data` VALUES ('109', '45', '265');
INSERT INTO `sys_user_data` VALUES ('110', '45', '192');
INSERT INTO `sys_user_data` VALUES ('111', '45', '269');
INSERT INTO `sys_user_data` VALUES ('112', '45', '189');
INSERT INTO `sys_user_data` VALUES ('113', '45', '266');
INSERT INTO `sys_user_data` VALUES ('114', '45', '193');
INSERT INTO `sys_user_data` VALUES ('115', '45', '270');
INSERT INTO `sys_user_data` VALUES ('116', '45', '1');
INSERT INTO `sys_user_data` VALUES ('117', '45', '132');
INSERT INTO `sys_user_data` VALUES ('118', '45', '158');
INSERT INTO `sys_user_data` VALUES ('119', '45', '219');
INSERT INTO `sys_user_data` VALUES ('120', '45', '159');
INSERT INTO `sys_user_data` VALUES ('121', '45', '222');
INSERT INTO `sys_user_data` VALUES ('122', '45', '223');
INSERT INTO `sys_user_data` VALUES ('123', '45', '220');
INSERT INTO `sys_user_data` VALUES ('124', '45', '157');
INSERT INTO `sys_user_data` VALUES ('125', '45', '221');
INSERT INTO `sys_user_data` VALUES ('126', '45', '218');
INSERT INTO `sys_user_data` VALUES ('127', '45', '136');
INSERT INTO `sys_user_data` VALUES ('128', '45', '174');
INSERT INTO `sys_user_data` VALUES ('129', '45', '247');
INSERT INTO `sys_user_data` VALUES ('130', '45', '178');
INSERT INTO `sys_user_data` VALUES ('131', '45', '253');
INSERT INTO `sys_user_data` VALUES ('132', '45', '254');
INSERT INTO `sys_user_data` VALUES ('133', '45', '175');
INSERT INTO `sys_user_data` VALUES ('134', '45', '248');
INSERT INTO `sys_user_data` VALUES ('135', '45', '277');
INSERT INTO `sys_user_data` VALUES ('136', '45', '249');
INSERT INTO `sys_user_data` VALUES ('137', '45', '278');
INSERT INTO `sys_user_data` VALUES ('138', '45', '179');
INSERT INTO `sys_user_data` VALUES ('139', '45', '255');
INSERT INTO `sys_user_data` VALUES ('140', '45', '176');
INSERT INTO `sys_user_data` VALUES ('141', '45', '250');
INSERT INTO `sys_user_data` VALUES ('142', '45', '180');
INSERT INTO `sys_user_data` VALUES ('143', '45', '256');
INSERT INTO `sys_user_data` VALUES ('144', '45', '173');
INSERT INTO `sys_user_data` VALUES ('145', '45', '244');
INSERT INTO `sys_user_data` VALUES ('146', '45', '245');
INSERT INTO `sys_user_data` VALUES ('147', '45', '246');
INSERT INTO `sys_user_data` VALUES ('148', '45', '177');
INSERT INTO `sys_user_data` VALUES ('149', '45', '252');
INSERT INTO `sys_user_data` VALUES ('150', '45', '251');
INSERT INTO `sys_user_data` VALUES ('151', '45', '128');
INSERT INTO `sys_user_data` VALUES ('152', '45', '142');
INSERT INTO `sys_user_data` VALUES ('153', '45', '198');
INSERT INTO `sys_user_data` VALUES ('154', '45', '143');
INSERT INTO `sys_user_data` VALUES ('155', '45', '199');
INSERT INTO `sys_user_data` VALUES ('156', '45', '140');
INSERT INTO `sys_user_data` VALUES ('157', '45', '194');
INSERT INTO `sys_user_data` VALUES ('158', '45', '274');
INSERT INTO `sys_user_data` VALUES ('159', '45', '273');
INSERT INTO `sys_user_data` VALUES ('160', '45', '195');
INSERT INTO `sys_user_data` VALUES ('161', '45', '141');
INSERT INTO `sys_user_data` VALUES ('162', '45', '197');
INSERT INTO `sys_user_data` VALUES ('163', '45', '196');
INSERT INTO `sys_user_data` VALUES ('164', '45', '133');
INSERT INTO `sys_user_data` VALUES ('165', '45', '162');
INSERT INTO `sys_user_data` VALUES ('166', '45', '226');
INSERT INTO `sys_user_data` VALUES ('167', '45', '166');
INSERT INTO `sys_user_data` VALUES ('168', '45', '233');
INSERT INTO `sys_user_data` VALUES ('169', '45', '234');
INSERT INTO `sys_user_data` VALUES ('170', '45', '232');
INSERT INTO `sys_user_data` VALUES ('171', '45', '163');
INSERT INTO `sys_user_data` VALUES ('172', '45', '227');
INSERT INTO `sys_user_data` VALUES ('173', '45', '160');
INSERT INTO `sys_user_data` VALUES ('174', '45', '224');
INSERT INTO `sys_user_data` VALUES ('175', '45', '164');
INSERT INTO `sys_user_data` VALUES ('176', '45', '230');
INSERT INTO `sys_user_data` VALUES ('177', '45', '231');
INSERT INTO `sys_user_data` VALUES ('178', '45', '228');
INSERT INTO `sys_user_data` VALUES ('179', '45', '161');
INSERT INTO `sys_user_data` VALUES ('180', '45', '225');
INSERT INTO `sys_user_data` VALUES ('181', '45', '165');
INSERT INTO `sys_user_data` VALUES ('182', '45', '229');
INSERT INTO `sys_user_data` VALUES ('183', '45', '137');
INSERT INTO `sys_user_data` VALUES ('184', '45', '182');
INSERT INTO `sys_user_data` VALUES ('185', '45', '258');
INSERT INTO `sys_user_data` VALUES ('186', '45', '183');
INSERT INTO `sys_user_data` VALUES ('187', '45', '259');
INSERT INTO `sys_user_data` VALUES ('188', '45', '181');
INSERT INTO `sys_user_data` VALUES ('189', '45', '257');
INSERT INTO `sys_user_data` VALUES ('190', '45', '129');
INSERT INTO `sys_user_data` VALUES ('191', '45', '146');
INSERT INTO `sys_user_data` VALUES ('192', '45', '202');
INSERT INTO `sys_user_data` VALUES ('193', '45', '147');
INSERT INTO `sys_user_data` VALUES ('194', '45', '203');
INSERT INTO `sys_user_data` VALUES ('195', '45', '144');
INSERT INTO `sys_user_data` VALUES ('196', '45', '200');
INSERT INTO `sys_user_data` VALUES ('197', '45', '275');
INSERT INTO `sys_user_data` VALUES ('198', '45', '148');
INSERT INTO `sys_user_data` VALUES ('199', '45', '204');
INSERT INTO `sys_user_data` VALUES ('200', '45', '145');
INSERT INTO `sys_user_data` VALUES ('201', '45', '201');
INSERT INTO `sys_user_data` VALUES ('202', '1', '130');
INSERT INTO `sys_user_data` VALUES ('203', '1', '150');
INSERT INTO `sys_user_data` VALUES ('204', '1', '207');
INSERT INTO `sys_user_data` VALUES ('205', '1', '276');
INSERT INTO `sys_user_data` VALUES ('206', '1', '151');
INSERT INTO `sys_user_data` VALUES ('207', '1', '208');
INSERT INTO `sys_user_data` VALUES ('208', '1', '152');
INSERT INTO `sys_user_data` VALUES ('209', '1', '209');
INSERT INTO `sys_user_data` VALUES ('210', '1', '149');
INSERT INTO `sys_user_data` VALUES ('211', '1', '205');
INSERT INTO `sys_user_data` VALUES ('212', '1', '206');
INSERT INTO `sys_user_data` VALUES ('213', '1', '134');
INSERT INTO `sys_user_data` VALUES ('214', '1', '170');
INSERT INTO `sys_user_data` VALUES ('215', '1', '240');
INSERT INTO `sys_user_data` VALUES ('216', '1', '167');
INSERT INTO `sys_user_data` VALUES ('217', '1', '235');
INSERT INTO `sys_user_data` VALUES ('218', '1', '236');
INSERT INTO `sys_user_data` VALUES ('219', '1', '168');
INSERT INTO `sys_user_data` VALUES ('220', '1', '237');
INSERT INTO `sys_user_data` VALUES ('221', '1', '238');
INSERT INTO `sys_user_data` VALUES ('222', '1', '169');
INSERT INTO `sys_user_data` VALUES ('223', '1', '239');
INSERT INTO `sys_user_data` VALUES ('224', '1', '138');
INSERT INTO `sys_user_data` VALUES ('225', '1', '186');
INSERT INTO `sys_user_data` VALUES ('226', '1', '262');
INSERT INTO `sys_user_data` VALUES ('227', '1', '184');
INSERT INTO `sys_user_data` VALUES ('228', '1', '260');
INSERT INTO `sys_user_data` VALUES ('229', '1', '263');
INSERT INTO `sys_user_data` VALUES ('230', '1', '185');
INSERT INTO `sys_user_data` VALUES ('231', '1', '261');
INSERT INTO `sys_user_data` VALUES ('232', '1', '121');
INSERT INTO `sys_user_data` VALUES ('233', '1', '131');
INSERT INTO `sys_user_data` VALUES ('234', '1', '154');
INSERT INTO `sys_user_data` VALUES ('235', '1', '211');
INSERT INTO `sys_user_data` VALUES ('236', '1', '212');
INSERT INTO `sys_user_data` VALUES ('237', '1', '155');
INSERT INTO `sys_user_data` VALUES ('238', '1', '213');
INSERT INTO `sys_user_data` VALUES ('239', '1', '214');
INSERT INTO `sys_user_data` VALUES ('240', '1', '215');
INSERT INTO `sys_user_data` VALUES ('241', '1', '216');
INSERT INTO `sys_user_data` VALUES ('242', '1', '156');
INSERT INTO `sys_user_data` VALUES ('243', '1', '217');
INSERT INTO `sys_user_data` VALUES ('244', '1', '153');
INSERT INTO `sys_user_data` VALUES ('245', '1', '210');
INSERT INTO `sys_user_data` VALUES ('246', '1', '135');
INSERT INTO `sys_user_data` VALUES ('247', '1', '171');
INSERT INTO `sys_user_data` VALUES ('248', '1', '241');
INSERT INTO `sys_user_data` VALUES ('249', '1', '172');
INSERT INTO `sys_user_data` VALUES ('250', '1', '242');
INSERT INTO `sys_user_data` VALUES ('251', '1', '243');
INSERT INTO `sys_user_data` VALUES ('252', '1', '139');
INSERT INTO `sys_user_data` VALUES ('253', '1', '190');
INSERT INTO `sys_user_data` VALUES ('254', '1', '267');
INSERT INTO `sys_user_data` VALUES ('255', '1', '187');
INSERT INTO `sys_user_data` VALUES ('256', '1', '264');
INSERT INTO `sys_user_data` VALUES ('257', '1', '191');
INSERT INTO `sys_user_data` VALUES ('258', '1', '268');
INSERT INTO `sys_user_data` VALUES ('259', '1', '188');
INSERT INTO `sys_user_data` VALUES ('260', '1', '265');
INSERT INTO `sys_user_data` VALUES ('261', '1', '192');
INSERT INTO `sys_user_data` VALUES ('262', '1', '269');
INSERT INTO `sys_user_data` VALUES ('263', '1', '189');
INSERT INTO `sys_user_data` VALUES ('264', '1', '266');
INSERT INTO `sys_user_data` VALUES ('265', '1', '193');
INSERT INTO `sys_user_data` VALUES ('266', '1', '270');
INSERT INTO `sys_user_data` VALUES ('267', '1', '1');
INSERT INTO `sys_user_data` VALUES ('268', '1', '132');
INSERT INTO `sys_user_data` VALUES ('269', '1', '158');
INSERT INTO `sys_user_data` VALUES ('270', '1', '219');
INSERT INTO `sys_user_data` VALUES ('271', '1', '159');
INSERT INTO `sys_user_data` VALUES ('272', '1', '222');
INSERT INTO `sys_user_data` VALUES ('273', '1', '223');
INSERT INTO `sys_user_data` VALUES ('274', '1', '220');
INSERT INTO `sys_user_data` VALUES ('275', '1', '157');
INSERT INTO `sys_user_data` VALUES ('276', '1', '221');
INSERT INTO `sys_user_data` VALUES ('277', '1', '218');
INSERT INTO `sys_user_data` VALUES ('278', '1', '136');
INSERT INTO `sys_user_data` VALUES ('279', '1', '174');
INSERT INTO `sys_user_data` VALUES ('280', '1', '247');
INSERT INTO `sys_user_data` VALUES ('281', '1', '178');
INSERT INTO `sys_user_data` VALUES ('282', '1', '253');
INSERT INTO `sys_user_data` VALUES ('283', '1', '254');
INSERT INTO `sys_user_data` VALUES ('284', '1', '175');
INSERT INTO `sys_user_data` VALUES ('285', '1', '248');
INSERT INTO `sys_user_data` VALUES ('286', '1', '277');
INSERT INTO `sys_user_data` VALUES ('287', '1', '249');
INSERT INTO `sys_user_data` VALUES ('288', '1', '278');
INSERT INTO `sys_user_data` VALUES ('289', '1', '179');
INSERT INTO `sys_user_data` VALUES ('290', '1', '255');
INSERT INTO `sys_user_data` VALUES ('291', '1', '176');
INSERT INTO `sys_user_data` VALUES ('292', '1', '250');
INSERT INTO `sys_user_data` VALUES ('293', '1', '180');
INSERT INTO `sys_user_data` VALUES ('294', '1', '256');
INSERT INTO `sys_user_data` VALUES ('295', '1', '173');
INSERT INTO `sys_user_data` VALUES ('296', '1', '244');
INSERT INTO `sys_user_data` VALUES ('297', '1', '245');
INSERT INTO `sys_user_data` VALUES ('298', '1', '246');
INSERT INTO `sys_user_data` VALUES ('299', '1', '177');
INSERT INTO `sys_user_data` VALUES ('300', '1', '252');
INSERT INTO `sys_user_data` VALUES ('301', '1', '251');
INSERT INTO `sys_user_data` VALUES ('302', '1', '128');
INSERT INTO `sys_user_data` VALUES ('303', '1', '142');
INSERT INTO `sys_user_data` VALUES ('304', '1', '198');
INSERT INTO `sys_user_data` VALUES ('305', '1', '143');
INSERT INTO `sys_user_data` VALUES ('306', '1', '199');
INSERT INTO `sys_user_data` VALUES ('307', '1', '140');
INSERT INTO `sys_user_data` VALUES ('308', '1', '194');
INSERT INTO `sys_user_data` VALUES ('309', '1', '274');
INSERT INTO `sys_user_data` VALUES ('310', '1', '273');
INSERT INTO `sys_user_data` VALUES ('311', '1', '195');
INSERT INTO `sys_user_data` VALUES ('312', '1', '141');
INSERT INTO `sys_user_data` VALUES ('313', '1', '197');
INSERT INTO `sys_user_data` VALUES ('314', '1', '196');
INSERT INTO `sys_user_data` VALUES ('315', '1', '133');
INSERT INTO `sys_user_data` VALUES ('316', '1', '162');
INSERT INTO `sys_user_data` VALUES ('317', '1', '226');
INSERT INTO `sys_user_data` VALUES ('318', '1', '166');
INSERT INTO `sys_user_data` VALUES ('319', '1', '233');
INSERT INTO `sys_user_data` VALUES ('320', '1', '234');
INSERT INTO `sys_user_data` VALUES ('321', '1', '232');
INSERT INTO `sys_user_data` VALUES ('322', '1', '163');
INSERT INTO `sys_user_data` VALUES ('323', '1', '227');
INSERT INTO `sys_user_data` VALUES ('324', '1', '160');
INSERT INTO `sys_user_data` VALUES ('325', '1', '224');
INSERT INTO `sys_user_data` VALUES ('326', '1', '164');
INSERT INTO `sys_user_data` VALUES ('327', '1', '230');
INSERT INTO `sys_user_data` VALUES ('328', '1', '231');
INSERT INTO `sys_user_data` VALUES ('329', '1', '228');
INSERT INTO `sys_user_data` VALUES ('330', '1', '161');
INSERT INTO `sys_user_data` VALUES ('331', '1', '225');
INSERT INTO `sys_user_data` VALUES ('332', '1', '165');
INSERT INTO `sys_user_data` VALUES ('333', '1', '229');
INSERT INTO `sys_user_data` VALUES ('334', '1', '137');
INSERT INTO `sys_user_data` VALUES ('335', '1', '182');
INSERT INTO `sys_user_data` VALUES ('336', '1', '258');
INSERT INTO `sys_user_data` VALUES ('337', '1', '183');
INSERT INTO `sys_user_data` VALUES ('338', '1', '259');
INSERT INTO `sys_user_data` VALUES ('339', '1', '181');
INSERT INTO `sys_user_data` VALUES ('340', '1', '257');
INSERT INTO `sys_user_data` VALUES ('341', '1', '129');
INSERT INTO `sys_user_data` VALUES ('342', '1', '146');
INSERT INTO `sys_user_data` VALUES ('343', '1', '202');
INSERT INTO `sys_user_data` VALUES ('344', '1', '147');
INSERT INTO `sys_user_data` VALUES ('345', '1', '203');
INSERT INTO `sys_user_data` VALUES ('346', '1', '144');
INSERT INTO `sys_user_data` VALUES ('347', '1', '200');
INSERT INTO `sys_user_data` VALUES ('348', '1', '275');
INSERT INTO `sys_user_data` VALUES ('349', '1', '148');
INSERT INTO `sys_user_data` VALUES ('350', '1', '204');
INSERT INTO `sys_user_data` VALUES ('351', '1', '145');
INSERT INTO `sys_user_data` VALUES ('352', '1', '201');
INSERT INTO `sys_user_data` VALUES ('353', '40', '248');
INSERT INTO `sys_user_data` VALUES ('354', '40', '277');
INSERT INTO `sys_user_data` VALUES ('355', '57', '274');
INSERT INTO `sys_user_data` VALUES ('356', '58', '274');
INSERT INTO `sys_user_data` VALUES ('358', '59', '128');
INSERT INTO `sys_user_data` VALUES ('359', '59', '194');
INSERT INTO `sys_user_data` VALUES ('360', '59', '273');
INSERT INTO `sys_user_data` VALUES ('361', '59', '274');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` smallint(5) NOT NULL,
  `role_id` smallint(5) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK_fxu3td9m5o7qov1kbdvmn0g0x` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('34', '28');
INSERT INTO `sys_user_role` VALUES ('35', '28');
INSERT INTO `sys_user_role` VALUES ('36', '29');
INSERT INTO `sys_user_role` VALUES ('37', '28');
INSERT INTO `sys_user_role` VALUES ('38', '30');
INSERT INTO `sys_user_role` VALUES ('39', '26');
INSERT INTO `sys_user_role` VALUES ('40', '26');
INSERT INTO `sys_user_role` VALUES ('41', '28');
INSERT INTO `sys_user_role` VALUES ('42', '28');
INSERT INTO `sys_user_role` VALUES ('43', '28');
INSERT INTO `sys_user_role` VALUES ('45', '27');
INSERT INTO `sys_user_role` VALUES ('46', '26');
INSERT INTO `sys_user_role` VALUES ('47', '28');
INSERT INTO `sys_user_role` VALUES ('48', '28');
INSERT INTO `sys_user_role` VALUES ('49', '28');
INSERT INTO `sys_user_role` VALUES ('50', '28');
INSERT INTO `sys_user_role` VALUES ('51', '28');
INSERT INTO `sys_user_role` VALUES ('52', '28');
INSERT INTO `sys_user_role` VALUES ('53', '28');
INSERT INTO `sys_user_role` VALUES ('54', '26');
INSERT INTO `sys_user_role` VALUES ('55', '28');
INSERT INTO `sys_user_role` VALUES ('56', '29');
INSERT INTO `sys_user_role` VALUES ('57', '28');
INSERT INTO `sys_user_role` VALUES ('58', '26');
INSERT INTO `sys_user_role` VALUES ('59', '29');
INSERT INTO `sys_user_role` VALUES ('60', '26');

-- ----------------------------
-- Table structure for t_area
-- ----------------------------
DROP TABLE IF EXISTS `t_area`;
CREATE TABLE `t_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '区域id',
  `name` varchar(20) DEFAULT NULL COMMENT '区域名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '区域备注',
  `college` varchar(20) DEFAULT NULL COMMENT '所属院系',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_area
-- ----------------------------
INSERT INTO `t_area` VALUES ('2', '西区', '这是西区', null, null, null, null, '1', '2017-07-12 18:24:16', '277');
INSERT INTO `t_area` VALUES ('3', '北区', '北区，北区有多个楼栋', null, null, null, null, '1', '2017-07-12 09:43:39', '277');
INSERT INTO `t_area` VALUES ('9', '中区', '这是中区', null, null, null, null, '1', '2017-07-11 16:36:32', '277');
INSERT INTO `t_area` VALUES ('10', '东区', '这是东区', null, null, null, null, '1', '2017-07-11 16:56:34', '277');
INSERT INTO `t_area` VALUES ('12', '南区', '这是南区', null, '1', '2017-07-11 17:55:25', '277', null, null, null);
INSERT INTO `t_area` VALUES ('14', '东北区', '东北区', null, null, null, null, '1', '2017-08-02 14:57:49', '1');
INSERT INTO `t_area` VALUES ('17', '测试区', '测试用', null, null, null, null, '1', '2017-08-02 16:01:03', '1');

-- ----------------------------
-- Table structure for t_bed
-- ----------------------------
DROP TABLE IF EXISTS `t_bed`;
CREATE TABLE `t_bed` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '床位ID',
  `bid` int(11) DEFAULT NULL COMMENT '楼栋ID',
  `rid` int(11) DEFAULT NULL COMMENT '寝室ID',
  `code` int(11) DEFAULT NULL COMMENT '床位编号',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_bed
-- ----------------------------
INSERT INTO `t_bed` VALUES ('2', '2', '2', '1', '1号床', null, null, null, '1', '2017-07-12 17:48:11', '277');
INSERT INTO `t_bed` VALUES ('9', '2', '2', '2', '2号床', null, null, null, '1', '2017-07-11 18:07:54', '277');
INSERT INTO `t_bed` VALUES ('10', '2', '2', '3', '3号床', null, null, null, '1', '2017-07-11 18:08:06', '277');
INSERT INTO `t_bed` VALUES ('11', '2', '2', '4', '4号床', null, null, null, '1', '2017-07-11 18:08:26', '277');
INSERT INTO `t_bed` VALUES ('12', '2', '3', '1', '1号床', null, null, null, '1', '2017-07-11 18:08:37', '277');
INSERT INTO `t_bed` VALUES ('13', '2', '3', '2', '2号床', null, null, null, '1', '2017-07-11 18:08:55', '277');
INSERT INTO `t_bed` VALUES ('14', '2', '3', '3', '3号床', null, null, null, '1', '2017-07-11 18:09:06', '277');
INSERT INTO `t_bed` VALUES ('15', '2', '3', '4', '4号床', null, null, null, '1', '2017-07-11 18:09:17', '277');
INSERT INTO `t_bed` VALUES ('16', '10', '4', '1', '1号床', null, null, null, '1', '2017-07-11 18:09:29', '277');
INSERT INTO `t_bed` VALUES ('17', '10', '4', '2', '2号床', null, null, null, '1', '2017-07-18 17:13:34', '277');
INSERT INTO `t_bed` VALUES ('18', '10', '4', '3', '3号床', null, null, null, '1', '2017-07-11 18:10:54', '277');
INSERT INTO `t_bed` VALUES ('19', '10', '4', '4', '4号床', null, null, null, '1', '2017-07-11 18:11:20', '277');
INSERT INTO `t_bed` VALUES ('20', '10', '5', '1', '1号床', null, null, null, '1', '2017-07-11 18:11:43', '277');
INSERT INTO `t_bed` VALUES ('21', '10', '5', '2', '2号床', null, null, null, '1', '2017-07-11 18:11:54', '277');
INSERT INTO `t_bed` VALUES ('22', '10', '5', '3', '3号床', null, null, null, '1', '2017-07-11 18:12:07', '277');
INSERT INTO `t_bed` VALUES ('26', '10', '5', '4', '4号床', null, null, null, '1', '2017-07-11 18:12:22', '277');
INSERT INTO `t_bed` VALUES ('27', '15', '6', '1', '1号床', null, null, null, '1', '2017-07-11 18:12:38', '277');
INSERT INTO `t_bed` VALUES ('28', '15', '6', '2', '2号床', null, null, null, '1', '2017-07-11 18:13:29', '277');
INSERT INTO `t_bed` VALUES ('29', '15', '6', '3', '3号床', null, null, null, '1', '2017-07-11 18:13:43', '277');
INSERT INTO `t_bed` VALUES ('30', '15', '6', '4', '4号床', null, null, null, '1', '2017-07-11 18:13:56', '277');
INSERT INTO `t_bed` VALUES ('31', '15', '7', '1', '1号床', null, null, null, '1', '2017-07-11 18:14:16', '277');
INSERT INTO `t_bed` VALUES ('32', '15', '7', '2', '2号床', null, null, null, '1', '2017-07-11 18:14:33', '277');
INSERT INTO `t_bed` VALUES ('33', '15', '7', '3', '3号床', null, null, null, '1', '2017-07-11 18:14:42', '277');
INSERT INTO `t_bed` VALUES ('34', '15', '7', '4', '4号床', null, null, null, '1', '2017-07-11 18:14:57', '277');
INSERT INTO `t_bed` VALUES ('35', '20', '8', '1', '1号床', null, null, null, '1', '2017-07-11 18:15:46', '277');
INSERT INTO `t_bed` VALUES ('36', '20', '8', '2', '2号床', '1', '2017-07-11 18:16:11', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('37', '20', '8', '3', '3号床', '1', '2017-07-11 18:16:21', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('38', '20', '8', '4', '4号床', '1', '2017-07-11 18:16:31', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('39', '20', '9', '1', '1号床', '1', '2017-07-11 18:16:43', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('40', '20', '9', '2', '2号床', '1', '2017-07-11 18:16:55', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('41', '20', '9', '3', '3号床', '1', '2017-07-11 18:17:12', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('42', '20', '9', '4', '4号床', '1', '2017-07-11 18:17:24', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('43', '21', '10', '1', '1号床', '1', '2017-07-11 18:18:14', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('44', '21', '10', '2', '2号床', '1', '2017-07-11 18:18:23', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('45', '21', '10', '3', '3号床', '1', '2017-07-11 18:18:31', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('46', '21', '10', '4', '4号床', '1', '2017-07-11 18:18:41', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('47', '21', '11', '1', '1号床', '1', '2017-07-11 18:18:50', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('48', '21', '11', '2', '2号床', '1', '2017-07-11 18:18:59', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('49', '21', '11', '3', '3号床', '1', '2017-07-11 18:19:10', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('50', '21', '11', '4', '4号床', '1', '2017-07-11 18:19:22', '277', null, null, null);
INSERT INTO `t_bed` VALUES ('60', '30', '20', '1', '测试', '1', '2017-08-02 16:02:05', '1', null, null, null);
INSERT INTO `t_bed` VALUES ('61', '30', '20', '2', '测试', null, null, null, '1', '2017-08-02 16:02:16', '1');
INSERT INTO `t_bed` VALUES ('62', '30', '20', '3', '测试', null, null, null, '1', '2017-08-02 16:02:21', '1');

-- ----------------------------
-- Table structure for t_build
-- ----------------------------
DROP TABLE IF EXISTS `t_build`;
CREATE TABLE `t_build` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '楼栋ID',
  `aid` int(11) NOT NULL COMMENT '区域ID',
  `code` varchar(11) DEFAULT NULL COMMENT '楼栋编号',
  `name` varchar(20) DEFAULT NULL COMMENT '楼栋名称',
  `layer` int(11) DEFAULT NULL COMMENT '楼层',
  `address` varchar(100) DEFAULT NULL COMMENT '详细地址',
  `type` varchar(20) DEFAULT NULL COMMENT '楼栋类型',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `college` varchar(20) DEFAULT NULL COMMENT '所属院系',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_build
-- ----------------------------
INSERT INTO `t_build` VALUES ('2', '3', '16', '北16栋', '6', '湖北工业大学北区', '男生宿舍楼', '学校入口', null, null, null, null, '1', '2017-08-02 15:08:53', '1');
INSERT INTO `t_build` VALUES ('10', '10', '04', '草莓园4栋', '6', '草莓园4栋', '女生宿舍楼', '草莓园4栋是女生寝室楼', null, null, null, null, '1', '2017-08-02 15:10:53', '1');
INSERT INTO `t_build` VALUES ('15', '9', '11', '中区11栋', '6', '中区11栋', '女生宿舍楼', '中区11栋是一栋男生寝室', null, null, null, null, '1', '2017-07-11 17:23:07', '277');
INSERT INTO `t_build` VALUES ('20', '2', '7', '西区7栋', '6', '西区7栋', '男生宿舍楼', '西区7栋靠近西区食堂', null, '1', '2017-07-11 17:56:51', '277', null, null, null);
INSERT INTO `t_build` VALUES ('21', '12', '23', '南区23栋', '6', '南区23栋', '男生宿舍楼', '南区23栋', null, null, null, null, '1', '2017-08-02 15:14:12', '1');
INSERT INTO `t_build` VALUES ('30', '17', '1', '测试楼', '6', '测试', '男生宿舍楼', '测试', '测试', '1', '2017-08-02 16:01:19', '1', null, null, null);

-- ----------------------------
-- Table structure for t_counselor_teacher
-- ----------------------------
DROP TABLE IF EXISTS `t_counselor_teacher`;
CREATE TABLE `t_counselor_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `college` varchar(255) DEFAULT NULL COMMENT '学院',
  `department` varchar(255) DEFAULT NULL COMMENT '系部',
  `class_name` varchar(255) DEFAULT NULL COMMENT '班级',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `type` varchar(255) DEFAULT NULL COMMENT '类别（班导师、辅导员）',
  `contact_information` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(255) DEFAULT NULL COMMENT 'email',
  `sex` varchar(255) DEFAULT NULL COMMENT '性别',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `major` varchar(255) DEFAULT NULL COMMENT '专业',
  `loginname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_counselor_teacher
-- ----------------------------
INSERT INTO `t_counselor_teacher` VALUES ('18', '128', '140', '273', '朱桢', '2002', '15623712622', '2323915348@qq.com', '1901', '1', '2017-07-03 11:07:17', '1', '1', '2017-08-05 15:50:05', '1', '194', '2017070504');
INSERT INTO `t_counselor_teacher` VALUES ('19', '136', '175', '277', '班主任', '2002', '13907180888', '30865403467@qq.com', '1901', '1', '2017-07-04 14:08:28', '277', '1', '2017-07-31 14:33:20', '1', '248', '2017070503');
INSERT INTO `t_counselor_teacher` VALUES ('20', '136', '175', '278', '余凌', '2002', '110112113', '15652@qq.com', '1902', '1', '2017-07-05 10:04:55', '277', '1', '2017-08-02 17:20:30', '1', '249', '2017070502');
INSERT INTO `t_counselor_teacher` VALUES ('21', '128', '140', '274', '测试班主任', '2002', '10086', '10086@qq.com', '1901', '1', '2017-08-03 14:03:46', '1', null, null, null, '194', '201710086');
INSERT INTO `t_counselor_teacher` VALUES ('23', '128', '140', '273', '7777111', null, '6161616', '4235346456', '1901', '1', '2017-08-05 16:12:51', '1', '1', '2017-08-05 16:32:16', '1', '194', '10086');

-- ----------------------------
-- Table structure for t_family_information
-- ----------------------------
DROP TABLE IF EXISTS `t_family_information`;
CREATE TABLE `t_family_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `examinee_id` varchar(14) DEFAULT NULL COMMENT '考生号',
  `appellation` varchar(10) DEFAULT NULL COMMENT '称谓',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `company` varchar(64) DEFAULT NULL COMMENT '工作单位',
  `contact_information` varchar(11) DEFAULT NULL COMMENT '联系方式',
  `occupation` varchar(20) DEFAULT NULL COMMENT '职务',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `occupation_name` varchar(255) DEFAULT NULL,
  `appellation_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_family_information
-- ----------------------------
INSERT INTO `t_family_information` VALUES ('5', '1410831115', '8602', '杨爸爸', '大公司', '15992969918', '老板', '35', '2017-06-28 14:16:45', '127', '41', '2017-07-12 15:52:45', '277', null, null);
INSERT INTO `t_family_information` VALUES ('7', '1410831115', '8603', '杨妈妈', '烟草公司', '13927828409', '职员', '41', '2017-07-12 10:38:17', '277', '41', '2017-07-12 15:52:50', '277', null, null);
INSERT INTO `t_family_information` VALUES ('8', '1410831109', '8602', '大黄狗', '家门口', '15628732619', '看家', '48', '2017-07-12 15:31:06', '278', null, null, null, null, null);
INSERT INTO `t_family_information` VALUES ('9', '1410831109', '8603', '大花猫', '家里面', '18912837198', '吃饭', '48', '2017-07-12 16:28:33', '278', null, null, null, null, null);
INSERT INTO `t_family_information` VALUES ('10', '10086', '8602', '测试爸爸', '测试', '10086', '测试', '57', '2017-08-03 10:43:09', '277', '57', '2017-08-03 10:43:15', '277', null, null);
INSERT INTO `t_family_information` VALUES ('11', '10086', '8603', '测试妈妈', '测试', '10086', '测试', '57', '2017-08-03 10:43:35', '277', '57', '2017-08-03 10:43:43', '277', null, null);

-- ----------------------------
-- Table structure for t_file_type
-- ----------------------------
DROP TABLE IF EXISTS `t_file_type`;
CREATE TABLE `t_file_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL COMMENT '类别',
  `type_code` varchar(50) DEFAULT NULL COMMENT '类别编码',
  `file_name` varchar(200) DEFAULT NULL COMMENT '文件夹名称',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父id',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `creater_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `updater_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `data_authority` int(11) DEFAULT NULL COMMENT '数据权限',
  `authority_id` varchar(2000) DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_file_type
-- ----------------------------
INSERT INTO `t_file_type` VALUES ('2', '', null, '测试目录文件夹', '', '1', null, '1', '2017-04-19 15:22:28', null, '1', '2017-04-28 11:22:08', null, '5', null);
INSERT INTO `t_file_type` VALUES ('3', '', null, '测试目录文件夹2', '', '0', null, '1', '2017-04-19 15:26:11', null, '1', '2017-04-28 11:22:25', null, '5', null);
INSERT INTO `t_file_type` VALUES ('4', '', null, '测试文件夹目录3', '', '3', null, '1', '2017-04-19 15:26:53', null, null, null, null, '5', null);
INSERT INTO `t_file_type` VALUES ('10', '', null, '测试文件目录4', '321', '4', null, '1', '2017-04-19 15:56:28', null, '1', '2017-04-28 10:27:03', null, '4', '28');
INSERT INTO `t_file_type` VALUES ('11', '测试文件目录4', null, '子目录测试', '', '0', '10', '1', '2017-04-19 16:57:29', null, '1', '2017-04-28 10:27:11', null, '4', '28');
INSERT INTO `t_file_type` VALUES ('12', '测试文件目录4', null, '子目录测试2', '111', '1', '10', '1', '2017-04-19 16:57:40', null, '1', '2017-04-19 16:58:32', null, '5', null);
INSERT INTO `t_file_type` VALUES ('13', '子目录测试', null, '子目录测试1', '123', '0', '11', '1', '2017-04-19 16:57:51', null, '1', '2017-04-28 15:49:23', null, '5', null);
INSERT INTO `t_file_type` VALUES ('15', '测试目录文件夹2', null, '测试子目录文件夹', '', '0', '3', '1', '2017-04-28 15:51:52', null, null, null, null, '4', '27');

-- ----------------------------
-- Table structure for t_file_url
-- ----------------------------
DROP TABLE IF EXISTS `t_file_url`;
CREATE TABLE `t_file_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `related_model` varchar(50) DEFAULT NULL COMMENT '关联模块',
  `related_id` int(11) DEFAULT NULL COMMENT '管理模块id',
  `url` varchar(1000) DEFAULT NULL COMMENT '文件路径',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_file_url
-- ----------------------------
INSERT INTO `t_file_url` VALUES ('11', '申请模块', '5', '/WelcomeNewsUpload/apply/01(2017-07-20).jpg', '2017-07-20 18:59:27');
INSERT INTO `t_file_url` VALUES ('20', '申请模块', '4', '/WelcomeNewsUpload/apply/01(2017-07-20).jpg', '2017-08-02 14:46:51');
INSERT INTO `t_file_url` VALUES ('21', '申请模块', '4', '/WelcomeNewsUpload/apply/bg(2017-07-20).jpg', '2017-08-02 14:46:51');
INSERT INTO `t_file_url` VALUES ('22', '申请模块', '1', '/WelcomeNewsUpload/apply/default(超级管理员20170802144758).png', '2017-08-02 14:48:01');
INSERT INTO `t_file_url` VALUES ('25', '申请模块', '7', '/WelcomeNewsUpload/apply/default(测试20170803105849).png', '2017-08-03 11:01:31');
INSERT INTO `t_file_url` VALUES ('26', '申请模块', '8', '/WelcomeNewsUpload/apply/text(测试20170803105942).jpg', '2017-08-03 11:01:44');

-- ----------------------------
-- Table structure for t_haved_payment_data
-- ----------------------------
DROP TABLE IF EXISTS `t_haved_payment_data`;
CREATE TABLE `t_haved_payment_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examinee_id` varchar(255) DEFAULT NULL COMMENT '考生号',
  `actually_money` varchar(255) DEFAULT NULL COMMENT '实际缴费金额',
  `derate_money` varchar(255) DEFAULT NULL COMMENT '减免金额',
  `deferred_money` varchar(255) DEFAULT NULL COMMENT '缓缴金额',
  `returns_money` varchar(255) DEFAULT NULL COMMENT '退费金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_haved_payment_data
-- ----------------------------

-- ----------------------------
-- Table structure for t_mw_approving
-- ----------------------------
DROP TABLE IF EXISTS `t_mw_approving`;
CREATE TABLE `t_mw_approving` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apply_id` int(11) DEFAULT NULL,
  `apply_type` int(11) DEFAULT NULL,
  `flow_id` int(11) DEFAULT NULL,
  `flow_role` int(11) DEFAULT NULL,
  `flow_user` int(11) DEFAULT NULL,
  `flow_level` int(11) DEFAULT NULL,
  `flow_state` int(11) DEFAULT NULL,
  `exp1` varchar(50) DEFAULT NULL,
  `exp2` varchar(50) DEFAULT NULL,
  `exp3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_mw_approving
-- ----------------------------

-- ----------------------------
-- Table structure for t_mw_flow_setting
-- ----------------------------
DROP TABLE IF EXISTS `t_mw_flow_setting`;
CREATE TABLE `t_mw_flow_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flow_code` varchar(50) DEFAULT NULL,
  `flow_name` varchar(255) DEFAULT NULL,
  `flow_role` int(11) DEFAULT NULL,
  `flow_user` int(11) DEFAULT NULL,
  `flow_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_mw_flow_setting
-- ----------------------------
INSERT INTO `t_mw_flow_setting` VALUES ('1', '1501', '班导师审批', '26', null, '1');
INSERT INTO `t_mw_flow_setting` VALUES ('2', '1501', '学工处审批', null, '45', '2');

-- ----------------------------
-- Table structure for t_mw_my_apply
-- ----------------------------
DROP TABLE IF EXISTS `t_mw_my_apply`;
CREATE TABLE `t_mw_my_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `apply_code` int(11) DEFAULT NULL COMMENT '所属申请（所属商机、所属计划）',
  `apply_matters` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '申请事项',
  `apply_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '申请名称',
  `apply_people` int(11) DEFAULT NULL COMMENT '申请人',
  `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
  `apply_reson` varchar(2000) CHARACTER SET utf8 DEFAULT NULL COMMENT '申请原因',
  `approval_status` int(11) DEFAULT NULL COMMENT '审批状态',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `creater_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `updater_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `extended_field_1` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '扩展字段1',
  `extended_field_2` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '扩展字段2',
  `extended_field_3` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '扩展字段3',
  `related_business` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '关联事项',
  `related_business_items` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '关联具体事项',
  `apply_file` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_mw_my_apply
-- ----------------------------
INSERT INTO `t_mw_my_apply` VALUES ('10', '1501', null, '测试通道', '57', '2017-08-03 17:40:06', '测试', '2', '57', '2017-08-03 17:40:17', '274', null, null, null, null, null, null, null, null, '');
INSERT INTO `t_mw_my_apply` VALUES ('11', '1502', null, '测试报到', '57', '2017-08-03 17:41:49', '测试啊', '0', '57', '2017-08-03 17:41:57', '274', '57', '2017-08-05 10:10:08', '274', null, null, null, null, null, '');

-- ----------------------------
-- Table structure for t_mw_my_approval
-- ----------------------------
DROP TABLE IF EXISTS `t_mw_my_approval`;
CREATE TABLE `t_mw_my_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `apply_id` int(11) DEFAULT NULL COMMENT '所属申请',
  `flow_role` int(11) DEFAULT NULL,
  `flow_user` int(11) DEFAULT NULL,
  `approval_people` int(11) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_opinion` varchar(2000) CHARACTER SET utf8 DEFAULT NULL COMMENT '审批意见',
  `approval_state` int(11) DEFAULT NULL,
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `creater_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `updater_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `extended_field_1` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '扩展字段1',
  `extended_field_2` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '扩展字段2',
  `extended_field_3` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`id`),
  KEY `fk` (`apply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_mw_my_approval
-- ----------------------------
INSERT INTO `t_mw_my_approval` VALUES ('2', '10', '26', null, '58', '2017-08-03 17:46:28', '', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO `t_mw_my_approval` VALUES ('3', '10', null, '45', '45', '2017-08-03 17:48:00', '', '0', null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for t_need_payment_data
-- ----------------------------
DROP TABLE IF EXISTS `t_need_payment_data`;
CREATE TABLE `t_need_payment_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examinee_id` varchar(255) DEFAULT NULL COMMENT '考生号',
  `charge_item` varchar(255) DEFAULT NULL COMMENT '项目',
  `assessment_money` varchar(255) DEFAULT NULL COMMENT '应收金额',
  `create_people` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_people` int(11) DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  `create_department` int(11) DEFAULT NULL,
  `update_department` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL COMMENT '网上已缴费/现场缴费/绿色通道',
  `standard` varchar(255) DEFAULT NULL COMMENT '标准',
  `actually_money` varchar(255) DEFAULT '0' COMMENT '实际缴费金额',
  `derate_money` varchar(255) DEFAULT '0' COMMENT '减免金额',
  `deferred_money` varchar(255) DEFAULT '0' COMMENT '缓缴金额',
  `returns_money` varchar(255) DEFAULT '0' COMMENT '退费金额',
  `pay_state` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_need_payment_data
-- ----------------------------
INSERT INTO `t_need_payment_data` VALUES ('1', '1410831115', '学费', '4000.00', '1', '2017-07-03', '1', '2017-08-02', '277', '1', '网上支付', '2017-2018学年', '0.00', '0.00', '0.00', '0.00', '已完成');
INSERT INTO `t_need_payment_data` VALUES ('7', '1410831113', '学费', '1000.00', '1', '2017-07-08', '1', '2017-07-17', '277', '277', '网上支付', '2017-2018学年', '1000.00', '0.00', '0.00', '0.00', '已完成');
INSERT INTO `t_need_payment_data` VALUES ('30', '1410831112', '奖学金', '5000.00', '1', '2017-07-10', null, null, '277', null, '网上支付', '2017-2018学年', '5000.00', '0.00', '0.00', '0.00', '未完成');
INSERT INTO `t_need_payment_data` VALUES ('31', '1410831109', '学费', '5230.00', '1', '2017-07-10', null, null, '277', null, '网上支付', '2017-2018学年', '0.00', '0.00', '0.00', '0.00', '未完成');
INSERT INTO `t_need_payment_data` VALUES ('33', '1410831102', '学费', '4000.00', '1', '2017-07-10', '1', '2017-07-11', '277', '277', '网上支付', '2017-2018学年', '4000.01', '0.00', '0.00', '0.00', '未完成');
INSERT INTO `t_need_payment_data` VALUES ('34', '1410831105', '学费', '4000.00', '1', '2017-07-10', '1', '2017-07-11', '277', '277', '网上支付', '2017-2018学年', '4000.02', '0.00', '0.00', '0.00', '未完成');
INSERT INTO `t_need_payment_data` VALUES ('35', '1410831122', '学费', '4000.00', '1', '2017-07-10', '1', '2017-07-11', '277', '277', '网上支付', '2017-2018学年', '4000.03', '0.00', '0.00', '0.00', '未完成');
INSERT INTO `t_need_payment_data` VALUES ('39', '1410831119', '学费', '4000.00', '1', '2017-07-10', null, null, '277', null, '网上支付', '2017-2018学年', '4000.00', '0.00', '0.00', '0.00', '未完成');
INSERT INTO `t_need_payment_data` VALUES ('40', '1310313127', '学费', '4000.00', '1', '2017-07-10', null, null, '277', null, '网上支付', '2017-2018学年', '4000.00', '0.00', '0.00', '0.00', '未完成');
INSERT INTO `t_need_payment_data` VALUES ('45', '1410831116', '学费', '4000.00', '1', '2017-07-10', '1', '2017-08-05', '277', '1', '网上支付', '2017-2018学年', '4000.00', '0.00', '0.00', '0.00', '已完成');
INSERT INTO `t_need_payment_data` VALUES ('49', '10086', '学费', '4000.00', '1', '2017-08-05', null, null, '1', null, '网上支付', '4000', '0.00', '0.00', '0.00', '0.00', '未完成');
INSERT INTO `t_need_payment_data` VALUES ('51', '1410831113', '奖学金', '3.00', '1', '2017-08-05', null, null, '1', null, '网上支付', '啊啊啊', '3.00', '3.00', '3.00', '3.00', '已完成');

-- ----------------------------
-- Table structure for t_pre_regist
-- ----------------------------
DROP TABLE IF EXISTS `t_pre_regist`;
CREATE TABLE `t_pre_regist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examinee_id` varchar(14) DEFAULT NULL COMMENT '考生号',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `phone_number` varchar(14) DEFAULT NULL COMMENT '联系方式',
  `estimated_time` datetime DEFAULT NULL COMMENT '预计到校时间',
  `transportation` varchar(10) DEFAULT NULL COMMENT '到校交通方式',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `stop` varchar(20) DEFAULT NULL COMMENT '乘车点',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_pre_regist
-- ----------------------------
INSERT INTO `t_pre_regist` VALUES ('60', '1310313127', '王爽', '13927828409', '2017-07-06 00:00:00', '自乘', '47', '2017-07-06 16:36:39', '273', null, null, null, null);
INSERT INTO `t_pre_regist` VALUES ('61', '1410831115', '杨旭杰', '13907180888', '2017-07-12 01:00:00', '校迎新车辆', '41', '2017-07-12 10:27:29', '277', '41', '2017-08-03 10:49:04', '277', '1802');
INSERT INTO `t_pre_regist` VALUES ('64', '1410831102', '阿斯哈提', '15820391238', '2017-07-03 00:00:00', '校迎新车辆', '49', '2017-07-12 11:05:02', '278', null, null, null, '1801');
INSERT INTO `t_pre_regist` VALUES ('75', '10086', '测试', '10086', '2017-08-03 14:49:57', '校迎新车辆', '57', '2017-08-03 14:50:08', '274', '57', '2017-08-03 18:03:37', '274', '1802');

-- ----------------------------
-- Table structure for t_registration_management
-- ----------------------------
DROP TABLE IF EXISTS `t_registration_management`;
CREATE TABLE `t_registration_management` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examinee_id` varchar(255) DEFAULT NULL COMMENT '考生号',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `type` varchar(255) DEFAULT NULL COMMENT '类别',
  `state` varchar(255) DEFAULT NULL COMMENT '状态',
  `pay_ack` varchar(255) DEFAULT NULL COMMENT '关联id',
  `goods_ack` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_people` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_people` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `task_id` varchar(255) DEFAULT NULL COMMENT '任务id',
  `create_department` int(11) DEFAULT NULL,
  `update_department` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_registration_management
-- ----------------------------
INSERT INTO `t_registration_management` VALUES ('10', '1410831115', '新生签到', null, '1', null, null, '1', '2017-07-01 11:52:34', null, null, '8', '1', null);
INSERT INTO `t_registration_management` VALUES ('13', '1410831115', '测试事项', null, '1', null, null, '277', '2017-07-11 13:57:23', null, null, '12', '1', null);
INSERT INTO `t_registration_management` VALUES ('19', '1410831116', '新生签到', null, '1', null, null, '277', '2017-07-11 18:43:58', null, null, '8', '1', null);
INSERT INTO `t_registration_management` VALUES ('20', '1410831102', '新生签到', null, '1', null, null, '277', '2017-07-11 18:44:46', null, null, '8', '1', null);
INSERT INTO `t_registration_management` VALUES ('24', '1410831116', '新生寝室', null, '1', null, null, '1', '2017-07-17 12:35:42', null, null, '10', '277', null);
INSERT INTO `t_registration_management` VALUES ('25', '1410831113', '新生签到', null, '1', null, null, '36', '2017-07-31 17:40:23', null, null, '8', '136', null);
INSERT INTO `t_registration_management` VALUES ('27', '1410831113', '新生缴费', null, '1', null, null, '36', '2017-07-31 17:44:01', null, null, '9', '136', null);
INSERT INTO `t_registration_management` VALUES ('28', '1410831113', '新生寝室', null, '1', null, null, '36', '2017-07-31 17:50:49', null, null, '10', '136', null);
INSERT INTO `t_registration_management` VALUES ('29', '1410831113', '新生寝室', null, '1', null, null, '36', '2017-07-31 17:50:50', null, null, '10', '136', null);
INSERT INTO `t_registration_management` VALUES ('30', '1410831113', '测试事项', null, '1', null, null, '36', '2017-07-31 17:53:06', null, null, '12', '136', null);
INSERT INTO `t_registration_management` VALUES ('31', '1410831115', '新生已购买物资发放', null, '1', null, null, '36', '2017-07-31 17:54:09', null, null, '11', '136', null);
INSERT INTO `t_registration_management` VALUES ('35', '10086', '新生签到', null, '1', null, null, '59', '2017-08-03 15:43:25', null, null, '8', '128', null);
INSERT INTO `t_registration_management` VALUES ('40', '10086', '新生已购买物资发放', null, '1', null, null, '59', '2017-08-03 15:49:19', null, null, '11', '128', null);
INSERT INTO `t_registration_management` VALUES ('43', '10086', '测试事项', null, '1', null, null, '59', '2017-08-03 15:51:28', null, null, '12', '128', null);
INSERT INTO `t_registration_management` VALUES ('44', '10086', '新生寝室', null, '1', null, null, '59', '2017-08-05 09:43:20', null, null, '10', '128', null);
INSERT INTO `t_registration_management` VALUES ('45', '10086', '新生寝室', null, '1', null, null, '59', '2017-08-05 09:43:20', null, null, '10', '128', null);
INSERT INTO `t_registration_management` VALUES ('46', '1410831113', '新生缴费', null, '1', null, null, '1', '2017-08-05 16:54:36', null, null, '9', '1', null);
INSERT INTO `t_registration_management` VALUES ('47', '1410831113', '新生缴费', null, '1', null, null, '1', '2017-08-05 17:04:26', null, null, '9', '1', null);
INSERT INTO `t_registration_management` VALUES ('48', '10000000', '新生缴费', null, '1', null, null, '1', '2017-08-05 17:05:19', null, null, '9', '1', null);

-- ----------------------------
-- Table structure for t_relationship
-- ----------------------------
DROP TABLE IF EXISTS `t_relationship`;
CREATE TABLE `t_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `register_id` int(11) DEFAULT NULL COMMENT '预登记ID',
  `relationship` varchar(20) DEFAULT '' COMMENT '随从亲友关系',
  `name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `contact_information` varchar(14) DEFAULT NULL COMMENT '联系方式',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_relationship
-- ----------------------------
INSERT INTO `t_relationship` VALUES ('16', '64', '父子', '啊麦提', '13456789042', '49', '2017-07-12 18:18:54', '278', null, null, null);
INSERT INTO `t_relationship` VALUES ('28', '64', '222', '222', '22', '49', '2017-07-12 19:02:45', '278', null, null, null);
INSERT INTO `t_relationship` VALUES ('29', '64', '222', '2223333', '22', '49', '2017-07-12 19:02:54', '278', null, null, null);
INSERT INTO `t_relationship` VALUES ('30', '64', '母子', '3244311333', '35554111', '49', '2017-07-12 19:02:54', '278', null, null, null);
INSERT INTO `t_relationship` VALUES ('31', '64', '姐弟', '阿丽亚333', '13988078908', '49', '2017-07-12 19:02:54', '278', null, null, null);
INSERT INTO `t_relationship` VALUES ('34', '61', '8603', '杨xx', '13022222222', null, null, null, null, null, null);
INSERT INTO `t_relationship` VALUES ('47', '75', '8602', '测试爸爸', '10086', null, null, null, null, null, null);
INSERT INTO `t_relationship` VALUES ('50', '75', '8603', '测试妈妈', '10086', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for t_room
-- ----------------------------
DROP TABLE IF EXISTS `t_room`;
CREATE TABLE `t_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '寝室ID',
  `bid` int(11) DEFAULT NULL COMMENT '楼栋ID',
  `code` varchar(11) DEFAULT NULL COMMENT '寝室编号',
  `name` varchar(20) DEFAULT NULL COMMENT '寝室名称',
  `property` varchar(20) DEFAULT NULL COMMENT '寝室属性',
  `price` int(11) DEFAULT NULL COMMENT '价格',
  `type` varchar(20) DEFAULT NULL COMMENT '寝室类型',
  `layer` int(11) DEFAULT NULL COMMENT '楼层',
  `college` varchar(20) DEFAULT NULL COMMENT '所属院系',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_room
-- ----------------------------
INSERT INTO `t_room` VALUES ('2', '2', '310', '310号', '4人间', '1250', '男寝', '3', null, '欢迎', null, null, null, '1', '2017-07-11 18:33:14', '277');
INSERT INTO `t_room` VALUES ('3', '2', '311', '311号', '4人间', '1250', '男寝', '3', null, '欢迎', null, null, null, '1', '2017-07-11 18:30:35', '277');
INSERT INTO `t_room` VALUES ('4', '10', '410', '410号', '4人间', '1250', '女寝', '4', null, '', null, null, null, '1', '2017-07-11 18:06:39', '277');
INSERT INTO `t_room` VALUES ('5', '10', '411', '411号', '4人间', '1250', '女寝', '4', null, '', null, null, null, '1', '2017-07-11 18:06:30', '277');
INSERT INTO `t_room` VALUES ('6', '15', '102', '102号', '4人间', '1250', '男寝', '1', null, '', null, null, null, '1', '2017-07-11 18:06:47', '277');
INSERT INTO `t_room` VALUES ('7', '15', '103', '103号', '4人间', '1250', '男寝', '1', null, '', null, null, null, '1', '2017-07-11 18:13:05', '277');
INSERT INTO `t_room` VALUES ('8', '20', '308', '308号', '4人间', '1250', '男寝', '3', null, '', null, null, null, '1', '2017-07-11 18:02:33', '277');
INSERT INTO `t_room` VALUES ('9', '20', '309', '309号', '4人间', '1250', '男寝', '3', null, '', null, null, null, '1', '2017-07-11 18:06:19', '277');
INSERT INTO `t_room` VALUES ('10', '21', '411', '411号', '4人间', '1250', '男寝', '4', null, '', null, null, null, '1', '2017-07-11 18:15:24', '277');
INSERT INTO `t_room` VALUES ('11', '21', '412', '412号', '4人间', '1250', '男寝', '4', null, '', null, null, null, '1', '2017-07-11 18:15:31', '277');
INSERT INTO `t_room` VALUES ('20', '30', '1008', '1008号', '4人间', '1250', '男寝', '0', '测试', '测试', '1', '2017-08-02 16:01:48', '1', null, null, null);

-- ----------------------------
-- Table structure for t_roommate
-- ----------------------------
DROP TABLE IF EXISTS `t_roommate`;
CREATE TABLE `t_roommate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `major` varchar(64) DEFAULT NULL COMMENT '专业',
  `college` varchar(64) DEFAULT NULL COMMENT '学院',
  `grade` varchar(64) DEFAULT NULL COMMENT '班级',
  `contact_information` varchar(11) DEFAULT NULL COMMENT '联系方式',
  `nation` varchar(20) DEFAULT NULL COMMENT '民族',
  `email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_roommate
-- ----------------------------
INSERT INTO `t_roommate` VALUES ('2', '黄任峥', '207', null, '276', '18078907862', '8501', '32443@qq.com', 'dsfdffdd', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for t_sd_base_information
-- ----------------------------
DROP TABLE IF EXISTS `t_sd_base_information`;
CREATE TABLE `t_sd_base_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `student_id` varchar(10) DEFAULT NULL COMMENT '学号',
  `examinee_id` varchar(14) DEFAULT NULL COMMENT '考生号',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(11) DEFAULT NULL COMMENT '性别',
  `contact_information` varchar(11) DEFAULT NULL COMMENT '联系方式',
  `people_id` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `nation` varchar(64) DEFAULT NULL COMMENT '民族',
  `birthday` date DEFAULT NULL COMMENT '出生日期',
  `photo` varchar(200) DEFAULT NULL COMMENT '照片',
  `college` varchar(64) DEFAULT NULL COMMENT '学院',
  `department` varchar(64) DEFAULT NULL COMMENT '系别',
  `major` varchar(64) DEFAULT NULL COMMENT '专业',
  `class_name` varchar(64) DEFAULT NULL COMMENT '班级',
  `school_length` int(1) DEFAULT NULL COMMENT '学制',
  `entrance_time` date DEFAULT NULL COMMENT '入学日期',
  `political_status` varchar(64) DEFAULT NULL COMMENT '政治面貌',
  `source_place` varchar(200) DEFAULT NULL COMMENT '生源地',
  `family_address` varchar(64) DEFAULT NULL COMMENT '家庭地址',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `examination_scores` int(3) DEFAULT NULL COMMENT '高考分数',
  `registered_residence` varchar(64) DEFAULT NULL COMMENT '户口所在地',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sd_base_information
-- ----------------------------
INSERT INTO `t_sd_base_information` VALUES ('17', '1410831115', '1410831115', '学生', '1901', '15992969918', '440223199506280010', '8501', '2017-08-02', '', '128', '140', '194', '274', '4', '2017-08-02', '101', '2219', '广东韶关金叶花园19栋301', '616253516@qq.com', '688', '广东韶关', '1', '2017-06-27 09:41:33', '1', '1', '2017-08-05 14:26:36', '1');
INSERT INTO `t_sd_base_information` VALUES ('28', '1410831115', '1410831113', '黄任峥', '1901', '15623712622', '440882199504281119', '8518', '2017-08-03', '', '136', '175', '248', '277', '4', '2017-08-03', '104', '2218', '湛江市霞山区世纪广场附近', '2323915348@qq.com', '663', '湛江雷州', '1', '2017-08-03 20:57:43', '277', '1', '2017-08-05 11:26:24', '1');
INSERT INTO `t_sd_base_information` VALUES ('29', '1410831115', '1310313127', '王爽', '1902', '15992969918', '421181199501020442', '8501', '2017-07-06', '', '128', '140', '194', '274', '4', '2017-07-11', '101', '2203', '新奇额街908号', '12392231@qq.com', '666', '河北省鞍山市', '1', '2017-07-06 16:35:14', '277', '1', '2017-07-12 14:20:08', '277');
INSERT INTO `t_sd_base_information` VALUES ('30', '1410831115', '1410831116', '华凯伦', '1901', '15992969918', '440223199506280010', '8503', '2017-07-10', '', '136', '175', '248', '277', '4', '2017-07-10', '101', '2228', '甘肃', '616263516@qq.com', '732', '甘肃长蛇', '1', '2017-07-10 12:01:52', '277', '1', '2017-07-10 15:24:24', '277');
INSERT INTO `t_sd_base_information` VALUES ('31', '1410831115', '1410831122', '喻雅洁', '1902', '15992969918', '441008199604281169', '8501', '1995-02-06', '', '128', '140', '194', '274', '4', '2017-07-11', '105', '2215', '汽修学院', 'suntuer@sina.com', '600', '山东蓝翔', '1', '2017-07-10 12:02:32', '277', '1', '2017-07-11 14:48:03', '277');
INSERT INTO `t_sd_base_information` VALUES ('32', '1410831115', '1410831119', '王栋', '1902', '13927828409', '440882199604281116', '8501', '2017-07-11', '', '128', '140', '194', '273', '4', '2017-07-11', '102', '2217', '武汉市洪山区南湖李子路湖北工业大学南区40栋1404', '949222516@qq.com', '590', '湖北武汉', '1', '2017-07-10 13:51:06', '277', '1', '2017-07-11 14:57:32', '277');
INSERT INTO `t_sd_base_information` VALUES ('33', '1410831115', '1410831105', '张坚城', '1901', '15920139980', '440882199304232226', '8504', '1995-06-30', '', '136', '175', '249', '278', '4', '2017-08-31', '113', '2205', '游牧民族没有家', '4242048212@qq.com', '566', '内蒙古大草原', '1', '2017-07-10 14:02:15', '277', '1', '2017-07-11 14:59:31', '277');
INSERT INTO `t_sd_base_information` VALUES ('34', '1410831115', '1410831102', '阿斯哈提', '1901', '15820391238', '440882199405263336', '8502', '1994-07-06', '', '136', '175', '248', '277', '4', '2017-08-31', '113', '2231', '酱香饼大街', '237294101@qq. com', '490', '新疆土家村', '1', '2017-07-10 14:09:13', '277', '49', '2017-07-12 19:05:28', '278');
INSERT INTO `t_sd_base_information` VALUES ('35', '1410831115', '1410831109', '刘伟', '1901', '13920183872', '440882199304298886', '8503', '1993-07-01', '', '136', '175', '249', '278', '4', '2017-08-31', '113', '2209', '上海文明大道001', '238201311234@qq.com', '555', '上海全百区', '1', '2017-07-10 14:10:19', '277', '1', '2017-07-11 15:01:11', '277');
INSERT INTO `t_sd_base_information` VALUES ('36', '1410831115', '10086', '测试', '1901', '10086', '440223199606280000', '8501', '1990-01-01', '', '128', '140', '194', '274', '4', '1990-01-01', '101', '2219', '测试', '10086@qq.com', '688', '测试', '1', '2017-08-02 16:50:06', '1', '57', '2017-08-03 14:18:34', '274');
INSERT INTO `t_sd_base_information` VALUES ('37', '1410831115', '1410831100', '一轮测试', '1901', '13907180888', '440223199506280010', '8501', '1995-06-28', '', '128', '140', '194', '274', '4', '2017-08-05', '101', '2219', '圣体花园', '10086@qq.com', '750', '广东韶关', '1', '2017-08-05 14:42:46', '1', '1', '2017-08-05 14:44:15', '1');

-- ----------------------------
-- Table structure for t_spot_matter
-- ----------------------------
DROP TABLE IF EXISTS `t_spot_matter`;
CREATE TABLE `t_spot_matter` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `spot_id` int(11) DEFAULT NULL COMMENT '点位id',
  `matter` varchar(64) DEFAULT NULL COMMENT '办理事项',
  `matter_describe` varchar(2000) DEFAULT NULL COMMENT '描述',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_spot_matter
-- ----------------------------
INSERT INTO `t_spot_matter` VALUES ('7', '4', '各项新生杂事', '此点位为管理学院学工处所在。\r\n需要各种帮助可以直接联系。', '1', '2017-06-30 09:06:02', '1', '1', '2017-07-01 14:09:08', '1', '不需办理');
INSERT INTO `t_spot_matter` VALUES ('8', '5', '新生签到', '新生报到院系签到', '1', '2017-06-30 09:07:32', '1', '1', '2017-08-05 09:36:22', '1', '需办理');
INSERT INTO `t_spot_matter` VALUES ('9', '5', '新生缴费', '新生缴费确认，确认完毕才可以领取宿舍凭单。\r\n如果网上缴费的新生，可以根据财务缴费单直接领取宿舍凭单。\r\n如果现场缴费，先去xx地进行现场缴费再根据凭单领取宿舍凭单。\r\n需要申请绿色通道的，则需要向学工处进行申请。\r\n确认地点：AA区', '1', '2017-06-30 09:19:55', '1', '1', '2017-07-03 14:01:37', '277', '需办理');
INSERT INTO `t_spot_matter` VALUES ('10', '5', '新生寝室', '发放宿舍凭单，非网上缴费新生进行寝室确认。', '1', '2017-06-30 09:26:29', '1', '1', '2017-07-03 14:01:42', '277', '需办理');
INSERT INTO `t_spot_matter` VALUES ('11', '5', '新生已购买物资发放', '网上购买生活物品人员，进行生活物品领取', '1', '2017-06-30 09:27:09', '1', '59', '2017-08-03 15:59:44', '128', '需办理');
INSERT INTO `t_spot_matter` VALUES ('12', '5', '测试事项', '仅供测试', '1', '2017-07-11 13:56:47', '277', '59', '2017-08-03 15:59:29', '128', '需办理');

-- ----------------------------
-- Table structure for t_spot_people
-- ----------------------------
DROP TABLE IF EXISTS `t_spot_people`;
CREATE TABLE `t_spot_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `spot_id` int(11) DEFAULT NULL COMMENT '点位id',
  `charge` varchar(5) DEFAULT NULL COMMENT '负责人/值班人',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `contact_information` varchar(11) DEFAULT NULL COMMENT '联系方式',
  `role` varchar(10) DEFAULT NULL COMMENT '学生/老师',
  `student_id` varchar(10) DEFAULT NULL COMMENT '学号（如果是学生，填报学号）',
  `scheduling_time_s` datetime DEFAULT NULL COMMENT '排班时间',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `scheduling_time_e` datetime DEFAULT NULL COMMENT '排班结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_spot_people
-- ----------------------------
INSERT INTO `t_spot_people` VALUES ('52', '4', '1702', null, null, null, '36', null, '1', '2017-08-03 14:43:43', '1', null, null, null, null);
INSERT INTO `t_spot_people` VALUES ('53', '4', '1702', null, null, null, '56', null, '1', '2017-08-03 14:43:43', '1', null, null, null, null);
INSERT INTO `t_spot_people` VALUES ('54', '4', '1701', null, null, null, '36', null, '1', '2017-08-03 14:43:43', '1', null, null, null, null);
INSERT INTO `t_spot_people` VALUES ('55', '4', '1701', null, null, null, '56', null, '1', '2017-08-03 14:43:43', '1', null, null, null, null);
INSERT INTO `t_spot_people` VALUES ('56', '14', '1702', null, null, null, '56', null, '1', '2017-08-03 14:44:01', '277', null, null, null, null);
INSERT INTO `t_spot_people` VALUES ('57', '14', '1701', null, null, null, '56', null, '1', '2017-08-03 14:44:01', '277', null, null, null, null);
INSERT INTO `t_spot_people` VALUES ('60', '18', '1702', null, null, null, '59', null, '1', '2017-08-03 15:29:47', '1', null, null, null, null);
INSERT INTO `t_spot_people` VALUES ('61', '18', '1701', null, null, null, '59', null, '1', '2017-08-03 15:29:47', '1', null, null, null, null);

-- ----------------------------
-- Table structure for t_spot_set
-- ----------------------------
DROP TABLE IF EXISTS `t_spot_set`;
CREATE TABLE `t_spot_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `spot_name` varchar(64) DEFAULT NULL COMMENT '点位名称',
  `spot_x` double DEFAULT NULL COMMENT '点位X',
  `spot_y` double DEFAULT NULL COMMENT '点位Y',
  `college` varchar(64) DEFAULT NULL COMMENT '学院',
  `department` varchar(64) DEFAULT NULL COMMENT '系部',
  `major` varchar(64) DEFAULT NULL COMMENT '标识类别（所属类别为“标识点位”时选择）',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  `spot_type` varchar(255) DEFAULT NULL COMMENT '所属类别',
  `detail_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_spot_set
-- ----------------------------
INSERT INTO `t_spot_set` VALUES ('4', '学工办理点', '114.317491', '30.491036', '136', null, '', '1', '2017-06-30 09:03:14', '1', '36', '2017-08-03 14:43:42', '136', '2301', '11111');
INSERT INTO `t_spot_set` VALUES ('5', '管理学院报到点', '114.317922', '30.488184', '136', null, null, '1', '2017-06-30 09:04:10', '1', '1', '2017-06-30 09:06:35', '1', '2301', null);
INSERT INTO `t_spot_set` VALUES ('6', '中区食堂', '114.314733', '30.488636', '128', null, '食堂', '1', '2017-07-07 16:34:18', '277', '1', '2017-07-10 10:23:01', '277', '2302', null);
INSERT INTO `t_spot_set` VALUES ('7', '计算机学院报道点', '114.315394', '30.486519', '132', null, '', '1', '2017-07-08 16:15:41', '277', null, null, null, '2301', null);
INSERT INTO `t_spot_set` VALUES ('8', '中区图书馆', '114.31357', '30.485578', '', null, '图书馆', '1', '2017-07-08 16:57:48', '277', null, null, null, '2302', null);
INSERT INTO `t_spot_set` VALUES ('9', '创新苑11', '114.311392', '30.488511', '', null, '宿舍', '1', '2017-07-08 16:58:39', '277', '1', '2017-07-10 10:21:46', '277', '2302', null);
INSERT INTO `t_spot_set` VALUES ('10', '创新苑12', '114.31149', '30.488892', '', null, '宿舍', '1', '2017-07-08 16:59:05', '277', '1', '2017-07-10 10:22:40', '277', '2302', null);
INSERT INTO `t_spot_set` VALUES ('11', '创新苑10', '114.31123', '30.488192', '', null, '宿舍', '1', '2017-07-08 16:59:41', '277', '1', '2017-08-04 18:16:16', '1', '2302', '');
INSERT INTO `t_spot_set` VALUES ('12', '中区体育馆', '114.316404', '30.487258', '', null, '体育馆', '1', '2017-07-08 17:02:41', '277', null, null, null, '2302', null);
INSERT INTO `t_spot_set` VALUES ('13', '第二体育场', '114.31247', '30.483477', '', null, '体育馆', '1', '2017-07-08 17:03:35', '277', null, null, null, '2302', null);
INSERT INTO `t_spot_set` VALUES ('14', '测试点位', '114.316817', '30.487577', '136', null, '', '1', '2017-07-19 15:53:26', '277', '36', '2017-08-03 14:44:01', '136', '2301', '2111');
INSERT INTO `t_spot_set` VALUES ('18', '机械院办理地点', '114.315492', '30.486971', '128', null, '', '1', '2017-08-03 15:29:46', '1', null, null, null, '2301', '超市旁边!特别好找!');

-- ----------------------------
-- Table structure for t_student_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_student_goods`;
CREATE TABLE `t_student_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `examinee_id` varchar(255) DEFAULT NULL COMMENT '考生号',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `contact_information` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `goods_list` varchar(255) DEFAULT NULL COMMENT '物品清单',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_student_goods
-- ----------------------------
INSERT INTO `t_student_goods` VALUES ('1', '14108311130', '黄任峥', '15623712622', '课本 军训服 帽子 鞋子 小凳子', '1', '2017-06-28 17:14:24', '1', '1', '2017-08-02 17:10:20', '1');
INSERT INTO `t_student_goods` VALUES ('2', '1310313127', '王爽', '13543520609', '课本 军训服 帽子 鞋子 小凳子', '1', '2017-06-28 17:24:56', '1', '1', '2017-07-11 17:48:05', '277');
INSERT INTO `t_student_goods` VALUES ('4', '1410831116', '华凯伦', '15623712665', '课本，军训服，帽子，鞋子，小凳子', '1', '2017-06-28 18:02:04', '1', '1', '2017-07-11 17:47:13', '277');
INSERT INTO `t_student_goods` VALUES ('5', '1410831109', '刘伟', '15632653698', '课本，军训服，帽子，鞋子，小凳子', '1', '2017-06-28 18:04:22', '1', '1', '2017-07-11 17:46:49', '277');
INSERT INTO `t_student_goods` VALUES ('7', '1410831119', '王栋', '15636985632', '课本 军训服 帽子 鞋子 小凳子 腰带', '1', '2017-06-29 09:44:27', '1', '1', '2017-07-11 17:46:23', '277');
INSERT INTO `t_student_goods` VALUES ('8', '1410831115', '杨旭杰', '13026132222', '课本，军训服，帽子，鞋子，小凳子', '1', '2017-07-03 11:15:13', '1', null, null, null);
INSERT INTO `t_student_goods` VALUES ('9', '1410831122', '喻雅洁', '15632698523', '课本，军训服，帽子，鞋子，小凳子', '1', '2017-07-11 17:46:05', '277', null, null, null);
INSERT INTO `t_student_goods` VALUES ('10', '1410831106', '张坚城', '14563269853', '课本，军训服，帽子，鞋子，小凳子', '1', '2017-07-11 17:49:05', '277', null, null, null);
INSERT INTO `t_student_goods` VALUES ('11', '1410831102', '阿斯哈提', '14563562365', '课本，军训服，帽子，鞋子，小凳子', '1', '2017-07-11 17:49:45', '277', null, null, null);
INSERT INTO `t_student_goods` VALUES ('16', '1410831135', '杜海涛', '15623654563', '课本，军训鞋，帽子，鞋子，小凳子', '1', '2017-08-05 16:48:00', '1', null, null, null);

-- ----------------------------
-- Table structure for t_stu_dormitory
-- ----------------------------
DROP TABLE IF EXISTS `t_stu_dormitory`;
CREATE TABLE `t_stu_dormitory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examinee_id` varchar(14) DEFAULT NULL COMMENT '考生号',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `build_id` int(11) DEFAULT NULL COMMENT '楼栋ID',
  `room_id` int(11) DEFAULT NULL COMMENT '宿舍ID',
  `bed_id` int(11) DEFAULT NULL COMMENT '床位ID',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_people` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_department` int(11) DEFAULT NULL COMMENT '创建人所在部门',
  `update_people` int(11) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_department` int(11) DEFAULT NULL COMMENT '更新人所在部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_stu_dormitory
-- ----------------------------
INSERT INTO `t_stu_dormitory` VALUES ('5', '1410831115', '学生', '30', '20', '61', '测试', null, null, null, '1', '2017-08-02 17:03:15', '1');
INSERT INTO `t_stu_dormitory` VALUES ('6', '1410831102', '阿斯哈提', '15', '6', '27', '', null, null, null, '1', '2017-07-11 18:20:48', '277');
INSERT INTO `t_stu_dormitory` VALUES ('9', '1410831109', '刘伟', '15', '6', '28', '', null, null, null, '1', '2017-07-12 16:30:15', '277');
INSERT INTO `t_stu_dormitory` VALUES ('10', '1410831106', '张坚城', '15', '6', '29', '', null, null, null, '1', '2017-07-11 18:25:17', '277');
INSERT INTO `t_stu_dormitory` VALUES ('13', '1410831113', '黄任峥', '2', '2', '9', '', null, null, null, '1', '2017-07-11 18:25:33', '277');
INSERT INTO `t_stu_dormitory` VALUES ('14', '1410831116', '华凯伦', '2', '2', '10', '', null, null, null, '1', '2017-07-11 18:25:42', '277');
INSERT INTO `t_stu_dormitory` VALUES ('15', '1410831119', '王栋', '10', '4', '18', '', null, null, null, '1', '2017-07-11 18:40:36', '277');
INSERT INTO `t_stu_dormitory` VALUES ('17', '1410831122', '喻雅洁', '10', '4', '17', '', '1', '2017-07-11 18:33:00', '277', null, null, null);
INSERT INTO `t_stu_dormitory` VALUES ('18', '1310313127', '王爽', '10', '4', '16', '', null, null, null, '1', '2017-07-11 18:40:16', '277');
INSERT INTO `t_stu_dormitory` VALUES ('22', '1410831135', '杜安黎', '10', '4', '18', '', null, null, null, '1', '2017-07-12 18:13:56', '277');

-- ----------------------------
-- View structure for v_allbed_base
-- ----------------------------
DROP VIEW IF EXISTS `v_allbed_base`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER  VIEW `v_allbed_base` AS select `b`.`code` AS `buildcode`,`r`.`code` AS `rcode`,`tb`.`code` AS `bedcode`,`tb`.`bid` AS `buildid`,`tb`.`rid` AS `roomid`,`tb`.`id` AS `bedid` from ((`t_room` `r` left join `t_build` `b` on((`r`.`bid` = `b`.`id`))) left join `t_bed` `tb` on((`tb`.`rid` = `r`.`id`))) group by `tb`.`id` ; ;

-- ----------------------------
-- View structure for v_all_bedinroom
-- ----------------------------
DROP VIEW IF EXISTS `v_all_bedinroom`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER  VIEW `v_all_bedinroom` AS select `vb`.`buildcode` AS `buildcode`,`vb`.`rcode` AS `rcode`,`vb`.`bedcode` AS `bedcode`,`tsd`.`name` AS `sname`,`vb`.`buildid` AS `buildid`,`vb`.`roomid` AS `roomid`,`vb`.`bedid` AS `bedid`,`tsd`.`examinee_id` AS `examinee_id` from (`v_allbed_base` `vb` left join `t_stu_dormitory` `tsd` on((`tsd`.`bed_id` = `vb`.`bedid`))) group by `vb`.`bedid` ; ;

-- ----------------------------
-- View structure for v_approval
-- ----------------------------
DROP VIEW IF EXISTS `v_approval`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_approval` AS select `t_mw_my_approval`.`apply_id` AS `apply_id`,`t_mw_my_approval`.`flow_role` AS `flow_role`,`t_mw_my_approval`.`flow_user` AS `flow_user`,`t_mw_my_approval`.`approval_people` AS `approval_people`,`t_mw_my_approval`.`approval_time` AS `approval_time`,`t_mw_my_approval`.`approval_opinion` AS `approval_opinion`,`t_mw_my_approval`.`approval_state` AS `approval_state`,2 AS `flow_state` from `t_mw_my_approval` union all select `t_mw_approving`.`apply_id` AS `apply_id`,`t_mw_approving`.`flow_role` AS `flow_role`,`t_mw_approving`.`flow_user` AS `flow_user`,NULL AS `NULL`,NULL AS `NULL`,NULL AS `NULL`,NULL AS `NULL`,`t_mw_approving`.`flow_state` AS `flow_state` from `t_mw_approving` ; ;

-- ----------------------------
-- View structure for v_not_null_room
-- ----------------------------
DROP VIEW IF EXISTS `v_not_null_room`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_not_null_room` AS select #已住房的状态，未满，已满
	t.bid,t1.build_id,t.rid,t1.room_id,
	CASE when t.total=t1.total then '已满' ELSE '未满' END state
from v_room_bed t,v_room_stu t1
where t.rid=t1.room_id ; ;

-- ----------------------------
-- View structure for v_null_bed
-- ----------------------------
DROP VIEW IF EXISTS `v_null_bed`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_null_bed` AS SELECT t.bid,t.rid,t.id,t.`code` from t_bed t LEFT JOIN t_stu_dormitory t1 on t.id=t1.bed_id where t1.id is NULL  #空床 ;

-- ----------------------------
-- View structure for v_null_room
-- ----------------------------
DROP VIEW IF EXISTS `v_null_room`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_null_room` AS SELECT t.bid,t.id rid,t.id,t.`code` from t_room t LEFT JOIN t_stu_dormitory t1 on t.id=t1.room_id where t1.id is NULL  #空房 ;

-- ----------------------------
-- View structure for v_room_all
-- ----------------------------
DROP VIEW IF EXISTS `v_room_all`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_room_all` AS select 
`b`.`id` AS `bid`,
`b`.`aid` AS `aid`,
`b`.`code` AS `bcode`,
`b`.`name` AS `bname`,
`b`.`layer` AS `maxlayer`,
`b`.`address` AS `address`,
`b`.`type` AS `btype`,
`b`.`remark` AS `bremark`,
`b`.`college` AS `college`,
`r`.`id` AS `rid`,
`r`.`code` AS `rcode`,
`r`.`name` AS `rname`,
`r`.`property` AS `property`,
`r`.`price` AS `price`,
`r`.`type` AS `rtype`,
`r`.`layer` AS `rlayer`,
vs.state AS rstatus
from 
`t_room` `r` 
left join `t_build` `b` on`r`.`bid` = `b`.`id`
LEFT JOIN v_room_status vs ON r.id = vs.rid ;

-- ----------------------------
-- View structure for v_room_bed
-- ----------------------------
DROP VIEW IF EXISTS `v_room_bed`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_room_bed` AS select bid,rid,COUNT(1)total from t_bed t GROUP BY t.rid #每间房有多少人 ;

-- ----------------------------
-- View structure for v_room_status
-- ----------------------------
DROP VIEW IF EXISTS `v_room_status`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_room_status` AS select 
`t`.`rid` AS `rid`,
(case when (`t`.`total` = `t1`.`total`) then '已满' else '未满' end) AS `state` from 
(`v_room_bed` `t` join `v_room_stu` `t1`) 
where (`t`.`rid` = `t1`.`room_id`)
UNION ALL
select 
`t`.`id` AS `rid`,
(case when (t.id IS NOT NULL) then '空房' else 'null' end) AS `state`
 from (`t_room` `t` left join `t_stu_dormitory` `t1` on((`t`.`id` = `t1`.`room_id`))) where isnull(`t1`.`id`) ;

-- ----------------------------
-- View structure for v_room_stu
-- ----------------------------
DROP VIEW IF EXISTS `v_room_stu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_room_stu` AS select build_id,room_id,COUNT(1) total FROM t_stu_dormitory GROUP BY room_id#没间房住了多少人 ;

-- ----------------------------
-- View structure for v_spot_matter
-- ----------------------------
DROP VIEW IF EXISTS `v_spot_matter`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_spot_matter` AS SELECT
	tss.id,
	tsm.id taskId,
	group_concat(tss.spot_name) spot_name,
	tss.spot_x,
	tss.spot_y,
	tss.college,
	tsm.type,
	tsm.matter,
	tsm.matter_describe	,
	tss.spot_type
FROM
	spot_and_matter t,
	t_spot_matter tsm,
	t_spot_set tss
WHERE
	t.spot_id=tss.id
	and
	t.matter_id =tsm.id 
GROUP BY TASKID,college ;

-- ----------------------------
-- View structure for v_spot_student
-- ----------------------------
DROP VIEW IF EXISTS `v_spot_student`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_spot_student` AS select 
	a.id,
	a.taskId,
  a.college,
	a.matter,
	a.matter_describe,
	a.spot_name,
	a.spot_x,
	a.spot_y,
	a.type,
	b.examinee_id,
	b.`name` student_name,
	a.spot_type
FROM v_spot_matter a,v_student_user_info b
where a.college=b.college ;

-- ----------------------------
-- View structure for v_spot_student_state
-- ----------------------------
DROP VIEW IF EXISTS `v_spot_student_state`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_spot_student_state` AS select #基于v_spot_student，t_registration_management视图
	v.*,
	(case  t.state when '1' then 1 ELSE 0 end ) state 
from v_spot_student v LEFT JOIN t_registration_management t 
on v.examinee_id=t.examinee_id and v.taskId=t.task_id ;

-- ----------------------------
-- View structure for v_student_user_info
-- ----------------------------
DROP VIEW IF EXISTS `v_student_user_info`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%`  VIEW `v_student_user_info` AS SELECT##学生用户基础信息
	s.loginname,
	s.`name` as studentname,
	s.organization_id,
	s.usertype,
	t.*
FROM
	sys_user s,
	t_sd_base_information t
WHERE
	s.usertype = 14
AND t.examinee_id = s.loginname ;

-- ----------------------------
-- Procedure structure for p_tj_stusignin
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_tj_stusignin`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `p_tj_stusignin`(in coloege varchar(50))
BEGIN
-- 新生签到统计 ws 2017.7.12 --在用，不可删
-- 动态获取sql的过程
-- 输入参数（coloege  学院  varchar类型）

SET @EE='';
	SELECT @EE:=CONCAT(@EE,'SUM(IF(C2=\'',C2,'\'',',C3,0)) AS \'',C2,'\',') FROM ( 
SELECT '总人数' AS c2 FROM DUAL UNION
SELECT t.matter AS c2 FROM t_spot_matter t , t_spot_set s  , spot_and_matter sm  
WHERE s.id = sm.spot_id AND t.id = sm.matter_id   AND s.spot_type='2301' AND t.type='需办理'  
and s.college = coloege 
) A;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_tj_way
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_tj_way`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `p_tj_way`(in n int)
BEGIN
-- 按到校交通方式统计 ws 2017.7.7 --在用，不可删
-- 动态获取sql的过程
-- 输入参数（int类型）对查询结果无影响。
    SET @EE='';
	SELECT @EE:=CONCAT(@EE,'SUM(IF(C2=\'',C2,'\'',',C3,0)) AS \'',C2,'\',') FROM ( 
SELECT '预报到人数' as C2    FROM DUAL
UNION 	
SELECT '亲友驾车' as C2    FROM DUAL
UNION 
	SELECT '自乘' as C2    FROM DUAL
UNION 
select DISTINCT TX.dd_name  as C2   FROM base_data TX 
WHERE TX.dd_type_code = '18'
) A;

END
;;
DELIMITER ;
